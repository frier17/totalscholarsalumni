from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from util.models import BaseModel, name_default, text_default, code_default, BaseUser, datetime_default, PublishMixin, \
    FileUploadMixin


class BaseEvent(BaseModel):
    event_name = models.CharField(**name_default)
    event_comment = models.TextField(**text_default)
    event_start = models.DateTimeField(**datetime_default)
    event_end = models.DateTimeField(null=True)
    event_type = models.CharField(null=True, **code_default)

    class Meta:
        abstract = True


class MentoringProgramme(FileUploadMixin, PublishMixin, BaseModel):
    programme_name = models.CharField(**name_default)
    programme_start = models.DateTimeField(**datetime_default)
    programme_end = models.DateTimeField(**datetime_default)

    source = models.ForeignKey('util.BaseUser', related_name='mentoring_programme', on_delete=models.DO_NOTHING)
    approvals = GenericRelation('util.Approval', related_name='mentoring_programmes')
    objectives = models.TextField(**text_default)

    def publish(self, **kwargs):
        self.broadcasts = '__all__'


class MentoringSession(FileUploadMixin, PublishMixin, BaseEvent):
    programme = models.ForeignKey(MentoringProgramme, on_delete=models.CASCADE, related_name='mentoring_session')
    mentor = models.ForeignKey(BaseUser, related_name="mentor", on_delete=models.DO_NOTHING)
    pupil = models.ForeignKey(BaseUser, related_name="pupil", on_delete=models.DO_NOTHING)

    def publish(self, **kwargs):
        self.broadcast_order = ['mentoring_session', 'mentor', 'pupil']
        return super(MentoringSession, self).publish(**kwargs)
