from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import DetailView, ListView, CreateView

from mentorship import forms
from mentorship import models
from util.views import DeleteAclMixin, UpdateAclMixin, ReadOnlyMixin


class MentoringProgrammeListView(ListView):
    model = models.MentoringProgramme
    # paginate_by = 10
    template_name = 'mentorship/mentoring_programme_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(MentoringProgrammeListView, self).get_queryset()
            # filter queryset for logged in user
            # @todo: customise approval to show only entries with status == APPROVED
            queryset = queryset.all()
            return queryset
        else:
            return super(MentoringProgrammeListView, self).get_queryset().none()


class MentoringProgrammeCreateView(LoginRequiredMixin, CreateView):
    model = models.MentoringProgramme
    form_class = forms.MentoringProgrammeForm
    template_name = 'mentorship/mentoring_programme_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')
    
    def form_valid(self, form):
        form.instance.source = self.request.user
        return super(MentoringProgrammeCreateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(MentoringProgrammeCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('mentorship:mentoring_programme-list')


class MentoringProgrammeDetailView(DetailView):
    model = models.MentoringProgramme
    template_name = 'mentorship/mentoring_programme_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(MentoringProgrammeDetailView, self).get_context_data(**kwargs)


class MentoringProgrammeUpdateView(UpdateAclMixin):
    model = models.MentoringProgramme
    form_class = forms.MentoringProgrammeForm
    template_name = 'mentorship/mentoring_programme_form.html'
    initial = {}
    user_field = 'source'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(MentoringProgrammeUpdateView, self).get_object()

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('mentorship:mentoring_programme-list')

    def get_initial(self):
        initial = super(MentoringProgrammeUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class MentoringProgrammeDeleteView(DeleteAclMixin):
    model = models.MentoringProgramme
    success_url = reverse_lazy('mentorship:mentoring-programme-list')
    template_name = 'mentorship/mentoring_programme_confirm_delete.html'
    user_field = 'source'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(MentoringProgrammeDeleteView, self).get_context_data(**kwargs)


class MentoringSessionListView(LoginRequiredMixin, ListView):
    model = models.MentoringSession
    # paginate_by = 10
    template_name = 'mentorship/mentoring_session_list.html'
    login_url = reverse_lazy('user-login')

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(MentoringSessionListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(Q(mentor=user) | Q(pupil=user))
            return queryset
        else:
            return super(MentoringSessionListView, self).get_queryset().none()


class MentoringSessionCreateView(LoginRequiredMixin, CreateView):
    model = models.MentoringSession
    form_class = forms.MentoringSessionForm
    template_name = 'mentorship/mentoring_session_form.html'
    initial = {}

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(MentoringSessionCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('mentorship:mentoring_session-list')


class MentoringSessionDetailView(LoginRequiredMixin, DetailView):
    model = models.MentoringSession
    template_name = 'mentorship/mentoring_session_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(MentoringSessionDetailView, self).get_context_data(**kwargs)


class MentoringSessionUpdateView(ReadOnlyMixin):
    model = models.MentoringSession
    form_class = forms.MentoringSessionForm
    template_name = 'mentorship/mentoring_session_form.html'
    initial = {}


class MentoringSessionDeleteView(ReadOnlyMixin):
    model = models.MentoringSession
    success_url = reverse_lazy('mentorship:mentoring-session-list')
    template_name = 'mentorship/mentoring_session_confirm_delete.html'

