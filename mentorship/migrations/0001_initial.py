# Generated by Django 3.0.6 on 2020-08-18 20:17

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import util


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('util', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MentoringProgramme',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=util.generate_uuid, editable=False, unique=True)),
                ('slug', models.SlugField(editable=False)),
                ('is_public', models.BooleanField(default=False, help_text='Enable to make record visible to all members')),
                ('is_shareable', models.BooleanField(default=False, help_text='Enable to make this record shareable on site')),
                ('programme_name', models.CharField(max_length=256)),
                ('programme_start', models.DateTimeField()),
                ('programme_end', models.DateTimeField()),
                ('objectives', models.TextField(null=True)),
                ('media', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='mentoring_programme', to='util.UserUploadedDocument')),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='mentoring_programme', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MentoringSession',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=util.generate_uuid, editable=False, unique=True)),
                ('slug', models.SlugField(editable=False)),
                ('is_public', models.BooleanField(default=False, help_text='Enable to make record visible to all members')),
                ('is_shareable', models.BooleanField(default=False, help_text='Enable to make this record shareable on site')),
                ('event_name', models.CharField(max_length=256)),
                ('event_comment', models.TextField(null=True)),
                ('event_start', models.DateTimeField()),
                ('event_end', models.DateTimeField(null=True)),
                ('event_type', models.CharField(max_length=15, null=True)),
                ('coverage', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='mentoring_session', to='util.UserUploadedDocument')),
                ('mentor', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='mentor', to=settings.AUTH_USER_MODEL)),
                ('programme', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='mentoring_session', to='mentorship.MentoringProgramme')),
                ('pupil', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='pupil', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
