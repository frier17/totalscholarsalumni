from django.urls import path

from mentorship import views

app_name = 'mentorship'

urlpatterns = []

urlpatterns += (
    # urls for MentoringProgramme
    path('mentoring-programme/', views.MentoringProgrammeListView.as_view(), name='mentoring-programme-list'),
    path('mentoring-programme/create/', views.MentoringProgrammeCreateView.as_view(),
         name='mentoring-programme-create'),
    path('mentoring-programme/detail/<str:slug>/', views.MentoringProgrammeDetailView.as_view(),
         name='mentoring-programme-detail'),
    path('mentoring-programme/update/<str:slug>/', views.MentoringProgrammeUpdateView.as_view(),
         name='mentoring-programme-update'),
    path('mentoring-programme/delete/<str:slug>/', views.MentoringProgrammeDeleteView.as_view(),
         name='mentoring-programme-delete')
)

urlpatterns += (
    # urls for MentoringSession
    path('mentoring-session/', views.MentoringSessionListView.as_view(), name='mentoring-session-list'),
    path('mentoring-session/create/', views.MentoringSessionCreateView.as_view(), name='mentoring-session-create'),
    path('mentoring-session/detail/<str:slug>/', views.MentoringSessionDetailView.as_view(),
         name='mentoring-session-detail'),
    path('mentoring-session/update/<str:slug>/', views.MentoringSessionUpdateView.as_view(),
         name='mentoring-session-update'),
    path('mentoring-session/delete/<str:slug>/', views.MentoringSessionDeleteView.as_view(),
         name='mentoring-session-delete')
)
