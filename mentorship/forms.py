
from django import forms
from mentorship import models


class MentoringProgrammeForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(MentoringProgrammeForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.MentoringProgramme
        fields = '__all__'
        exclude = ['source', 'approvals']


class MentoringSessionForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(MentoringSessionForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.MentoringSession
        fields = '__all__'      

