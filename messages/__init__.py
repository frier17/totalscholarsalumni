"""
Defines the system wide message string templates for all signals, feed backs, notifications
etc.
"""

email_successful = """
Email successfully sent to recipient(s)
From Email: {from_email}
Recipient email: {recipient_email}
"""

email_error = """
Error occurred while attempting to send email. 
Error event on: {asctime}
"""

user_black_listed = """
User blacklisted due to multiple failed OTP authentications.
Black list event on: {asctime}
User: {username} with ID: {user_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

# @todo: The otp key is replaced by code passed by user not valid otp
otp_auth_failed = """
User OTP authentication failed.
Failed authentication event on: {asctime}
User code: {otp}
User: {username} with ID: {user_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

otp_authenticated = """
User OTP successfully authenticated.
Authentication event on: {asctime}
Authenticated code: {otp}
Authenticated user: {username} with ID: {user_id}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

otp_deleted = """
User OTP successfully deleted.
Authentication event on: {asctime}
Deleted OTP Reference code: {otp_reference}
Authenticated user: {username} with ID: {user_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

otp_pre_generated = """
User OTP successfully created.
Creation event on: {asctime}
Requesting user: {username} with ID: {user_id}
OTP: {otp}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

otp_generated = """
User OTP successfully created.
Creation event on: {asctime}
Requesting user: {username} with ID: {user_id}
Reference: {reference}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

create_otp_failed = """
User OTP generation process failed.
Error event on: {asctime}
Requesting user: {username} with ID: {user_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

user_inquiry = """
User inquiry received.
Inquiry event on: {asctime}
Authenticated user: {username} with ID: {user_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

multiple_user_id = """
Registered new user with credentials used by existing member.
Registration event on: {asctime}
Shared credentials: {credentials}
"""

auth_user_exist = """
An authenticated user is logged in for the current session.
To perform services for another user, kindly log out the current user.
"""

# @todo: Recipients can be passed in as a comma separated list of emails
invitation_to_register = """
A prospective member invited to register.
Invitation event on: {asctime}
Invitation recipient(s): {recipients}
Authenticated user: {username} with ID: {user_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

invitation_to_view = """ 
A prospective member invited to view TSA.
Invitation event on: {asctime}
Invitation recipient(s): {recipients}
Authenticated user: {username} with ID: {user_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

invitation_to_view_profile = """
A prospective member invited to view existing member's profile.
Invitation event on: {asctime}
Invitation recipient(s): {recipients}
Profile owner: {username} with ID: {user_id}
Authenticated user: {username} with ID: {user_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

object_published = """
Model object published.
Publication event on: {asctime}
Published instance: {model} with ID: {object_id}
Authenticated user: {username} with ID: {user_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

request_approved = """
User's request approved.
Approval event on: {asctime}
Approved instance: {model} with ID: {object_id}
Authenticated user: {username} with ID: {user_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

user_followed = """
A member is being followed.
Relationship event on: {asctime}
Follower profile: {follower} with ID: {follower_id}
Leader profile: {leader} with ID: {leader_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

user_blocked = """
A member is blocked by user.
Relationship event on: {asctime}
Follower profile: {follower} with ID: {follower_id}
Leader profile: {leader} with ID: {leader_id}
Authenticated user: {username} with ID: {user_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

user_viewed_profile = """
Member profile viewed.
Relationship event on: {asctime}
Member profile: {username} with ID: {user_id}
Authenticated user: {username} with ID: {user_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

http_request_failed = """
HTTP Request failed.
Http error on: {asctime}
Authenticated user: {username} with ID: {user_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

user_created = """
User successfully created.
Creation event on: {asctime}
Requesting user: {username} with ID: {user_id}
User fields: {user_fields}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

user_deleted = """
User deleted.
Delete event on: {asctime}
Deleted user: {username} with ID: {user_id}
Requesting user: {requester} with ID: {request_id}
Referer URL: {referer}
Request URL: {url}
User IP/Remote Address: {remote_addr}
"""

user_login_failed = """
User log in failed.
Error on: {asctime}
"""

user_login = """
User log successful.
Username: {username}
Log in on: {last_login}
User IP/Remote Address: {remote_addr}
"""

user_does_not_exist = """
User with the given credentials does not exist.
Username: {username}
Email: {email}
Mobile: {mobile}
"""

invalid_user_request = """
The request by user could not be processed. Ensure valid credentials were used and user is authenticated.
"""

contact_auto_response = """
Thank you for your recent inquiry or suggestion on {asctime}.
You will be contacted soon by a member of TSA team.
"""
