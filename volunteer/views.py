from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import DeleteView, DetailView, ListView, UpdateView, CreateView

from util.views import DeleteAclMixin, UpdateAclMixin
from volunteer import forms
from volunteer import models


class VolunteerCourseListView(ListView):
    model = models.VolunteerCourse
    # paginate_by = 10
    template_name = 'volunteer/volunteer_course_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(VolunteerCourseListView, self).get_queryset()
            # filter queryset for logged in user
            return queryset
        else:
            return super(VolunteerCourseListView, self).get_queryset().none()


class VolunteerCourseCreateView(LoginRequiredMixin, CreateView):
    model = models.VolunteerCourse
    form_class = forms.VolunteerCourseForm
    template_name = 'volunteer/volunteer_course_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(VolunteerCourseCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('volunteer:volunteer_course-list')


class VolunteerCourseDetailView(DetailView):
    model = models.VolunteerCourse
    template_name = 'volunteer/volunteer_course_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(VolunteerCourseDetailView, self).get_context_data(**kwargs)


class VolunteerCourseUpdateView(UpdateAclMixin):
    model = models.VolunteerCourse
    form_class = forms.VolunteerCourseForm
    template_name = 'volunteer/volunteer_course_form.html'
    initial = {}
    user_field = 'course_approvals__approved_by'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(VolunteerCourseUpdateView, self).get_object()

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('volunteer:volunteer_course-list')

    def get_initial(self):
        initial = super(VolunteerCourseUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class VolunteerCourseDeleteView(DeleteAclMixin):
    model = models.VolunteerCourse
    success_url = reverse_lazy('volunteer:volunteer-course-list')
    template_name = 'volunteer/volunteer_course_confirm_delete.html'
    user_field = 'course_approvals__approved_by'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(VolunteerCourseDeleteView, self).get_context_data(**kwargs)


class VolunteerCourseApplicationListView(ListView):
    model = models.VolunteerCourseApplication
    # paginate_by = 10
    template_name = 'volunteer/volunteer_course_application_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(VolunteerCourseApplicationListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(volunteer=user)
            return queryset
        else:
            return super(VolunteerCourseApplicationListView, self).get_queryset().none()


class VolunteerCourseApplicationCreateView(LoginRequiredMixin, CreateView):
    model = models.VolunteerCourseApplication
    form_class = forms.VolunteerCourseApplicationForm
    template_name = 'volunteer/volunteer_course_application_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(VolunteerCourseApplicationCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('volunteer:volunteer_course_application-list')


class VolunteerCourseApplicationDetailView(DetailView):
    model = models.VolunteerCourseApplication
    template_name = 'volunteer/volunteer_course_application_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(VolunteerCourseApplicationDetailView, self).get_context_data(**kwargs)


class VolunteerCourseApplicationUpdateView(UpdateAclMixin):
    model = models.VolunteerCourseApplication
    form_class = forms.VolunteerCourseApplicationForm
    template_name = 'volunteer/volunteer_course_application_form.html'
    initial = {}
    user_field = 'volunteer'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(VolunteerCourseApplicationUpdateView, self).get_object()

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('volunteer:volunteer_course_application-list')

    def get_initial(self):
        initial = super(VolunteerCourseApplicationUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class VolunteerCourseApplicationDeleteView(DeleteAclMixin):
    model = models.VolunteerCourseApplication
    success_url = reverse_lazy('volunteer:volunteer-course-application-list')
    template_name = 'volunteer/volunteer_course_application_confirm_delete.html'
    user_field = 'volunteer'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(VolunteerCourseApplicationDeleteView, self).get_context_data(**kwargs)
