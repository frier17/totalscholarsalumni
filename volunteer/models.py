from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from util.models import BaseModel, name_default, PublishMixin, FileUploadMixin


class VolunteerCourse(FileUploadMixin, PublishMixin, BaseModel):
    course_name = models.CharField(**name_default)
    course_goals = models.TextField()
    description = models.TextField()
    kpi = models.TextField(help_text='Key Performance Indices separated by newline or '
                                     'semicolon')
    course_start = models.DateTimeField()
    course_end = models.DateTimeField()
    publish_course = models.BooleanField(default=False)
    enable_volunteering = models.BooleanField(default=False)
    course_owners = models.ForeignKey('util.BaseUser', on_delete=models.DO_NOTHING,
                                      related_name='volunteer_courses')
    course_approvals = GenericRelation('util.Approval', on_delete=models.DO_NOTHING, related_name='volunteer_course')

    def publish(self, **kwargs):
        self.excluded_broadcasts = ['course_approvals', 'course_owners', 'kpi']
        return super(VolunteerCourse, self).publish(**kwargs)


class VolunteerCourseApplication(PublishMixin, BaseModel):
    volunteer = models.ForeignKey('util.BaseUser', on_delete=models.DO_NOTHING)
    volunteer_course = models.ForeignKey('VolunteerCourse', on_delete=models.DO_NOTHING,
                                         related_name='volunteer_course_application')
    application_datetime = models.DateTimeField()
    application_approvals = GenericRelation('util.Approval', related_name='volunteer_applications')
    application_reason = models.TextField()

    def publish(self, **kwargs):
        self.excluded_broadcasts = ['application_approvals']
        return super(VolunteerCourseApplication, self).publish(**kwargs)
