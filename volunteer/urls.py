from django.urls import path

from volunteer import views

app_name = 'volunteer'

urlpatterns = []

urlpatterns += (
    # urls for VolunteerCourse
    path('volunteer-course/', views.VolunteerCourseListView.as_view(), name='volunteer-course-list'),
    path('volunteer-course/create/', views.VolunteerCourseCreateView.as_view(), name='volunteer-course-create'),
    path('volunteer-course/detail/<str:slug>/', views.VolunteerCourseDetailView.as_view(),
         name='volunteer-course-detail'),
    path('volunteer-course/update/<str:slug>/', views.VolunteerCourseUpdateView.as_view(),
         name='volunteer-course-update'),
    path('volunteer-course/delete/<str:slug>/', views.VolunteerCourseDeleteView.as_view(),
         name='volunteer-course-delete')
)

urlpatterns += (
    # urls for VolunteerCourseApplication
    path('volunteer-course-application/', views.VolunteerCourseApplicationListView.as_view(),
         name='volunteer-course-application-list'),
    path('volunteer-course-application/create/', views.VolunteerCourseApplicationCreateView.as_view(),
         name='volunteer-course-application-create'),
    path('volunteer-course-application/detail/<str:slug>/', views.VolunteerCourseApplicationDetailView.as_view(),
         name='volunteer-course-application-detail'),
    path('volunteer-course-application/update/<str:slug>/', views.VolunteerCourseApplicationUpdateView.as_view(),
         name='volunteer-course-application-update'),
    path('volunteer-course-application/delete/<str:slug>/', views.VolunteerCourseApplicationDeleteView.as_view(),
         name='volunteer-course-application-delete')
)
