
from django import forms
from volunteer import models


class VolunteerCourseForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(VolunteerCourseForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.VolunteerCourse
        fields = '__all__'      


class VolunteerCourseApplicationForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(VolunteerCourseApplicationForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.VolunteerCourseApplication
        fields = '__all__'      

