from string import Template

view_template = Template(
"""
class $model_list_view(ListView):
    model = models.$model
    # paginate_by = 10
    template_name = '$app_case/$model_case_list.html'

    def get_queryset(self):  
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super($model_list_view, self).get_queryset()
            # filter queryset for logged in user
            return queryset
        else:
            return super($model_list_view, self).get_queryset().none()        


class $model_create_view(CreateView):
    model = models.$model
    form_class = forms.$model_form
    template_name = '$app_case/$model_case_form.html'
    initial = {}

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
            
    def get_initial(self):
        initial = super($model_create_view, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('$app_case:$model_case-list')
            

class $model_detail_view(DetailView):
    model = models.$model
    template_name = '$app_case/$model_case_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super($model_detail_view, self).get_context_data(**kwargs)


class $model_update_view(UpdateView):
    model = models.$model
    form_class = forms.$model_form
    template_name = '$app_case/$model_case_form.html'
    initial = {}

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super($model_update_view, self).get_object()

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('$app_case:$model_case-list')
            
    def get_initial(self):
        initial = super($model_update_view, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial
    

class $model_delete_view(DeleteView):
    model = models.$model
    success_url = reverse_lazy('$app:$model_url-list')
    template_name = '$app_case/$model_case_confirm_delete.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super($model_delete_view, self).get_context_data(**kwargs)
"""
)

view_import = Template(
"""
from django.views.generic import DeleteView, DetailView, ListView, UpdateView, CreateView
from django.urls import reverse_lazy

from $app import models
from $app import forms
"""
)
