from string import Template

serializer_import = Template("""
from rest_framework import serializers

from $app import models

""")

serializer_template = Template("""

class $modelSerializer(serializers.$serializer):

    class Meta:
        model = models.$model
        fields = $fields

""")
