from string import Template

form_import = Template(
"""
from django import forms
from $app import models

"""
)
form_template = Template(
"""
class $model_form(forms.$default_model_form):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super($model_form, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.$model
        fields = $fields      

"""
)
