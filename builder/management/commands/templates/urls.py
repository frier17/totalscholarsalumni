from string import Template

url_import = Template(
    """
    from django.contrib import admin    
    from django.urls import include, path
    from rest_framework import routers
    
    from $app import api
    
    """
)
url_template = Template(
    """
    router = routers.$router()
    # $app REST endpoints
    router.register(r'$end_point', api.$modelViewSet)
    
    urlpatterns = (path('api/$app/$end_point/', include((router.urls, '$end_point'))),)
    
    """
)
