from string import Template

api_import = Template(
    """
    from rest_framework import viewsets, permissions
    
    from $app import models
    from $app import serializers
    
    """
)

api_template = Template(
    """
    
    class $modelViewSet(viewsets.$viewset):
    
        queryset = models.$model.$model_manager.all()
        serializer_class = serializers.$modelSerializer
        permission_classes = $permissions
    
    """
)
