"""tsa URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include
from django.conf.urls.static import static

from tsa import settings
from achievement.urls import urlpatterns as achievement_urls
from career.urls import urlpatterns as career_urls
from mentorship.urls import urlpatterns as mentoring_urls
from profile_management.urls import urlpatterns as profile_management_urls
from social_network.urls import urlpatterns as social_urls
from util.registration import urlpatterns as registration_urls
from util.site import urlpatterns as site_urls
from util.urls import urlpatterns as util_urls
from volunteer.urls import urlpatterns as volunteer_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(site_urls + registration_urls)),
    path('mentorship/', include((mentoring_urls, 'mentorship'))),
    path('util/', include((util_urls, 'util'))),
    path('achievement/', include((achievement_urls, 'achievement'))),
    path('career/', include((career_urls, 'career'))),
    path('profile_management/', include((profile_management_urls, 'profile_management'))),
    path('social_network/', include((social_urls, 'social_network'))),
    path('volunteer/', include((volunteer_urls, 'volunteer'))),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


