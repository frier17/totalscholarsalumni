"""
Django settings for tsa project.

Generated by 'django-admin startproject' using Django 3.0.5.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""
import os
import environ

env = environ.Env()
environ.Env.read_env()

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'i*g^_8i%oqbnciu6mgdg2o*w$c!fi%j%^y02!8m!www)@7@aid'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ENABLE_DEV_LOG = True

ALLOWED_HOSTS = ['*', ]

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.templatetags.static',
    'django.contrib.humanize',
    'crispy_forms',
    'storages',
    'whitenoise'
]

INSTALLED_APPS += [
    'django_seed',
    'achievement',
    'career',
    'mentorship',
    'profile_management',
    'social_network',
    'volunteer',
    'util',
    'builder',
]

# Use custom user class to manage user and admin in system
AUTH_USER_MODEL = 'util.BaseUser'

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'tsa.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR + '/templates/', ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

CRISPY_TEMPLATE_PACK = 'bootstrap4'

WSGI_APPLICATION = 'tsa.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },
    'test': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'test_db.sqlite3'),
    }
}

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation'
                '.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Amazon S3 storage for files
if not DEBUG:
    USE_S3_STORAGE = True
else:
    USE_S3_STORAGE = False

# aws settings

if USE_S3_STORAGE:
    AWS_ACCESS_KEY_ID = 'AKIAVSVI3KKTPERJ355Y'
    AWS_SECRET_ACCESS_KEY = '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'
    AWS_STORAGE_BUCKET_NAME = 'tsa-media'
    AWS_LOCATION = 'media'
    AWS_DEFAULT_ACL = 'public-read'
    AWS_S3_FILE_OVERWRITE = False

    AWS_S3_CUSTOM_DOMAIN = f'{AWS_STORAGE_BUCKET_NAME}.s3.amazonaws.com'
    AWS_S3_OBJECT_PARAMETERS = {'CacheControl': 'max-age=86400'}

    # Media file storage
    PUBLIC_MEDIA_LOCATION = 'media'
    MEDIA_URL = f'https://{AWS_S3_CUSTOM_DOMAIN}/{PUBLIC_MEDIA_LOCATION}/'
    MEDIA_PATH = MEDIA_URL
    DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
else:
    # Path to storing application and users' files
    MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
    MEDIA_PATH = 'media/'
    MEDIA_URL = '/media/'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = (

)
STATIC_HOST = 'https://d1a77r4f3hv91h.cloudfront.net' if not DEBUG else ''

STATIC_URL = STATIC_HOST + '/'

# Session and cookies
SESSION_COOKIE_SECURE = True

CSRF_COOKIE_SECURE = True

# File hashing key for user uploaded files
FILE_HASH_KEY = b'$6$0XJjfeVpuPhA7iEh$6$FoABnf0EmQa98So7$6$X2IAJ/g3N.Kt1RBy'

DEFAULT_IMAGE_HEIGHT = 200
DEFAULT_IMAGE_WIDTH = 185

# Approved media types for the application and users
MEDIA_TYPES = [
    ('jpeg', 'JPEG Image'),
    ('jpg', 'JPEG Image'),
    ('png', 'PNG Image'),
    ('gif', 'GIF Image'),
    ('mp3', 'MP3 Audio'),
    ('mp4', 'MP4 Video'),
    ('avi', 'AVI Video'),
    ('doc', 'MS Word Document 2003'),
    ('docx', 'MS Word Document 2007+'),
    ('xls', 'MS Excel Document 2003'),
    ('xlsx', 'MS Excel Document 2007+'),
    ('ppt', 'MS PowerPoint 2003'),
    ('pptx', 'MS PowerPoint 2007+'),
    ('pdf', 'Acrobat PDF'),
    ('xlm', 'XLM File'),
    ('csv', 'CSV File'),
    ('md', 'MarkDown File'),
    ('json', 'JSON File'),
    ('html', 'Web Page')
]

if ENABLE_DEV_LOG:
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
            },
        },
        'root': {
            'handlers': ['console'],
            'level': 'WARNING',
        },
    }

# OTP server configurations
OTP_DURATION = 5
OTP_EMAIL_SUBJECT = 'TSA ONE TIME PASSCODE (OTP) SERVICE'

# SMTP Email settings for the application
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_HOST = 'smtp.ipage.com'
EMAIL_PORT = '587'

# API email settings
EMAIL_API_KEY = 'cefa48c615f92d1d14c7a5a2a4c7da20-7238b007-b87f9468'
EMAIL_API_URL = 'https://api.mailgun.net/v3/totalscholarsalumni.org/messages'

# Email OTP API user settings
OTP_FROM_EMAIL = 'otp.service@totalscholarsalumni.org'

# Email OTP SMTP settings
OTP_SMTP_EMAIL_USER = 'otp.service@totalscholarsalumni.org'
OTP_SMTP_EMAIL_PASSWORD = 'Eef4b1526d4897acbc6a3e92c00582ec-5238b007-a502125a'

# Default settings
EMAIL_HOST_USER = 'postmaster@totalscholarsalumni.org'
EMAIL_HOST_PASSWORD = 'Rf7c0ed0fc3aa5088d80372116cbd3e8-3235b007-7cbd36d2'
EMAIL_USE_TLS = False
EMAIL_USE_SSL = False
DEFAULT_FROM_EMAIL = 'postmaster@totalscholarsalumni.org'
EMAIL_TEST_MESSAGE = 'Sample application testing from the Total Scholars Alumni community'

# User management for registration
DEFAULT_USER_PASSWORD = '<generic password>'

