LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
                'verbose': {
                        'format': '{levelname} - {asctime} - {module} - {process:d}'
                                  ' - {thread:d}: {message}',
                        'style': '{',
                },
                'standard': {
                        'format': '{levelname} - {asctime} - {module} : {message}',
                        'style': '{',
                },
                'basic': {
                        'format': '{levelname}: {message}',
                        'style': '{'
                }
        },
        'handlers': {
                'file': {
                        'level': 'WARNING',
                        'class': 'logging.FileHandler',
                        'filename': BASE_DIR + '/logs/application.log',
                        'formatter': 'verbose'
                },
                'mail_admins': {
                        'level': 'CRITICAL',
                        'class': 'django.utils.log.AdminEmailHandler',
                        'formatter': 'standard'
                }
        },
        'loggers': {
                'django': {
                        'handlers': ['file', 'mail_admins'],
                        'level': 'WARNING',
                        'propagate': True,
                },
                'django.request': {
                        'handlers': ['file', 'mail_admins'],
                        'level': 'CRITICAL',
                        'propagate': False,
                },
        },
}

