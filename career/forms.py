
from django import forms
from career import models


class JobApplicationForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(JobApplicationForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.JobApplication
        exclude = ['application_datetime']
        fields = ['job_posting', 'application_cv', 'application_reason', 'is_public', 'is_shareable']

    def save(self, commit=True):
        pass


class JobPostingForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(JobPostingForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.JobPosting
        fields = '__all__'
        exclude = ['source']

