import datetime as dt

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from util import current_timezone
from util.models import text_default, name_default, datetime_default, SoftDeleteMixin


class JobPosting(SoftDeleteMixin):
    title = models.CharField(**name_default)
    description = models.TextField(**text_default)
    source = models.ForeignKey('util.BaseUser', on_delete=models.DO_NOTHING,
                               related_name='job_postings')
    min_work_experience = models.PositiveSmallIntegerField()
    is_volunteer_job = models.BooleanField(default=False)
    approvals = GenericRelation('util.Approval', related_name='job_postings')
    required_qualification = models.CharField(**name_default)
    required_qualification_description = models.TextField()

    def publish(self, **kwargs):
        self.excluded_broadcasts = ['approvals']
        return super(JobPosting, self).publish(**kwargs)


class JobApplication(SoftDeleteMixin):
    job_posting = models.ForeignKey('JobPosting', on_delete=models.DO_NOTHING, related_name='job_applications')
    application_cv = models.ForeignKey('profile_management.UserCV', related_name='job_application',
                                       on_delete=models.DO_NOTHING)
    application_reason = models.TextField(**text_default)
    application_datetime = models.DateTimeField(**datetime_default)

    def save(self, *args, **kwargs):
        if not self.application_datetime:
            self.application_datetime = current_timezone(dt.datetime.now())

    def publish(self, **kwargs):
        # Job applications can be viewed on career management but not user's wall
        return NotImplemented
