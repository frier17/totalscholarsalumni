from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms import ModelMultipleChoiceField
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import DeleteView, DetailView, ListView, UpdateView, CreateView

from career import forms
from career import models
from profile_management.models import UserCV
from util.views import DeleteAclMixin, UpdateAclMixin


class JobApplicationListView(LoginRequiredMixin, ListView):
    model = models.JobApplication
    # paginate_by = 10
    template_name = 'career/job_application_list.html'
    login_url = reverse_lazy('user-login')

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(JobApplicationListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(application_cv__user=user)
            return queryset
        else:
            return super(JobApplicationListView, self).get_queryset().none()


class JobApplicationCreateView(LoginRequiredMixin, CreateView):
    model = models.JobApplication
    form_class = forms.JobApplicationForm
    template_name = 'career/job_application_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')

    def get(self, request, *args, **kwargs):        
        queryset = UserCV.objects.filter(user=self.request.user)
        form = self.form_class(initial={'application_cv': queryset})
        form.fields['application_cv'] = ModelMultipleChoiceField(queryset)
        return render(request, template_name=self.template_name, context={'form': form})

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(JobApplicationCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('career:job_application-list')


class JobApplicationDetailView(LoginRequiredMixin, DetailView):
    model = models.JobApplication
    template_name = 'career/job_application_detail.html'
    login_url = reverse_lazy('user-login')

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(JobApplicationDetailView, self).get_context_data(**kwargs)


class JobApplicationUpdateView(UpdateAclMixin):
    model = models.JobApplication
    form_class = forms.JobApplicationForm
    template_name = 'career/job_application_form.html'
    initial = {}

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(JobApplicationUpdateView, self).get_object()

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('career:job_application-list')

    def get_initial(self):
        initial = super(JobApplicationUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class JobApplicationDeleteView(LoginRequiredMixin, DeleteAclMixin):
    model = models.JobApplication
    success_url = reverse_lazy('career:job-application-list')
    template_name = 'career/job_application_confirm_delete.html'
    user_field = 'applicant'
    login_url = reverse_lazy('user-login')

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(JobApplicationDeleteView, self).get_context_data(**kwargs)


class JobPostingListView(ListView):
    model = models.JobPosting
    # paginate_by = 10
    template_name = 'career/job_posting_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(JobPostingListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(source=user)
            return queryset
        else:
            return super(JobPostingListView, self).get_queryset().none()


class JobPostingCreateView(LoginRequiredMixin, CreateView):
    model = models.JobPosting
    form_class = forms.JobPostingForm
    template_name = 'career/job_posting_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')
    
    def form_valid(self, form):
        form.instance.source = self.request.user
        return super(JobPostingCreateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(JobPostingCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('career:job_posting-list')


class JobPostingDetailView(DetailView):
    model = models.JobPosting
    template_name = 'career/job_posting_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(JobPostingDetailView, self).get_context_data(**kwargs)


class JobPostingUpdateView(LoginRequiredMixin, UpdateAclMixin):
    model = models.JobPosting
    form_class = forms.JobPostingForm
    template_name = 'career/job_posting_form.html'
    initial = {}
    user_field = 'source'
    login_url = reverse_lazy('user-login')

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(JobPostingUpdateView, self).get_object()

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('career:job_posting-list')

    def get_initial(self):
        initial = super(JobPostingUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class JobPostingDeleteView(DeleteAclMixin):
    model = models.JobPosting
    success_url = reverse_lazy('career:job-posting-list')
    template_name = 'career/job_posting_confirm_delete.html'
    user_field = 'source'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(JobPostingDeleteView, self).get_context_data(**kwargs)
