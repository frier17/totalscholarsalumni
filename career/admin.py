from django.contrib import admin

from . import models

admin.register(models.JobApplication)
admin.register(models.JobPosting)
