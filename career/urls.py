from django.urls import path

from career import views

app_name = 'career'

urlpatterns = []

urlpatterns += (
    # urls for JobApplication
    path('job-application/', views.JobApplicationListView.as_view(), name='job-application-list'),
    path('job-application/create/', views.JobApplicationCreateView.as_view(), name='job-application-create'),
    path('job-application/detail/<str:slug>/', views.JobApplicationDetailView.as_view(), name='job-application-detail'),
    path('job-application/update/<str:slug>/', views.JobApplicationUpdateView.as_view(), name='job-application-update'),
    path('job-application/delete/<str:slug>/', views.JobApplicationDeleteView.as_view(), name='job-application-delete')
)

urlpatterns += (
    # urls for JobPosting
    path('job-posting/', views.JobPostingListView.as_view(), name='job-posting-list'),
    path('job-posting/create/', views.JobPostingCreateView.as_view(), name='job-posting-create'),
    path('job-posting/detail/<str:slug>/', views.JobPostingDetailView.as_view(), name='job-posting-detail'),
    path('job-posting/update/<str:slug>/', views.JobPostingUpdateView.as_view(), name='job-posting-update'),
    path('job-posting/delete/<str:slug>/', views.JobPostingDeleteView.as_view(), name='job-posting-delete')
)
