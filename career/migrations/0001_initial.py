# Generated by Django 3.0.6 on 2020-08-18 20:16

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import util


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('profile_management', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='JobPosting',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=util.generate_uuid, editable=False, unique=True)),
                ('slug', models.SlugField(editable=False)),
                ('is_public', models.BooleanField(default=False, help_text='Enable to make record visible to all members')),
                ('is_shareable', models.BooleanField(default=False, help_text='Enable to make this record shareable on site')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('last_modified_at', models.DateTimeField(auto_now=True)),
                ('deleted_at', models.DateTimeField(editable=False, null=True)),
                ('force_delete', models.BooleanField(default=False, editable=False)),
                ('title', models.CharField(max_length=256)),
                ('description', models.TextField(null=True)),
                ('min_work_experience', models.PositiveSmallIntegerField()),
                ('is_volunteer_job', models.BooleanField(default=False)),
                ('required_qualification', models.CharField(max_length=256)),
                ('required_qualification_description', models.TextField()),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='job_postings', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['created_at'],
                'get_latest_by': 'last_modified_at',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='JobApplication',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=util.generate_uuid, editable=False, unique=True)),
                ('slug', models.SlugField(editable=False)),
                ('is_public', models.BooleanField(default=False, help_text='Enable to make record visible to all members')),
                ('is_shareable', models.BooleanField(default=False, help_text='Enable to make this record shareable on site')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('last_modified_at', models.DateTimeField(auto_now=True)),
                ('deleted_at', models.DateTimeField(editable=False, null=True)),
                ('force_delete', models.BooleanField(default=False, editable=False)),
                ('application_reason', models.TextField(null=True)),
                ('application_datetime', models.DateTimeField()),
                ('application_cv', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='job_application', to='profile_management.UserCV')),
                ('job_posting', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='job_applications', to='career.JobPosting')),
            ],
            options={
                'ordering': ['created_at'],
                'get_latest_by': 'last_modified_at',
                'abstract': False,
            },
        ),
    ]
