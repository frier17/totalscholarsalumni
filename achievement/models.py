from django.db import models
from django.contrib.contenttypes.fields import GenericRelation

from util import name_default, code_default, identifier_default, date_default, text_default, currency_default
from util.models import BaseModel, PublishMixin, SoftDeleteMixin, FileUploadMixin


class Achievements(FileUploadMixin, SoftDeleteMixin):
    title = models.CharField(**name_default)
    achievement_date = models.DateField(null=True)
    achievement_source = models.CharField(**name_default)
    descriptive_value = models.TextField()
    achievement_cause = models.TextField(**text_default)
    achiever = models.ForeignKey('util.BaseUser', on_delete=models.CASCADE, related_name='achievements')
    perceived_value = models.TextField(null=True)
    ratings = GenericRelation('social_network.UserComments', related_query_name='achievements')
    
    def publish(self, **kwargs):
        self.broadcasts = ['title', 'achievement_date', 'achievement_source', 
                           'descriptive_value', 'achievement_cause', 'achievement_references']
        return super(Achievements, self).publish(**kwargs)


class AchievementReference(PublishMixin, BaseModel):
    reference_title = models.CharField(**code_default)
    reference_full_name = models.CharField(**name_default)
    reference_address = models.TextField(**text_default)
    achievement = models.ForeignKey('Achievements', on_delete=models.DO_NOTHING,
                                    related_name='references')


class WorkExperience(SoftDeleteMixin):
    employer = models.CharField(**name_default)
    employment_start = models.DateField()
    employment_end = models.DateField(null=True, editable=True)
    job_title = models.CharField(**name_default)
    job_id = models.CharField(help_text='Unique Identifier for the job or role',
                              **name_default)
    staff_id = models.CharField(help_text='Unique identifier as a staff while in the '
                                          'specified job role', null=True,
                                **identifier_default)
    salary_range_min = models.DecimalField(**currency_default)
    salary_range_max = models.DecimalField(**currency_default)
    base_salary_amount = models.DecimalField(null=True, editable=True, **currency_default)
    referee_contact = models.TextField(help_text='Referees from the specified job is '
                                                 'advisable', null=True)
    job_supervisor_contact = models.TextField(null=True, help_text='Provide email of supervisor if possible')
    employee = models.ForeignKey('util.BaseUser', on_delete=models.DO_NOTHING,
                                 related_name='work_experiences')
    is_volunteer_role = models.BooleanField(default=False, null=True)
    ratings = GenericRelation('social_network.UserComments',
                              related_query_name='work_experience')

    def publish(self, **kwargs):
        self.broadcasts = '__all__'
        self.excluded_broadcasts = ['staff_id', 'job_id', 'salary_range_min',
                                    'salary_range_max', 'base_salary_amount',
                                    'referee_contact', 'job_supervisor_contact']
        return super(WorkExperience, self).publish(**kwargs)


class EducationHistory(SoftDeleteMixin):
    programme_start = models.DateField(**date_default)
    programme_end = models.DateField(**date_default)
    institution = models.CharField(**name_default)
    department = models.CharField(**name_default)
    faculty = models.CharField(**name_default)
    programme = models.CharField(**name_default)
    entry_level = models.CharField(**code_default)
    specialization = models.CharField(**name_default)
    awarded_degree = models.CharField(**code_default)
    awardee = models.ForeignKey('util.BaseUser', on_delete=models.CASCADE)
    ratings = GenericRelation('social_network.UserComments',
                              related_query_name='education_history')

    def publish(self, **kwargs):
        self.broadcasts = '__all__'
        return super(EducationHistory, self).publish(**kwargs)


class Qualification(PublishMixin, BaseModel):
    qualification_name = models.CharField(**name_default)
    qualification_code = models.CharField(
            null=True,
            help_text='Short name or code by which qualification is generally known. E.g BSc.',
            **code_default)
    description = models.TextField()
    awarding_institution = models.ForeignKey('util.Institution', on_delete=models.DO_NOTHING)
    is_academic_qualification = models.BooleanField(default=False)
    awardee = models.ForeignKey('util.BaseUser', on_delete=models.CASCADE)
    awarded_on = models.DateField(null=True)
    valid_till = models.DateField(null=True)
    ratings = GenericRelation('social_network.UserComments',
                              related_query_name='qualification')

    def publish(self, **kwargs):
        self.broadcasts = '__all__'
        return super(Qualification, self).publish(**kwargs)


class AcademicGraduation(PublishMixin, BaseModel):
    graduand = models.ForeignKey('util.BaseUser', on_delete=models.CASCADE, related_name='academic_graduation')
    graduating_level = models.CharField(**code_default)
    entry_year = models.DateField(**code_default)
    entry_level = models.CharField(**code_default)
    entry_academic_year = models.CharField(**name_default)
    graduating_year = models.DateField(**name_default)
    graduating_academic_year = models.CharField(**code_default)
    ratings = GenericRelation('social_network.UserComments',
                              related_query_name='academic_graduation')

    def publish(self, **kwargs):
        self.broadcasts = '__all__'
        return super(AcademicGraduation, self).publish(**kwargs)
