from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import DetailView, ListView, CreateView

from achievement import forms
from achievement import models
from util.views import DeleteAclMixin, UpdateAclMixin


class AcademicGraduationListView(LoginRequiredMixin, ListView):
    model = models.AcademicGraduation
    # paginate_by = 10
    template_name = 'achievement/academic_graduation_list.html'
    login_url = reverse_lazy('user-login')

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(AcademicGraduationListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(graduand=user)
            return queryset
        else:
            return super(AcademicGraduationListView, self).get_queryset().none()


class AcademicGraduationCreateView(LoginRequiredMixin, CreateView):
    model = models.AcademicGraduation
    form_class = forms.AcademicGraduationForm
    template_name = 'achievement/academic_graduation_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')

    def form_valid(self, form):
        form.instance.graduand = self.request.user
        return super(AcademicGraduationCreateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(AcademicGraduationCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        return reverse_lazy('achievement:academic-graduation-list')


class AcademicGraduationDetailView(LoginRequiredMixin, DetailView):
    model = models.AcademicGraduation
    template_name = 'achievement/academic_graduation_detail.html'
    login_url = reverse_lazy('user-login')

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(AcademicGraduationDetailView, self).get_context_data(**kwargs)


class AcademicGraduationUpdateView(UpdateAclMixin):
    model = models.AcademicGraduation
    form_class = forms.AcademicGraduationForm
    template_name = 'achievement/academic_graduation_form.html'
    user_field = 'graduand'
    initial = {}

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(AcademicGraduationUpdateView, self).get_object()

    def get_success_url(self):
        return reverse_lazy('achievement:academic-graduation-list')

    def get_initial(self):
        initial = super(AcademicGraduationUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class AcademicGraduationDeleteView(DeleteAclMixin):
    model = models.AcademicGraduation
    success_url = reverse_lazy('achievement:academic-graduation-list')
    template_name = 'achievement/academic_graduation_confirm_delete.html'
    user_field = 'graduand'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(AcademicGraduationDeleteView, self).get_context_data(**kwargs)


class AchievementReferenceListView(LoginRequiredMixin, ListView):
    model = models.AchievementReference
    # paginate_by = 10
    template_name = 'achievement/achievement_reference_list.html'
    login_url = reverse_lazy('user-login')

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(AchievementReferenceListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(achievement__achiever=user)
            return queryset
        else:
            return super(AchievementReferenceListView, self).get_queryset().none()


class AchievementReferenceCreateView(LoginRequiredMixin, CreateView):
    model = models.AchievementReference
    form_class = forms.AchievementReferenceForm
    template_name = 'achievement/achievement_reference_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(AchievementReferenceCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        return reverse_lazy('achievement:achievement-reference-list')


class AchievementReferenceDetailView(LoginRequiredMixin, DetailView):
    model = models.AchievementReference
    template_name = 'achievement/achievement_reference_detail.html'
    login_url = reverse_lazy('user-login')

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(AchievementReferenceDetailView, self).get_context_data(**kwargs)


class AchievementReferenceUpdateView(UpdateAclMixin):
    model = models.AchievementReference
    form_class = forms.AchievementReferenceForm
    template_name = 'achievement/achievement_reference_form.html'
    user_field = 'achievement__achiever'
    initial = {}

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(AchievementReferenceUpdateView, self).get_object()

    def get_success_url(self):
        return reverse_lazy('achievement:achievement-reference-list')

    def get_initial(self):
        initial = super(AchievementReferenceUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class AchievementReferenceDeleteView(DeleteAclMixin):
    model = models.AchievementReference
    success_url = reverse_lazy('achievement:achievement-reference-list')
    template_name = 'achievement/achievement_reference_confirm_delete.html'
    user_field = 'achievement__achiever'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(AchievementReferenceDeleteView, self).get_context_data(**kwargs)


class AchievementsListView(LoginRequiredMixin, ListView):
    model = models.Achievements
    # paginate_by = 10
    template_name = 'achievement/achievements_list.html'
    login_url = reverse_lazy('user-login')

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(AchievementsListView, self).get_queryset()
            # filter queryset for logged in user
            return queryset
        else:
            return super(AchievementsListView, self).get_queryset().none()


class AchievementsCreateView(LoginRequiredMixin, CreateView):
    model = models.Achievements
    form_class = forms.AchievementsForm
    template_name = 'achievement/achievements_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')

    def form_valid(self, form):
        form.instance.achiever = self.request.user
        return super(AchievementsCreateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(AchievementsCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        return reverse_lazy('achievement:achievements-list')


class AchievementsDetailView(LoginRequiredMixin, DetailView):
    model = models.Achievements
    template_name = 'achievement/achievements_detail.html'
    login_url = reverse_lazy('user-login')

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(AchievementsDetailView, self).get_context_data(**kwargs)


class AchievementsUpdateView(UpdateAclMixin):
    model = models.Achievements
    form_class = forms.AchievementsForm
    template_name = 'achievement/achievements_form.html'
    initial = {}
    user_field = 'achiever'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(AchievementsUpdateView, self).get_object()

    def get_success_url(self):
        return reverse_lazy('achievement:achievements-list')

    def get_initial(self):
        initial = super(AchievementsUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class AchievementsDeleteView(DeleteAclMixin):
    model = models.Achievements
    success_url = reverse_lazy('achievement:achievements-list')
    template_name = 'achievement/achievements_confirm_delete.html'
    user_field = 'achiever'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(AchievementsDeleteView, self).get_context_data(**kwargs)


class EducationHistoryListView(LoginRequiredMixin, ListView):
    model = models.EducationHistory
    # paginate_by = 10
    template_name = 'achievement/education_history_list.html'
    login_url = reverse_lazy('user-login')

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(EducationHistoryListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(awardee=user)
            return queryset
        else:
            return super(EducationHistoryListView, self).get_queryset().none()


class EducationHistoryCreateView(LoginRequiredMixin, CreateView):
    model = models.EducationHistory
    form_class = forms.EducationHistoryForm
    template_name = 'achievement/education_history_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')
    
    def form_valid(self, form):
        form.instance.awardee = self.request.user
        return super(EducationHistoryCreateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(EducationHistoryCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        return reverse_lazy('achievement:education-history-list')


class EducationHistoryDetailView(LoginRequiredMixin, DetailView):
    model = models.EducationHistory
    template_name = 'achievement/education_history_detail.html'
    login_url = reverse_lazy('user-login')

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(EducationHistoryDetailView, self).get_context_data(**kwargs)


class EducationHistoryUpdateView(UpdateAclMixin):
    model = models.EducationHistory
    form_class = forms.EducationHistoryForm
    template_name = 'achievement/education_history_form.html'
    initial = {}
    user_field = 'awardee'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(EducationHistoryUpdateView, self).get_object()

    def get_success_url(self):
        return reverse_lazy('achievement:education-history-list')

    def get_initial(self):
        initial = super(EducationHistoryUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class EducationHistoryDeleteView(DeleteAclMixin):
    model = models.EducationHistory
    success_url = reverse_lazy('achievement:education-history-list')
    template_name = 'achievement/education_history_confirm_delete.html'
    user_field = 'awardee'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(EducationHistoryDeleteView, self).get_context_data(**kwargs)


class QualificationListView(LoginRequiredMixin, ListView):
    model = models.Qualification
    # paginate_by = 10
    template_name = 'achievement/qualification_list.html'
    login_url = reverse_lazy('user-login')

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(QualificationListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(awardee=user)
            return queryset
        else:
            return super(QualificationListView, self).get_queryset().none()


class QualificationCreateView(LoginRequiredMixin, CreateView):
    model = models.Qualification
    form_class = forms.QualificationForm
    template_name = 'achievement/qualification_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')
    
    def form_valid(self, form):
        form.instance.awardee = self.request.user
        return super(QualificationCreateView, self).get_form(form)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(QualificationCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        return reverse_lazy('achievement:qualification-list')


class QualificationDetailView(LoginRequiredMixin, DetailView):
    model = models.Qualification
    template_name = 'achievement/qualification_detail.html'
    login_url = reverse_lazy('user-login')

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(QualificationDetailView, self).get_context_data(**kwargs)


class QualificationUpdateView(UpdateAclMixin):
    model = models.Qualification
    form_class = forms.QualificationForm
    template_name = 'achievement/qualification_form.html'
    initial = {}
    user_field = 'awardee'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(QualificationUpdateView, self).get_object()

    def get_success_url(self):
        return reverse_lazy('achievement:qualification-list')

    def get_initial(self):
        initial = super(QualificationUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class QualificationDeleteView(DeleteAclMixin):
    model = models.Qualification
    success_url = reverse_lazy('achievement:qualification-list')
    template_name = 'achievement/qualification_confirm_delete.html'
    user_field = 'awardee'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(QualificationDeleteView, self).get_context_data(**kwargs)


class WorkExperienceListView(LoginRequiredMixin, ListView):
    model = models.WorkExperience
    # paginate_by = 10
    template_name = 'achievement/work_experience_list.html'
    login_url = reverse_lazy('user-login')

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(WorkExperienceListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(employee=user)
            return queryset
        else:
            return super(WorkExperienceListView, self).get_queryset().none()


class WorkExperienceCreateView(LoginRequiredMixin, CreateView):
    model = models.WorkExperience
    form_class = forms.WorkExperienceForm
    template_name = 'achievement/work_experience_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')
    
    def form_valid(self, form):
        form.instance.employee = self.request.user
        return super(WorkExperienceCreateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(WorkExperienceCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        return reverse_lazy('achievement:work-experience-list')


class WorkExperienceDetailView(LoginRequiredMixin, DetailView):
    model = models.WorkExperience
    template_name = 'achievement/work_experience_detail.html'
    login_url = reverse_lazy('user-login')

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(WorkExperienceDetailView, self).get_context_data(**kwargs)


class WorkExperienceUpdateView(UpdateAclMixin):
    model = models.WorkExperience
    form_class = forms.WorkExperienceForm
    template_name = 'achievement/work_experience_form.html'
    initial = {}
    user_field = 'employee'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(WorkExperienceUpdateView, self).get_object()

    def get_success_url(self):
        return reverse_lazy('achievement:work-experience-list')

    def get_initial(self):
        initial = super(WorkExperienceUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class WorkExperienceDeleteView(DeleteAclMixin):
    model = models.WorkExperience
    success_url = reverse_lazy('achievement:work-experience-list')
    template_name = 'achievement/work_experience_confirm_delete.html'
    user_field = 'employee'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(WorkExperienceDeleteView, self).get_context_data(**kwargs)
