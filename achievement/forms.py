from django import forms

from achievement import models


class AcademicGraduationForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(AcademicGraduationForm, self).get_initial_for_field(field, field_name)

    def save(self, commit=True):
        super(AcademicGraduationForm, self).save(commit)

    class Meta:
        model = models.AcademicGraduation
        fields = '__all__'
        exclude = ['graduand']


class AchievementReferenceForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(AchievementReferenceForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.AchievementReference
        fields = '__all__'


class AchievementsForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(AchievementsForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.Achievements
        fields = '__all__'
        exclude = ['achiever']


class EducationHistoryForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(EducationHistoryForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.EducationHistory
        fields = '__all__'
        exclude = ['awardee']


class QualificationForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(QualificationForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.Qualification
        fields = '__all__'


class WorkExperienceForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(WorkExperienceForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.WorkExperience
        fields = '__all__'
