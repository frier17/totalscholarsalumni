from django.contrib import admin

from . import models

admin.register(models.AcademicGraduation)
admin.register(models.Achievements)
admin.register(models.WorkExperience)
admin.register(models.Qualification)
