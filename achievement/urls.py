
from django.urls import path
from achievement import views

app_name = 'achievement' 

urlpatterns = []

urlpatterns += (
    # urls for AcademicGraduation
    path('academic-graduation/', views.AcademicGraduationListView.as_view(), name='academic-graduation-list'),
    path('academic-graduation/create/', views.AcademicGraduationCreateView.as_view(), name='academic-graduation-create'),
    path('academic-graduation/detail/<str:slug>/', views.AcademicGraduationDetailView.as_view(), name='academic-graduation-detail'),
    path('academic-graduation/update/<str:slug>/', views.AcademicGraduationUpdateView.as_view(), name='academic-graduation-update'),
    path('academic-graduation/delete/<str:slug>/', views.AcademicGraduationDeleteView.as_view(), name='academic-graduation-delete')    
)


urlpatterns += (
    # urls for AchievementReference
    path('achievement-reference/', views.AchievementReferenceListView.as_view(), name='achievement-reference-list'),
    path('achievement-reference/create/', views.AchievementReferenceCreateView.as_view(), name='achievement-reference-create'),
    path('achievement-reference/detail/<str:slug>/', views.AchievementReferenceDetailView.as_view(), name='achievement-reference-detail'),
    path('achievement-reference/update/<str:slug>/', views.AchievementReferenceUpdateView.as_view(), name='achievement-reference-update'),
    path('achievement-reference/delete/<str:slug>/', views.AchievementReferenceDeleteView.as_view(), name='achievement-reference-delete')    
)


urlpatterns += (
    # urls for Achievements
    path('achievements/', views.AchievementsListView.as_view(), name='achievements-list'),
    path('achievements/create/', views.AchievementsCreateView.as_view(), name='achievements-create'),
    path('achievements/detail/<str:slug>/', views.AchievementsDetailView.as_view(), name='achievements-detail'),
    path('achievements/update/<str:slug>/', views.AchievementsUpdateView.as_view(), name='achievements-update'),
    path('achievements/delete/<str:slug>/', views.AchievementsDeleteView.as_view(), name='achievements-delete')    
)


urlpatterns += (
    # urls for EducationHistory
    path('education-history/', views.EducationHistoryListView.as_view(), name='education-history-list'),
    path('education-history/create/', views.EducationHistoryCreateView.as_view(), name='education-history-create'),
    path('education-history/detail/<str:slug>/', views.EducationHistoryDetailView.as_view(), name='education-history-detail'),
    path('education-history/update/<str:slug>/', views.EducationHistoryUpdateView.as_view(), name='education-history-update'),
    path('education-history/delete/<str:slug>/', views.EducationHistoryDeleteView.as_view(), name='education-history-delete')    
)


urlpatterns += (
    # urls for Qualification
    path('qualification/', views.QualificationListView.as_view(), name='qualification-list'),
    path('qualification/create/', views.QualificationCreateView.as_view(), name='qualification-create'),
    path('qualification/detail/<str:slug>/', views.QualificationDetailView.as_view(), name='qualification-detail'),
    path('qualification/update/<str:slug>/', views.QualificationUpdateView.as_view(), name='qualification-update'),
    path('qualification/delete/<str:slug>/', views.QualificationDeleteView.as_view(), name='qualification-delete')    
)


urlpatterns += (
    # urls for WorkExperience
    path('work-experience/', views.WorkExperienceListView.as_view(), name='work-experience-list'),
    path('work-experience/create/', views.WorkExperienceCreateView.as_view(), name='work-experience-create'),
    path('work-experience/detail/<str:slug>/', views.WorkExperienceDetailView.as_view(), name='work-experience-detail'),
    path('work-experience/update/<str:slug>/', views.WorkExperienceUpdateView.as_view(), name='work-experience-update'),
    path('work-experience/delete/<str:slug>/', views.WorkExperienceDeleteView.as_view(), name='work-experience-delete')    
)

