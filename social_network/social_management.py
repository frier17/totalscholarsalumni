from django.db import models
from django.urls import path
from django.utils.http import urlsafe_base64_decode
from django.views.generic import FormView, ListView

from .models import SocialNetwork, UserLikes


class FollowersView(ListView):
    template_name = 'social_network/followers.html'
    paginate_by = 10
    model = SocialNetwork

    def __init__(self):
        if self.model:
            self.queryset = self.model.objects.all()
        super(FollowersView, self).__init__()

    def get_queryset(self):
        queryset = super(FollowersView, self).get_queryset()
        # filter the queryset for current user
        return self.queryset.filter(leader=self.request.user)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(FollowersView, self).get_context_data(
                object_list=object_list,
                **kwargs)
        if self.request.user.is_authenticated and not self.kwargs.get('slug'):
            followers = self.queryset.filter(leader=self.request.user)
            context.update({'followers': followers})
        elif self.request.user.is_authenticated and self.kwargs.get('slug'):
            member_uuid = urlsafe_base64_decode(self.kwargs.get('slug')).decode()
            followers = self.queryset.filter(
                    (models.Q(leader__is_shareable=True) | models.Q(leader__is_public=True)),
                    follower=self.request.user,
                    leader__uuid=member_uuid)
            context.update({'followers': followers})
        return context


class LeadersView(ListView):
    template_name = 'social_network/leaders.html'
    paginate_by = 10

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(LeadersView, self).get_context_data(object_list=object_list, **kwargs)
        if self.request.user.is_authenticated and not self.kwargs.get('slug'):
            leaders = SocialNetwork.objects.filter(follower=self.request.user)
            context.update({'leaders': leaders})
        elif self.request.user.is_authenticated and self.kwargs.get('slug'):
            member_uuid = urlsafe_base64_decode(self.kwargs.get('slug')).decode()
            leaders = SocialNetwork.objects.filter(
                    models.Q(follower__is_shareable=True) | models.Q(follower__is_public=True),
                    follower__uuid=member_uuid
            )
            if self.request.user.has_permission(SocialNetwork):
                pass
            context.update({'leaders': leaders})
            return context


class UserLikesView(ListView):
    template_name = 'social_network/user_likes.html'
    paginate_by = 10
    model = UserLikes

    def get_queryset(self):
        return super(UserLikesView, self).get_queryset()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(UserLikesView, self).get_context_data(
                object_list=object_list,
                **kwargs)
        if self.request.user.is_authenticated:
            likes = UserLikes.objects.filter(user=self.request.user, like=True)
            dislikes = UserLikes.objects.filter(user=self.request.user, like=False)
            if likes or dislikes:
                context.update({'likes': likes, 'dislikes': dislikes})
        return context


class RatingAjaxView(FormView):
    template_name = 'social_network/user_achievements.html'


urlpatterns = [
        path('socials/followers/', FollowersView.as_view(), name='social-followers'),
        path('socials/followers/<str:slug>', FollowersView.as_view(),
             name='social-followers'),
        path('socials/leaders/', FollowersView.as_view(), name='social-leaders'),
        path('socials/leaders/<str:slug>', FollowersView.as_view(), name='social-leaders'),
]
