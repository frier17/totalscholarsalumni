from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import gettext_lazy as _

from util import datetime_default, text_default
from util.models import BaseModel, SoftDeleteMixin, PublishMixin


class SocialNetwork(SoftDeleteMixin):
    leader = models.ForeignKey('util.BaseUser', on_delete=models.DO_NOTHING,
                               related_name="followers_network")
    follower = models.ForeignKey('util.BaseUser', on_delete=models.DO_NOTHING,
                                 related_name="leaders_network")
    social_start = models.DateTimeField(**datetime_default)
    objects = models.Manager()

    def save(self, *args, **kwargs):
        if self.pk:
            raise RuntimeError('Cannot modify existing relationship')
        return super(SocialNetwork, self).save(*args, **kwargs)


class UserLikes(PublishMixin, BaseModel):
    like = models.BooleanField(default=True)
    rating = models.PositiveSmallIntegerField(
        help_text=_('Numeric value to rank a user. Default '
                    'range is set from 0 - 9'), default=0)
    content_object = GenericForeignKey()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveSmallIntegerField()
    user = models.ForeignKey('util.BaseUser', on_delete=models.DO_NOTHING)

    def save(self, *args, **kwargs):
        if self.rating > 10:
            self.rating = 10
        elif self.rating < 0:
            self.rating = 0
        return super(UserLikes, self).save(*args, **kwargs)


class UserComments(SoftDeleteMixin):
    comments = models.TextField(**text_default)
    user = models.ForeignKey('util.BaseUser', on_delete=models.CASCADE,
                             related_name='user_comments')
    object_id = models.PositiveSmallIntegerField()
    content_type = models.ForeignKey(ContentType, on_delete=models.DO_NOTHING)
    content_object = GenericForeignKey()

    def publish(self, **kwargs):
        return NotImplemented
