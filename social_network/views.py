from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import DetailView, ListView, CreateView
from django.db.models import Q

from social_network import forms
from social_network import models
from util.views import UpdateAclMixin, DeleteAclMixin, ReadOnlyMixin


class SocialNetworkListView(ListView):
    model = models.SocialNetwork
    # paginate_by = 10
    template_name = 'social_network/social_network_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user
        user = self.request.user
        if user.is_authenticated:
            queryset = super(SocialNetworkListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(Q(follower=user) | Q(leader=user))
            return queryset
        else:
            return super(SocialNetworkListView, self).get_queryset().none()


class SocialNetworkCreateView(LoginRequiredMixin, CreateView):
    model = models.SocialNetwork
    form_class = forms.SocialNetworkForm
    template_name = 'social_network/social_network_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(SocialNetworkCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('social_network:social_network-list')


class SocialNetworkDetailView(DetailView):
    model = models.SocialNetwork
    template_name = 'social_network/social_network_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(SocialNetworkDetailView, self).get_context_data(**kwargs)


class SocialNetworkUpdateView(ReadOnlyMixin):
    model = models.SocialNetwork
    form_class = forms.SocialNetworkForm
    template_name = 'social_network/social_network_form.html'
    initial = {}


class SocialNetworkDeleteView(ReadOnlyMixin):
    model = models.SocialNetwork
    success_url = reverse_lazy('social_network:social-network-list')
    template_name = 'social_network/social_network_confirm_delete.html'


class UserCommentsListView(ListView):
    model = models.UserComments
    # paginate_by = 10
    template_name = 'social_network/user_comments_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user
        user = self.request.user
        if user.is_authenticated:
            queryset = super(UserCommentsListView, self).get_queryset()
            # filter queryset for logged in user
            return queryset
        else:
            return super(UserCommentsListView, self).get_queryset().none()


class UserCommentsCreateView(LoginRequiredMixin, CreateView):
    model = models.UserComments
    form_class = forms.UserCommentsForm
    template_name = 'social_network/user_comments_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(UserCommentsCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('social_network:user_comments-list')


class UserCommentsDetailView(DetailView):
    model = models.UserComments
    template_name = 'social_network/user_comments_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserCommentsDetailView, self).get_context_data(**kwargs)


class UserCommentsUpdateView(UpdateAclMixin):
    model = models.UserComments
    form_class = forms.UserCommentsForm
    template_name = 'social_network/user_comments_form.html'
    initial = {}
    user_field = 'user'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(UserCommentsUpdateView, self).get_object()

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('social_network:user_comments-list')

    def get_initial(self):
        initial = super(UserCommentsUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class UserCommentsDeleteView(DeleteAclMixin):
    model = models.UserComments
    success_url = reverse_lazy('social_network:user-comments-list')
    template_name = 'social_network/user_comments_confirm_delete.html'
    user_field = 'user'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserCommentsDeleteView, self).get_context_data(**kwargs)


class UserLikesListView(ListView):
    model = models.UserLikes
    # paginate_by = 10
    template_name = 'social_network/user_likes_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(UserLikesListView, self).get_queryset()
            # filter queryset for logged in user
            return queryset
        else:
            return super(UserLikesListView, self).get_queryset().none()


class UserLikesCreateView(LoginRequiredMixin, CreateView):
    model = models.UserLikes
    form_class = forms.UserLikesForm
    template_name = 'social_network/user_likes_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(UserLikesCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('social_network:user_likes-list')


class UserLikesDetailView(DetailView):
    model = models.UserLikes
    template_name = 'social_network/user_likes_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserLikesDetailView, self).get_context_data(**kwargs)


class UserLikesUpdateView(UpdateAclMixin):
    model = models.UserLikes
    form_class = forms.UserLikesForm
    template_name = 'social_network/user_likes_form.html'
    initial = {}

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(UserLikesUpdateView, self).get_object()

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('social_network:user_likes-list')

    def get_initial(self):
        initial = super(UserLikesUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class UserLikesDeleteView(DeleteAclMixin):
    model = models.UserLikes
    success_url = reverse_lazy('social_network:user-likes-list')
    template_name = 'social_network/user_likes_confirm_delete.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserLikesDeleteView, self).get_context_data(**kwargs)
