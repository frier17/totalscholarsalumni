from django.urls import path

from social_network import views

app_name = 'social_network'

urlpatterns = []

urlpatterns += (
    # urls for SocialNetwork
    path('social-network/', views.SocialNetworkListView.as_view(), name='social-network-list'),
    path('social-network/create/', views.SocialNetworkCreateView.as_view(), name='social-network-create'),
    path('social-network/detail/<str:slug>/', views.SocialNetworkDetailView.as_view(), name='social-network-detail'),
    path('social-network/update/<str:slug>/', views.SocialNetworkUpdateView.as_view(), name='social-network-update'),
    path('social-network/delete/<str:slug>/', views.SocialNetworkDeleteView.as_view(), name='social-network-delete')
)

urlpatterns += (
    # urls for UserComments
    path('user-comments/', views.UserCommentsListView.as_view(), name='user-comments-list'),
    path('user-comments/create/', views.UserCommentsCreateView.as_view(), name='user-comments-create'),
    path('user-comments/detail/<str:slug>/', views.UserCommentsDetailView.as_view(), name='user-comments-detail'),
    path('user-comments/update/<str:slug>/', views.UserCommentsUpdateView.as_view(), name='user-comments-update'),
    path('user-comments/delete/<str:slug>/', views.UserCommentsDeleteView.as_view(), name='user-comments-delete')
)

urlpatterns += (
    # urls for UserLikes
    path('user-likes/', views.UserLikesListView.as_view(), name='user-likes-list'),
    path('user-likes/create/', views.UserLikesCreateView.as_view(), name='user-likes-create'),
    path('user-likes/detail/<str:slug>/', views.UserLikesDetailView.as_view(), name='user-likes-detail'),
    path('user-likes/update/<str:slug>/', views.UserLikesUpdateView.as_view(), name='user-likes-update'),
    path('user-likes/delete/<str:slug>/', views.UserLikesDeleteView.as_view(), name='user-likes-delete')
)
