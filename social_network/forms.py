
from django import forms
from social_network import models


class SocialNetworkForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(SocialNetworkForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.SocialNetwork
        fields = '__all__'      


class UserCommentsForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(UserCommentsForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.UserComments
        fields = '__all__'      


class UserLikesForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(UserLikesForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.UserLikes
        fields = '__all__'      

