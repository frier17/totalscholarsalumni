import itertools
import logging
import re as regex
import unicodedata
from datetime import datetime as dt
from typing import Sequence, Any, Mapping

from django import forms
from django.apps import apps
from django.conf.urls.static import static
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, PasswordResetView, PasswordResetDoneView, \
    PasswordResetConfirmView, PasswordResetCompleteView
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.core.validators import validate_email, ValidationError
from django.db import models
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect, render
from django.urls import path
from django.urls import reverse_lazy
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView, FormView, DetailView, CreateView

import messages as message_templates
import tsa.settings as settings
from career.models import JobPosting, JobApplication
from mentorship.models import MentoringProgramme
from profile_management.models import UserCV
from social_network.models import UserLikes, UserComments, SocialNetwork
from util import MOBILE_DIAL_CODES, context_data, current_timezone
from util.logging import flash_message
from util.models import BaseUser, Notification, NotificationResponse, UserUploadedDocument, Complaints
from util.signals import user_inquiry_signal
from util.views import AccessRedirectView
from volunteer.models import VolunteerCourse, VolunteerCourseApplication

logger = logging.getLogger(__name__)


def _process_search(request: Any, entity: Any, search: str, fields: Sequence = None, **kwargs) -> Any:
    conjunctions = {'after', 'also', 'although', 'and', 'as', 'because', 'before', 'but', 'by', 'case', 'either',
                    'even', 'for', 'if', 'in', 'lest', 'long', 'much', 'neither', 'nor', 'not', 'of', 'once', 'only',
                    'or', 'order', 'provided', 'since', 'so', 'soon', 'than', 'that', 'the', 'though', 'till', 'unless',
                    'until', 'what', 'when', 'whenever', 'where', 'wherever', 'whether', 'while', 'yet'}
    sep = ';.|-:'
    space = ' '
    words = []
    for s in sep:
        words += [x for x in str(search).split(s) if x not in conjunctions and space not in x]
    words += [x for x in str(search).split() if x not in conjunctions and space not in x]
    words = set(words)
    if fields and isinstance(fields, (list, tuple)):
        results = itertools.chain.from_iterable(
            itertools.filterfalse(lambda y: not bool(y),
                                  [entity.objects.filter(**{f'{field}__icontains': x}).distinct() for x in words for
                                   field in fields]))

    else:
        results = itertools.chain.from_iterable(
            itertools.filterfalse(
                lambda y: not bool(y), [entity.objects.filter(**{f'{x.name}__icontains': word}).distinct()
                                        for word in words
                                        for x in entity._meta.get_fields()
                                        if isinstance(x, (models.CharField, models.TextField))]
            ))

    return {'searches': set(results)}


def _process_rating(request: Any, entity: Any, **kwargs) -> Sequence:
    data = request.POST.copy()
    fellowship = None
    instance = None

    if data.get('rating_form') and request.user.is_authenticated:
        rating = 0
        try:
            rating = int(data.get('rating'))
        except TypeError:
            rating = 0
        except ValueError:
            rating = 0
        comments = data.get('comments')
        follow = data.get('follow')
        like = True if rating > 1 else False
        likes_instance = None
        comments_instance = None
        instance_urlsafe_uuid = data.get('instance')
        if instance_urlsafe_uuid:
            uuid = urlsafe_base64_decode(instance_urlsafe_uuid).decode()
            instance = entity.objects.filter(uuid=uuid).first()
            # Generate the rating and comment for the target item using the current logged in user
            if instance:
                likes_instance = UserLikes.objects.filter(object_id=instance.id, user=request.user).exists()
                if not likes_instance:
                    likes_instance = UserLikes(like=like, user=request.user, rating=rating, content_object=instance)
                    likes_instance.save()

                comments_instance = UserComments(comments=comments, user=request.user, content_object=instance)
                comments_instance.save()

        if follow:
            fellowship = SocialNetwork.objects.create(leader=instance, follower=request.user,
                                                      social_start=current_timezone(dt.now()))
        if fellowship or likes_instance or comments_instance:
            flash_message(request, 'Rating of member has been saved')
        else:
            flash_message(request, 'Unable to save rating')

        return comments_instance, likes_instance


def _process_time_search(request, entity, get_context, background_data, **kwargs):
    data = request.POST.copy()
    result = None
    year = data.get('year')
    month = data.get('month')
    start = data.get('start')
    end = data.get('end')
    reference = data.get('reference')
    filters = data.get('filters')  # @todo: make dictionary of filters. perform safe search
    attr_search = data.get('attr_search')

    if filters:
        tokens = filters.split(', ;')
        try:
            words = []
            for w in tokens:
                words += w.split('=')
            idx = int(len(words) / 2)
            v1 = words[:idx]
            v2 = words[idx:]
            _filters = dict(zip(v1, v2))
            model_fields = [x.name for x in entity._meta.get_fields()]
            _keys = _filters.keys()
            params = dict()
            for key in _keys:
                if key in model_fields:
                    params[key] = _filters[key]
            result = get_context(queryset=None, year=year, month=month, start=start, end=end,
                                 reference=reference, filters=params, attr_search=attr_search)

        except ValueError:
            raise
        except Exception:
            raise
    else:
        result = get_context(queryset=None, year=year, month=month, start=start, end=end,
                             reference=reference, attr_search=attr_search)

    if background_data and isinstance(background_data, dict):
        result.update(background_data)
    return result


def logout_view(request):
    logout(request)
    return redirect('tsa-home')


class LoginForm(AuthenticationForm):
    username = forms.CharField(
        label=_('Email or Username'),
        widget=forms.TextInput(attrs={'autofocus': True}),
        error_messages={'required': _('Provide a valid email or username')},
        max_length=100,
        min_length=3
    )
    password = forms.CharField(label=_('Password'), widget=forms.PasswordInput,
                               max_length=25,
                               error_messages={
                                   'required': _(
                                       'Comments or inquiry should not be empty'),
                                   'max_length': _(
                                       'Number of approved characters has been '
                                       'exceeded. Kindly contact our admins using '
                                       'email or mobile provided. '
                                       'You may also check out the FAQ section '
                                       'for possible answers to your inquiry'),
                               })

    def __init__(self, request=None, *args, **kwargs):
        self._password = None
        self._username = None
        super(LoginForm, self).__init__(request, *args, **kwargs)

    def clean(self):
        username_or_email = self.cleaned_data.get('username')
        is_email = False
        is_mobile = False
        is_username = False

        if username_or_email:
            try:
                validate_email(username_or_email)
                pattern = r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'
                is_email = regex.search(pattern, username_or_email)
            except ValidationError:
                username_or_email = unicodedata.normalize('NFKC', username_or_email)
                is_username = regex.search(r'\w+', username_or_email)
                is_mobile = regex.search(r'[()0-9]', username_or_email)

        password = self.cleaned_data.get('password')
        self._password = password

        if username_or_email is not None and password:
            if is_email:
                try:
                    _user = BaseUser.objects.get(models.Q(email=username_or_email) | models.Q(
                        work_id=username_or_email))
                    self._username = _user.username
                    self.user_cache = authenticate(
                        self.request,
                        username=self._username,
                        password=self._password)
                except ObjectDoesNotExist:
                    logger.exception(message_templates.user_does_not_exist.format(
                        username=self._username,
                        email=username_or_email,
                        mobile=None
                    ))

            elif is_username:
                self._username = username_or_email
                self.user_cache = authenticate(
                    self.request,
                    username=self._username,
                    password=password)
            elif is_mobile:
                try:
                    _user = BaseUser.objects.get(mobile=username_or_email)
                    self._username = _user.username
                    self.user_cache = authenticate(
                        self.request,
                        username=self._username,
                        password=password)
                except ObjectDoesNotExist:
                    logger.exception(message_templates.user_does_not_exist.format(
                        username=self._username,
                        email=None,
                        mobile=username_or_email
                    ))
            else:
                raise self.get_invalid_login_error()
            if self.user_cache is None:
                raise self.get_invalid_login_error()
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data


class PinLoginForm(LoginForm):
    dial_code = forms.ChoiceField(label=_('Country Dial Code'),
                                  help_text=_('Select country dial code'),
                                  choices=MOBILE_DIAL_CODES,
                                  error_messages={
                                      'required': _('Select a valid dial code'),
                                      'invalid_choice': _('Selected value is invalid')
                                  })
    username = forms.CharField(max_length=20, min_length=7, empty_value='',
                               label=_('Mobile'),
                               widget=forms.TextInput, help_text=_('Enter mobile number '
                                                                   'without dial code'),
                               error_messages={
                                   'required': _('Provide valid mobile'),
                                   'max_length': _(
                                       'Mobile number exceeds maximum number of '
                                       'digits'),
                                   'min_length': _(
                                       'Mobile number below minimum number of '
                                       'digits')
                               })
    password = forms.CharField(
        label=_('Pin'),
        help_text=_('Enter PIN'),
        widget=forms.PasswordInput,
        max_length=6,
        error_messages={
            'required': _('Access pin should not be empty'),
            'max_length': _('Number of approved digits has been exceeded.'),
        })
    field_order = ['dial_code', 'username', 'password']

    @property
    def clean(self):
        pin = self.cleaned_data['password']
        mobile = "(%s) %s" % (self.cleaned_data['dial_code'], self.cleaned_data['username'])
        try:
            if BaseUser.objects.filter(mobile=mobile, pin=pin):
                user = BaseUser.objects.filter(mobile=mobile, pin=pin)[0]
                self._username = user.username
                self._password = user.password
        except ObjectDoesNotExist:
            logger.exception(message_templates.user_does_not_exist.format(
                username=self._username,
                email=None,
                mobile=mobile
            ))

        return super(PinLoginForm, self).clean


class StaffLoginForm(LoginForm):
    username = forms.CharField(max_length=20, min_length=7, empty_value='',
                               label=_('IGG'),
                               widget=forms.TextInput, help_text=_('Enter IGG or Staff ID. Only registered or '
                                                                   'approved TEPNG and TSA staff may login'),
                               error_messages={
                                   'required': _('Provide valid IGG or TSA staff work ID'),
                                   'max_length': _(
                                       'IGG or Work ID exceeds maximum number of '
                                       'digits'),
                                   'min_length': _(
                                       'IGG or Work ID is below minimum number of '
                                       'digits')
                               })
    field_order = ['username', 'password']


class ContactForm(forms.Form):
    email = forms.EmailField(
        label=_('Email'),
        help_text=_('Provide a valid email by which we may contact you'),
        error_messages={'required': _('Provide a valid email')})
    dial_code = forms.ChoiceField(label=_('Country Dial Code'),
                                  help_text=_('Select country dial code'),
                                  choices=MOBILE_DIAL_CODES,
                                  required=False,
                                  error_messages={
                                      'required': _('Select a valid dial code'),
                                      'invalid_choice': _('Selected value is invalid')
                                  })
    mobile = forms.CharField(
        max_length=20,
        min_length=7,
        empty_value='',
        label=_('Mobile'),
        widget=forms.TextInput,
        required=False,
        help_text=_('Enter mobile number without dial code'),
        error_messages={
            'max_length': _('Mobile number exceeds maximum number of digits'),
            'min_length': _('Mobile number below minimum number of digits')
        })
    inquiry = forms.CharField(
        label=_('Comments, Inquiry or Suggestions'),
        help_text=_('Enter your comments, inquiry or suggestions you may have for TSA'),
        widget=forms.Textarea,
        max_length=250,
        error_messages={
            'required': _('Comments or inquiry should not be empty'),
            'max_length': _('Number of approved characters has been exceeded. '
                            'Kindly contact our admins using email or mobile '
                            'provided. You may also check out the FAQ section '
                            'for possible answers to your inquiry')
        })


class UserPasswordResetView(PasswordResetView):
    form_class = PasswordResetForm
    extra_email_context = {'event_time': dt.now()}


class UserPasswordResetConfirmView(PasswordResetConfirmView):

    def get_user(self, uidb64):
        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            user = BaseUser.objects.get(uuid=uid)
        except (TypeError, ValueError, OverflowError, ObjectDoesNotExist, ValidationError):
            user = None
        return user


class HomeView(TemplateView):
    template_name = 'index.html'


class NotificationView(TemplateView):
    template_name = 'feeds/user/notifications.html'

    def get_context_data(self, **kwargs):
        context = super(NotificationView, self).get_context_data(**kwargs)
        slug = self.kwargs.get('slug')
        if self.request.user.is_authenticated:
            user_uuid = self.request.user.uuid
            try:
                user = self.request.user
                context.update(context_data.get_notifications(
                    user=user,
                    exclude={'header__contains': [
                        'USER_LOGIN',
                        'USER_LOGOUT',
                        'USER_PASSWORD_SERVICE',
                        'USER_AUTH_SERVICE']}))
            except (ObjectDoesNotExist, MultipleObjectsReturned):
                flash_message(request=self.request, message='Invalid user request performed')
        else:
            flash_message(request=self.request,
                          message='Anonymous user does not have any notification. '
                                  'Do log in to access any notification')

        return context

    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        shareable = True if data.get('is_shareable') else False
        public = True if data.get('is_public') else False
        uuid = urlsafe_base64_decode(data.get('notification')).decode()
        notice = Notification.objects.get(uuid=uuid)

        if self.request.user.is_authenticated and notice:
            instance = NotificationResponse.objects.create(
                acknowledgement=data.get('acknowledgement'),
                notification=notice,
                is_shareable=shareable,
                is_public=public,
                recipient=self.request.user
            )
            if instance:
                flash_message(request=request, message='Added response to notification')
            else:
                flash_message(request=request, message='Unable to add response to notification. '
                                                       'Please contact admin for further assistance')
        return render(request, template_name=self.template_name, context=self.get_context_data())


class ContactResponseView(TemplateView):
    template_name = 'contact_autoresponse.html'


class AjaxResponseMixin:
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """

    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super().form_valid(form)
        remote_addr = self.request.META.get('X-FORWARDED-FOR') or \
                      self.request.META.get('REMOTE_ADDRESS')
        referer = self.request.META.get('referer')
        reporter = None
        if self.request.user.is_authenticated:
            reporter = self.request.user.urlsafe_uuid

        if self.request.is_ajax():
            slug = self.request.GET.get('uidb64')
            report = self.request.POST.copy()
            career = JobPosting.objects.filter(models.Q(slug=slug) | models.Q(uuid=slug)).first()
            if career:
                Complaints.objects.create(
                    reporter=reporter,
                    comments=report.get('comments'),
                    rating=report.get('rating'),
                    referer=referer,
                    remote_addr=remote_addr
                )
                data = {'id': career.slug, 'item': career.__class__.__name__}
            else:
                program = MentoringProgramme.objects.filter(models.Q(slug=slug) | models.Q(uuid=slug)).first()
                if program:
                    Complaints.objects.create(
                        reporter=reporter,
                        comments=report.get('comments'),
                        rating=report.get('rating'),
                        referer=referer,
                        remote_addr=remote_addr
                    )
                    data = {'id': program.slug, 'item': program.__class__.__name__}
                else:
                    volunteering = VolunteerCourse.objects.filter(models.Q(slug=slug) | models.Q(uuid=slug)).first()
                    if volunteering:
                        Complaints.objects.create(
                            reporter=reporter,
                            comments=report.get('comments'),
                            rating=report.get('rating'),
                            referer=referer,
                            remote_addr=remote_addr
                        )
                        data = {'id': volunteering.slug, 'item': volunteering.__class__.__name__}
            return JsonResponse(data)
        else:
            return response

    def get(self, request, *args, **kwargs):
        remote_addr = request.META.get('X-FORWARDED-FOR') or request.META.get('REMOTE_ADDRESS')
        referer = request.META.get('referer')
        reporter = None
        data = {}
        if request.user.is_authenticated:
            reporter = request.user.urlsafe_uuid
        if request.is_ajax():
            slug = request.GET.get('uidb64')
            career = JobPosting.objects.filter(models.Q(slug=slug) | models.Q(uuid=slug)).first()
            if career:
                Complaints.objects.create(
                    reporter=reporter,
                    comments='Reported as spam',
                    rating='UNC',
                    referer=referer,
                    remote_addr=remote_addr
                )
                data = {'id': career.slug, 'item': career.__class__.__name__}
            else:
                program = MentoringProgramme.objects.filter(models.Q(slug=slug) | models.Q(uuid=slug)).first()
                if program:
                    Complaints.objects.create(
                        reporter=reporter,
                        comments='Reported as spam',
                        rating='UNC',
                        referer=referer,
                        remote_addr=remote_addr
                    )
                    data = {'id': program.slug, 'item': program.__class__.__name__}
                else:
                    volunteering = VolunteerCourse.objects.filter(models.Q(slug=slug) | models.Q(uuid=slug)).first()
                    if volunteering:
                        Complaints.objects.create(
                            reporter=reporter,
                            comments='Reported as spam',
                            rating='UNC',
                            referer=referer,
                            remote_addr=remote_addr
                        )
                        data = {'id': volunteering.slug, 'item': volunteering.__class__.__name__}
            return JsonResponse(data)


class ReportView(AjaxResponseMixin, CreateView):
    model = Complaints

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER')

    def form_valid(self, form):
        if form.is_valid():
            form.remote_addr = self.request.META.get('X-FORWARDED-FOR') or \
                               self.request.META.get('REMOTE_ADDRESS')
            form.referer = self.request.META.get('HTTP_REFERER')
            user = self.request.user
            if user.is_authenticated:
                form.reporter = user.urlsafe_uuid
            return super(ReportView, self).form_valid(form)


class AboutView(TemplateView):
    template_name = 'about.html'


class EntityView(TemplateView):
    template_name = 'feeds/public/entities.html'
    entity = None
    _entity_cache = dict()
    queryset = None
    context_function = None

    def __init__(self, *args, **kwargs):
        super(EntityView, self).__init__(*args, **kwargs)
        self._entity_cache = dict()
        self._entity_cache['search_attributes'] = {}
        self._entity_cache['search_result'] = False

    def get_context_data(self, **kwargs):
        context = super(EntityView, self).get_context_data(**kwargs)
        self._entity_cache.update(context)
        if not self._entity_cache.get('objects'):
            self._entity_cache.update(context)

        return self._entity_cache

    def post(self, request, *args, **kwargs):
        result = None
        if callable(self.context_function):
            result = _process_time_search(request, entity=self.entity, get_context=self.context_function,
                                          background_data=self._entity_cache)
            if result:
                result['search_result'] = True
        return render(request, template_name=self.template_name, context=result, status=302)


class CareersView(EntityView):
    template_name = 'feeds/public/careers.html'
    entity = JobPosting
    queryset = JobPosting.objects.filter(models.Q(is_public=True) | models.Q(is_shareable=True)).select_related(
        'source')

    def __init__(self, *args, **kwargs):
        super(CareersView, self).__init__(*args, **kwargs)
        self._entity_cache['search_attributes'] = {'created_at': 'created at'}

    def get_context_data(self, **kwargs):
        context = super(CareersView, self).get_context_data(**kwargs)
        stats = dict()
        if not self._entity_cache.get('objects'):
            self._entity_cache.update(context_data.get_job_postings(self.queryset))
            self._entity_cache.update(context)

        for record in self.queryset:
            stats.update({record.urlsafe_uuid: context_data.get_likes(instance=record, user=self.request.user)})
            self._entity_cache.update({'statistics': stats})

        return self._entity_cache

    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        if request.FILES:
            data.update(request.FILES.copy())

        result = None
        if data.get('time_search_form'):
            result = _process_time_search(request, entity=self.entity, get_context=context_data.get_job_postings,
                                          background_data=self._entity_cache)
            if result:
                result['search_result'] = True
        elif data.get('search_form'):
            search = data.get('job_search')
            result = _process_search(request=request, entity=JobPosting, search=search)
            if result:
                result['search_result'] = True
        elif data.get('job_application_form'):
            job_posting_uuid = urlsafe_base64_decode(data.get('job_posting_instance')).decode()
            job_posting = JobPosting.objects.get(uuid=job_posting_uuid)
            job_application = JobApplication(
                applicant=request.user,
                job_posting=job_posting,
                application_reason=data.get('application_reason'),
                application_datetime=current_timezone(dt.now())
            )

            if data.get('user_cv_instance') and request.user.is_authenticated:
                cv_uuid = urlsafe_base64_decode(data.get('user_cv_instance')).decode()
                cv_instance = UserCV.objects.filter(uuid=cv_uuid).last()
                if cv_instance:
                    job_application.application_cv = cv_instance

            if data.get('application_document'):
                user_document = UserUploadedDocument.objects.create(
                    media=data.get('application_document'),
                    owner=self.request.user
                )
                job_application.application_document = user_document

            if job_application:
                job_application.save()
                flash_message(request, message='Job application has been saved')
            else:
                flash_message(request,
                              message='Unable to save job application at the moment. Please contact administrator for '
                                      'assistance')
            result = self.get_context_data()
        elif data.get('entity_rating_form') and self.request.user.is_authenticated:
            _prepare_statistics(data, self.request, app='career')
            result = self.get_context_data()

        return render(request, template_name=self.template_name, context=result, status=302)


class ProgrammeView(EntityView):
    template_name = 'feeds/public/programmes.html'
    entity = MentoringProgramme
    queryset = MentoringProgramme.objects.all()

    def __init__(self, *args, **kwargs):
        super(ProgrammeView, self).__init__(*args, **kwargs)
        self._entity_cache['search_attributes'] = {'programme_start': 'programme start',
                                                   'programme_end': 'programme end'}

    def get_context_data(self, **kwargs):
        context = super(ProgrammeView, self).get_context_data(**kwargs)
        if not self._entity_cache.get('objects'):
            self._entity_cache.update(
                context_data.get_programme(queryset=self.queryset))
            self._entity_cache.update(context)
        return self._entity_cache

    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        result = None
        if data.get('time_search_form'):
            result = _process_time_search(request, entity=self.entity, get_context=context_data.get_programme,
                                          background_data=self._entity_cache)
            if result:
                result['search_result'] = True
        elif data.get('search_form'):
            search = data.get('search')
            result = _process_search(request=request, entity=MentoringProgramme, search=search)
            if result:
                result['search_result'] = True
        elif data.get('entity_rating_form') and self.request.user.is_authenticated:
            _prepare_statistics(data, self.request, app='mentorship')
            result = self.get_context_data()

        return render(request, template_name=self.template_name, context=result, status=302)


class VolunteersView(EntityView):
    template_name = 'feeds/public/volunteers.html'
    entity = VolunteerCourse
    queryset = VolunteerCourse.objects.all()

    def __init__(self, *args, **kwargs):
        super(VolunteersView, self).__init__(*args, **kwargs)
        self._entity_cache['search_attributes'] = {'course_start': 'course start',
                                                   'course_end': 'course end'}

    def get_context_data(self, **kwargs):
        context = super(VolunteersView, self).get_context_data(**kwargs)
        stats = dict()
        if not self._entity_cache.get('objects'):
            self._entity_cache.update(context_data.get_volunteer_courses())
            self._entity_cache.update(context)

            for record in self.queryset:
                stats.update({record.urlsafe_uuid: context_data.get_likes(instance=record, user=self.request.user)})
            self._entity_cache.update({'statistics': stats})
        return self._entity_cache

    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        result = None
        if data.get('time_search_form'):
            result = _process_time_search(request, entity=self.entity, get_context=context_data.get_volunteer_courses,
                                          background_data=self._entity_cache)
        elif data.get('volunteer_form') and request.user.is_authenticated:
            instance = None
            is_shareable = True if data.get('is_shareable') else False
            is_public = True if data.get('is_public') else False
            instance_urlsafe_uuid = data.get('instance')
            if instance_urlsafe_uuid:
                uuid = urlsafe_base64_decode(instance_urlsafe_uuid).decode()
                instance = VolunteerCourse.objects.filter(uuid=uuid).first()
                instance_exist = VolunteerCourseApplication.objects.filter(volunteer=request.user,
                                                                           volunteer_course=instance).exists()
                if not instance_exist:
                    application = VolunteerCourseApplication.objects.create(
                        volunteer=request.user,
                        volunteer_course=instance,
                        application_datetime=current_timezone(dt.now()),
                        is_public=is_public,
                        is_shareable=is_shareable
                    )
                    if application:
                        flash_message(request, 'Registered application for the selected volunteer course')
                    else:
                        flash_message(request, 'Unable to registered application for the selected volunteer course.')
                        if instance_exist:
                            flash_message(request, 'User already has volunteered to the selected course')

            result = context_data.get_volunteer_courses()

        elif data.get('search_form'):
            search = data.get('search')
            result = _process_search(request=request, entity=VolunteerCourse, search=search)
            if result:
                result['search_result'] = True

        elif data.get('entity_rating_form') and self.request.user.is_authenticated:
            _prepare_statistics(data, self.request, app='volunteer')
            result = self.get_context_data()

        if result:
            result['search_result'] = True

        return render(request, template_name=self.template_name, context=result, status=302)


class PublicFeedsView(TemplateView):
    template_name = 'public_feeds.html'


class UserFeedsView(TemplateView):
    template_name = 'notifications.html'
    slug_field = 'uuid'


class ContactFormView(FormView):
    success_url = reverse_lazy('contact-response')
    form_class = ContactForm
    template_name = 'contact_page.html'

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():

            remote_addr = self.request.META.get('X-FORWARDED-FOR') or \
                          self.request.META.get('REMOTE_ADDRESS')
            referer = self.request.META.get('referer')
            url = self.request.META.get('url')

            if hasattr(self.request, 'user'):
                user = self.request.user
                if user.is_authenticated:
                    user_inquiry_signal.send(
                        sender=self.__class__,
                        recipient=[form.cleaned_data['email'], user.email],
                        remote_addr=remote_addr,
                        referer=referer,
                        url=url,
                        asctime=dt.now(),
                        message=form.cleaned_data['inquiry'],
                        user=user
                    )
                else:
                    user_inquiry_signal.send(
                        sender=self.__class__,
                        recipient=[form.cleaned_data['email']],
                        remote_addr=remote_addr,
                        referer=referer,
                        url=url,
                        asctime=dt.now(),
                        message=form.cleaned_data['inquiry']
                    )
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class UserLoginView(LoginView):
    template_name = 'login.html'
    authentication_form = LoginForm
    cached_user = None

    def get_success_url(self):
        return reverse_lazy(
            'user-profile',
            kwargs={'slug': self.cached_user.urlsafe_uuid}
        )

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse_lazy('tsa-home'))
        return super(UserLoginView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        self.cached_user = form.get_user()
        login(self.request, self.cached_user)
        return HttpResponseRedirect(self.get_success_url())


class UserPinLoginView(UserLoginView):
    template_name = 'pin_login.html'
    authentication_form = PinLoginForm


class StaffLoginView(UserLoginView):
    template_name = 'staff_login.html'
    authentication_form = StaffLoginForm


class AlumniView(TemplateView):
    template_name = 'feeds/public/members.html'

    def get_context_data(self, **kwargs):
        queryset = BaseUser.objects.all()
        params = dict()
        if self.request.POST:
            params = self.request.POST.copy()
        if self.request.GET:
            params.update(self.request.GET.copy())
        context = super(AlumniView, self).get_context_data(**kwargs)

        stats = dict()
        for record in queryset:
            stats.update({record.urlsafe_uuid: context_data.get_likes(instance=record, user=None)})
        context.update({'statistics': stats})
        context.update(context_data.get_members(
            queryset,
            year=params.get('year'),
            month=params.get('month')),
            start=params.get('start'),
            end=params.get('end'),
            reference=params.get('reference'),
            filters=params.get('filters')
        )
        return context

    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        result = None
        fellowship = None
        count_likes = 0
        count_comments = 0
        count_ratings = 0
        instance = None
        instance_urlsafe_uuid = data.get('instance')
        if instance_urlsafe_uuid:
            uuid = urlsafe_base64_decode(instance_urlsafe_uuid).decode()
            instance = BaseUser.objects.filter(uuid=uuid).first()

        if data.get('time_search_form'):
            year = data.get('year')
            month = data.get('month')
            start = data.get('start')
            end = data.get('end')
            reference = data.get('reference')
            filters = data.get('filters')  # @todo: make dictionary of filters. perform safe search

            if filters:
                tokens = filters.split(', ;')
                try:
                    words = []
                    for w in tokens:
                        words += w.split('=')
                    idx = int(len(words) / 2)
                    v1 = words[:idx]
                    v2 = words[idx:]
                    _filters = dict(zip(v1, v2))
                    model_fields = [x.name for x in BaseUser._meta.get_fields()]
                    _keys = _filters.keys()
                    params = dict()
                    for key in _keys:
                        if key in model_fields:
                            params[key] = _filters[key]
                    result = context_data.get_members(queryset=None, year=year, month=month, start=start, end=end,
                                                      reference=reference, filters=params)

                except ValueError:
                    raise
                except Exception:
                    raise
            else:
                result = context_data.get_members(queryset=None, year=year, month=month, start=start, end=end,
                                                  reference=reference)

        if data.get('rating_form') and self.request.user.is_authenticated:
            print('Comment box')
            rating = 0
            try:
                rating = int(data.get('rating'))
            except TypeError:
                rating = 0
            except ValueError:
                rating = 0
            comments = data.get('comments')
            follow = data.get('follow')
            like = True if rating > 1 else False
            likes_instance = None
            comments_instance = None
            # Generate the rating and comment for the target item using the current logged in user
            if instance:
                existing_likes = UserLikes.objects.filter(member__uuid=instance.uuid, object_id=instance.id,
                                                          user=self.request.user).exists()
                if not existing_likes:
                    likes_instance = UserLikes(like=like, user=self.request.user, rating=rating,
                                               content_object=instance)
                    likes_instance.save()

                comments_instance = UserComments(comments=comments, user=self.request.user, content_object=instance)
                comments_instance.save()

                result = self.get_context_data(**kwargs)

            if follow:
                fellowship = SocialNetwork.objects.create(leader=instance, follower=self.request.user,
                                                          social_start=dt.utcnow())
            if fellowship or likes_instance or comments_instance:
                flash_message(self.request, 'Rating of member has been saved')
            else:
                flash_message(self.request, 'Unable to save rating')

        return render(request, template_name=self.template_name, context=result, status=302)


class UserProfileView(LoginRequiredMixin, DetailView):
    model = BaseUser
    slug_field = 'uuid'
    template_name = 'user_profile_page.html'
    context_object_name = 'object'
    login_url = reverse_lazy('user-login')

    def get_object(self, queryset=None):
        slug = urlsafe_base64_decode(self.kwargs.get(self.slug_url_kwarg)).decode()
        queryset = self.model.objects.filter(uuid=slug).first()
        return queryset

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        context = self.get_context_data(object=instance)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        # @todo: Set the member if the slug is provided and different from login user.
        context = {}
        member = kwargs.get('object')

        user = self.request.user
        can_view = False

        if member:
            related_follower = SocialNetwork.objects.filter(models.Q(follower=member) & models.Q(leader=user))
            related_leader = SocialNetwork.objects.filter(models.Q(follower=member) & models.Q(leader=user))
            if related_leader or related_follower or member == user or member.is_public or \
                    member.is_shareable:
                can_view = True

        if can_view:
            # Achievement context
            context.update(
                {'academic_graduations': context_data.get_academic_graduation(user=user, member=member).get(
                    'objects')})
            context.update({'achievements': context_data.get_achievements(user=user, member=member).get('objects')})
            context.update(
                {'education_histories': context_data.get_education_history(user=user, member=member).get(
                    'objects')})
            context.update(
                {'qualifications': context_data.get_qualification(user=user, member=member).get('objects')})
            context.update(
                {'work_experiences': context_data.get_work_experience(user=user, member=member).get('objects')})
            # Career context
            context.update(
                {'job_applications': context_data.get_job_applications(user=user, member=member).get('objects')})
            # Mentoring context
            context.update({'trainings': context_data.get_trainings(user=user, member=member).get('objects')})
            # Profile Management context
            context.update({'community': context_data.get_community(user=user, member=member).get('objects')})
            context.update(
                {'professional_associations': context_data.get_associations(user=user, member=member).get(
                    'objects')})
            context.update({'scholarships': context_data.get_scholarships(user=user, member=member).get('objects')})
            context.update({'testimonials': context_data.get_testimonials(user=user, member=member).get('objects')})
            context.update({'user_cvs': context_data.get_user_cvs(user=user, member=member).get('objects')})
            context.update(
                {'user_skill_sets': context_data.get_user_skillsets(user=user, member=member).get('objects')})
            # Social network context
            context.update({'followers': context_data.get_followers(user=user, member=member).get('objects')})
            context.update({'leaders': context_data.get_leaders(user=user, member=member).get('objects')})
            # Util context
            context.update(context_data.get_user_avatar(user=user))
            context.update(
                {'service_responses': context_data.get_service_responses(user=user, member=member).get('objects')})
            # Volunteer context
            context.update(
                {'volunteer_courses': context_data.get_user_volunteer_courses(user=user, member=member).get(
                    'objects')})
            return context
        return context


class UserProfileManagementView(LoginRequiredMixin, TemplateView):
    template_name = 'user_profile_management_page.html'
    login_url = reverse_lazy('user-login')

    def get(self, request, *args, **kwargs):
        context = super(UserProfileManagementView, self).get_context_data(**kwargs)

        if request.user.is_authenticated:
            return render(request, template_name=self.template_name, context=context, status=200)
        else:
            flash_message(request, message_templates.invalid_user_request)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def _prepare_statistics(data: Mapping, request: Any, app: str):
    rating = 0
    instance_urlsafe_uuid = data.get('instance')
    instance = None
    entity = apps.get_model(app, data.get('entity'))
    if instance_urlsafe_uuid:
        uuid = urlsafe_base64_decode(instance_urlsafe_uuid).decode()
        instance = entity.objects.filter(uuid=uuid).first()

    try:
        rating = int(data.get('rating'))
    except TypeError:
        rating = 0
    except ValueError:
        rating = 0
    comments = data.get('comments')
    like = True if rating > 1 else False
    likes_instance = None
    comments_instance = None
    # Generate the rating and comment for the target item using the current logged in user
    if instance:
        liked_entity = ContentType.objects.get_for_model(instance.__class__)
        existing_likes = UserLikes.objects.filter(content_type__pk=liked_entity.id, object_id=instance.pk,
                                                  user=request.user)
        if existing_likes:
            likes_instance = existing_likes.first()
            likes_instance.rating = rating
            likes_instance.save()
        else:
            likes_instance = UserLikes(like=like, user=request.user, rating=rating, content_object=instance)
            likes_instance.save()

        if comments:
            comments_instance = UserComments(comments=comments, user=request.user, content_object=instance)
            comments_instance.save()

    if likes_instance or comments_instance:
        flash_message(request, 'Rating has been saved')
    else:
        flash_message(request, 'Unable to save rating or no rating was performed')
    return likes_instance, comments_instance


urlpatterns = [
    path('', HomeView.as_view(), name='tsa-home'),
    path('access/', AccessRedirectView.as_view(), name='access-denied'),
    path('about/', AboutView.as_view(), name='about-us'),
    path('careers/', CareersView.as_view(), name='tsa-careers'),
    path('report/<str:uidb64>', ReportView.as_view(), name='report-advert'),
    path('programmes/', ProgrammeView.as_view(), name='tsa-programmes'),
    path('contact/', ContactFormView.as_view(), name='tsa-contact'),
    path('contact/response/', ContactResponseView.as_view(),
         name='contact-response'),
    path('volunteers/', VolunteersView.as_view(), name='tsa-volunteers'),
    path('feeds/', PublicFeedsView.as_view(), name='public-feeds'),
    path('feeds/user/<str:slug>/', UserFeedsView.as_view(),
         name='user-feeds'),
    path('login/', UserLoginView.as_view(), name='user-login'),
    path('login/pin/', UserPinLoginView.as_view(), name='user-pin-login'),
    path('login/staff/', StaffLoginView.as_view(), name='staff-login'),
    path('logout', logout_view, name='user-logout'),
    path('user/management/password/reset/', UserPasswordResetView.as_view(),
         name='password_reset'),
    path('user/management/reset/<uidb64>/<token>',
         UserPasswordResetConfirmView.as_view(),
         name='password_reset_confirm'),
    path('user/management/password/reset_done/',
         PasswordResetDoneView.as_view(),
         name='password_reset_done'),
    path('user/management/password/reset/successful/',
         PasswordResetCompleteView.as_view(),
         name='password_reset_complete'),
    path('user/profile/<str:slug>/', UserProfileView.as_view(),
         name='user-profile'),
    path('user/management/<str:slug>/', UserProfileManagementView.as_view(),
         name='user-profile-management'),
    path('alumni/', AlumniView.as_view(), name='alumni'),
    path('alumni/<str:year>/', AlumniView.as_view(), name='alumni'),
    path('alumni/<str:year>/<str:month>', AlumniView.as_view(), name='alumni'),
    path('alumni/<str:period>/?reference=before', AlumniView.as_view(), name='alumni'),
    path('alumni/after/<str:period>/?reference=after', AlumniView.as_view(), name='alumni'),
    path('alumni/<str:start>/<str:end>', AlumniView.as_view(), name='alumni'),
    path('careers/', CareersView.as_view(), name='careers'),
    path('careers/<str:year>/', CareersView.as_view(), name='careers'),
    path('careers/<str:year>/<int:month>', CareersView.as_view(), name='careers'),
    path('careers/<str:period>/?reference=before', CareersView.as_view(), name='careers'),
    path('careers/after/<str:period>/?reference=after', CareersView.as_view(), name='careers'),
    path('careers/<str:start>/<str:end>', CareersView.as_view(), name='alumni'),
    path('user/notifications/', NotificationView.as_view(), name='user-notification'),
    path('user/notifications/<str:slug>', NotificationView.as_view(), name='user-notification')
]#   + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
