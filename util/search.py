# defines the search algorithm for a model
from typing import Sequence, Any

from django.db import models


class SearchMixin(models.Model):

    @classmethod
    def search(cls, tokens: Sequence) -> Sequence:
        return search(cls, tokens)

    class Meta:
        abstract = True


def search(model: Any, tokens: Any, search_fields: list = None) -> Sequence:
    conjunctions = (
            'after', 'also', 'although', 'and', 'as', 'because', 'before', 'both', 'but', 'by',
            'case', 'either', 'even', 'for', 'if', 'in', 'lest', 'long', 'much', 'neither',
            'nor', 'not', 'once', 'only', 'or', 'order', 'provided', 'since', 'so', 'soon',
            'than', 'that', 'the', 'though', 'till', 'time', 'unless', 'until', 'what', 'when',
            'whenever', 'where', 'wherever', 'whether', 'while', 'yet',
    )
    words = []
    if isinstance(tokens, (list, tuple)):
        words = [x for x in tokens if isinstance(x, str) and x not in conjunctions]
    elif isinstance(tokens, str):
        words = tokens.split()
        words = [x for x in words if x not in conjunctions]
    if isinstance(model, models.Model):
        # search all model fields which contains records from words list
        fields = model.__class__._meta.get_fields()
        if search_fields:
            fields = [x for x in fields if x in search_fields]
        for field in fields:
            for word in words:
                q = model.__class__.objects.filter(field__icontains=word).distinct()
                if q:
                    yield q