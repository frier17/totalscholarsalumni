from django.urls import path

from util import views

app_name = 'util'

urlpatterns = []

urlpatterns += (
    # urls for Address
    path('address/', views.AddressListView.as_view(), name='address-list'),
    path('address/create/', views.AddressCreateView.as_view(), name='address-create'),
    path('address/detail/<str:slug>/', views.AddressDetailView.as_view(), name='address-detail'),
    path('address/update/<str:slug>/', views.AddressUpdateView.as_view(), name='address-update'),
    path('address/delete/<str:slug>/', views.AddressDeleteView.as_view(), name='address-delete')
)

urlpatterns += (
    # urls for Approval
    path('approval/', views.ApprovalListView.as_view(), name='approval-list'),
    path('approval/create/', views.ApprovalCreateView.as_view(), name='approval-create'),
    path('approval/detail/<str:slug>/', views.ApprovalDetailView.as_view(), name='approval-detail'),
    path('approval/update/<str:slug>/', views.ApprovalUpdateView.as_view(), name='approval-update'),
    path('approval/delete/<str:slug>/', views.ApprovalDeleteView.as_view(), name='approval-delete')
)

urlpatterns += (
    # urls for Institution
    path('institution/', views.InstitutionListView.as_view(), name='institution-list'),
    path('institution/create/', views.InstitutionCreateView.as_view(), name='institution-create'),
    path('institution/detail/<str:slug>/', views.InstitutionDetailView.as_view(), name='institution-detail'),
    path('institution/update/<str:slug>/', views.InstitutionUpdateView.as_view(), name='institution-update'),
    path('institution/delete/<str:slug>/', views.InstitutionDeleteView.as_view(), name='institution-delete')
)

urlpatterns += (
    # urls for Notification
    path('notification/', views.NotificationListView.as_view(), name='notification-list'),
    path('notification/create/', views.NotificationCreateView.as_view(), name='notification-create'),
    path('notification/detail/<str:slug>/', views.NotificationDetailView.as_view(), name='notification-detail'),
    path('notification/update/<str:slug>/', views.NotificationUpdateView.as_view(), name='notification-update'),
    path('notification/delete/<str:slug>/', views.NotificationDeleteView.as_view(), name='notification-delete')
)

urlpatterns += (
    # urls for NotificationResponse
    path('notification-response/', views.NotificationResponseListView.as_view(), name='notification-response-list'),
    path('notification-response/create/', views.NotificationResponseCreateView.as_view(),
         name='notification-response-create'),
    path('notification-response/detail/<str:slug>/', views.NotificationResponseDetailView.as_view(),
         name='notification-response-detail'),
    path('notification-response/update/<str:slug>/', views.NotificationResponseUpdateView.as_view(),
         name='notification-response-update'),
    path('notification-response/delete/<str:slug>/', views.NotificationResponseDeleteView.as_view(),
         name='notification-response-delete')
)

urlpatterns += (
    # urls for Signature
    path('signature/', views.SignatureListView.as_view(), name='signature-list'),
    path('signature/create/', views.SignatureCreateView.as_view(), name='signature-create'),
    path('signature/detail/<str:slug>/', views.SignatureDetailView.as_view(), name='signature-detail'),
    path('signature/update/<str:slug>/', views.SignatureUpdateView.as_view(), name='signature-update'),
    path('signature/delete/<str:slug>/', views.SignatureDeleteView.as_view(), name='signature-delete')
)

urlpatterns += (
    # urls for UserAvatar
    path('user-avatar/', views.UserAvatarListView.as_view(), name='user-avatar-list'),
    path('user-avatar/create/', views.UserAvatarCreateView.as_view(), name='user-avatar-create'),
    path('user-avatar/detail/<str:slug>/', views.UserAvatarDetailView.as_view(), name='user-avatar-detail'),
    path('user-avatar/update/<str:slug>/', views.UserAvatarUpdateView.as_view(), name='user-avatar-update'),
    path('user-avatar/delete/<str:slug>/', views.UserAvatarDeleteView.as_view(), name='user-avatar-delete')
)

urlpatterns += (
    # urls for UserPreferences
    path('user-preferences/', views.UserPreferencesListView.as_view(), name='user-preferences-list'),
    path('user-preferences/create/', views.UserPreferencesCreateView.as_view(), name='user-preferences-create'),
    path('user-preferences/detail/<str:slug>/', views.UserPreferencesDetailView.as_view(),
         name='user-preferences-detail'),
    path('user-preferences/update/<str:slug>/', views.UserPreferencesUpdateView.as_view(),
         name='user-preferences-update'),
    path('user-preferences/delete/<str:slug>/', views.UserPreferencesDeleteView.as_view(),
         name='user-preferences-delete')
)

urlpatterns += (
    # urls for UserSignedDocument
    path('user-signed-document/', views.UserSignedDocumentListView.as_view(), name='user-signed-document-list'),
    path('user-signed-document/create/', views.UserSignedDocumentCreateView.as_view(),
         name='user-signed-document-create'),
    path('user-signed-document/detail/<str:slug>/', views.UserSignedDocumentDetailView.as_view(),
         name='user-signed-document-detail'),
    path('user-signed-document/update/<str:slug>/', views.UserSignedDocumentUpdateView.as_view(),
         name='user-signed-document-update'),
    path('user-signed-document/delete/<str:slug>/', views.UserSignedDocumentDeleteView.as_view(),
         name='user-signed-document-delete')
)

urlpatterns += (
    # urls for UserUploadedDocument
    path('user-uploaded-document/', views.UserUploadedDocumentListView.as_view(), name='user-uploaded-document-list'),
    path('user-uploaded-document/create/', views.UserUploadedDocumentCreateView.as_view(),
         name='user-uploaded-document-create'),
    path('user-uploaded-document/detail/<str:slug>/', views.UserUploadedDocumentDetailView.as_view(),
         name='user-uploaded-document-detail'),
    path('user-uploaded-document/update/<str:slug>/', views.UserUploadedDocumentUpdateView.as_view(),
         name='user-uploaded-document-update'),
    path('user-uploaded-document/delete/<str:slug>/', views.UserUploadedDocumentDeleteView.as_view(),
         name='user-uploaded-document-delete')
)
