# define the custom managers for the application
from django.db import models


class SoftDeleteManager(models.Manager):

    def get_queryset(self):
        # return items with no deleted_on date set
        # @todo: ensure null dates or empty dates
        return super(SoftDeleteManager, self).get_queryset().filter(deleted_at__isnull=True)