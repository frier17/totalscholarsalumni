import datetime as dt
import logging
from typing import Sequence

from django.contrib import messages
from django.http import HttpRequest

import util.models as um

logger = logging.getLogger(__name__)


def _prepare_message(message: str, request: HttpRequest = None,
                     user: um.BaseUser = None, **kwargs) -> str:
    current_time = dt.datetime.now()
    _user = None
    if '{asctime}' in message:
        message.format(asctime=current_time)

    if isinstance(request, HttpRequest) and hasattr(request, 'user'):
        if request.user.is_active:
            _user = request.user
            if '{referer}' in message:
                message.format(referer=request.META.get('referer'))
            if '{url' in message:
                message.format(url=request.path)
            if '{remote_addr}' in message:
                message.format(remote_addr=request.META.get('REMOTE_ADDR'))
    if isinstance(user, um.BaseUser) and user.is_active:
        _user = user
        if _user.is_authenticated:
            if '{username}' in message:
                message.format(username=_user.username)
            if '{user_id}' in message:
                message.format(user_id=_user.urlsafe_uuid)
    try:
        for key, value in kwargs.items():
            if f'{key}' in message:
                message.format(key=value)
        return message
    except KeyError:
        logger.exception('Undefined key or field provided')
    except LookupError:
        logger.exception('Undefined key or field provided')


def log_user_info(request: HttpRequest = None, user: um.BaseUser = None,
                  message: str = None, **kwargs) -> None:
    message = _prepare_message(request=request, user=user, message=message, **kwargs)
    logger.info(message, extra=kwargs)


def log_user_warning(request: HttpRequest = None, user: um.BaseUser = None,
                     message: str = None, **kwargs) -> None:
    message = _prepare_message(message, request, user, **kwargs)
    logger.warning(message, extra=kwargs)


def log_exception(request: HttpRequest = None, user: um.BaseUser = None,
                  message: str = None, **kwargs) -> None:
    message = _prepare_message(message, request, user, **kwargs)
    logger.exception(message, extra=kwargs)


def log_user_error(request: HttpRequest = None, user: um.BaseUser = None,
                   message: str = None, **kwargs) -> None:
    message = _prepare_message(message, request, user, **kwargs)
    logger.error(message, extra=kwargs)


def log_critical(request: HttpRequest = None, user: um.BaseUser = None,
                 message: str = None, **kwargs) -> None:
    message = _prepare_message(message, request, user, **kwargs)
    logger.critical(message, extra=kwargs)


def flash_message(request: HttpRequest, message: str = None, kwargs: dict = None,
                  tags: list = None) -> None:
    msg = None
    if request:
        msg = _prepare_message(message, request, kwargs)
    if tags:
        messages.add_message(request, messages.INFO, message=msg, extra_tags=tags)
    else:
        messages.add_message(request, messages.INFO, message=msg)


def set_notification(user: um.BaseUser, message: str = None,
                     header: str = None, notification_code: str = None,
                     notification_reason: str = None) -> bool:
    message = _prepare_message(user=user, message=message)
    notice = um.Notification.objects.create(
            header=header,
            notification_code=notification_code,
            message=message,
            notification_reason=notification_reason)
    if notice.pk:
        notice.recipients.add(user)
    return True if notice.pk else False


def set_user_preference(request: HttpRequest = None, user: um.BaseUser = None,
                        preferences: Sequence = None):
    _user = None
    if isinstance(request, HttpRequest) and hasattr(request, 'user'):
        if request.user.is_active:
            _user = request.user
    if isinstance(user, um.BaseUser):
        if user.is_active:
            _user = user
    if isinstance(preferences, (tuple, list)):
        try:
            user_preferences = [um.UserPreferences(meta=k, value=v, user=_user)
                                for k, v in preferences]
            um.UserPreferences.objects.bulk_create(user_preferences)
        except KeyError:
            logger.exception('Invalid key-value pair passed to uer preferences')
    elif isinstance(preferences, dict):
        try:
            user_preferences = [um.UserPreferences(meta=k, value=v, user=_user)
                                for k, v in preferences.items()]
            um.UserPreferences.objects.bulk_create(user_preferences)
        except KeyError:
            logger.exception('Invalid key-value pair passed to uer preferences')
