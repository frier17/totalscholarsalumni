import os
from typing import Any

import stringcase
from django import template

from util.models import PublishMixin

register = template.Library()


@register.inclusion_tag('components/posts/default.html', name='publish')
def publish(articles, broadcasts=None, broadcast_order=None, excluded=None, **kwargs):
    _template = None
    _publish = None
    # prepare the published item based on the publish mixin attributes
    out = _post_articles(articles, broadcasts=broadcasts, broadcast_order=broadcast_order, excluded=excluded)
    out.update(**kwargs)
    return out


def _post_articles(articles, broadcasts: list = None, broadcast_order: list = None, excluded: list = None) -> Any:
    if isinstance(articles, (list, tuple)):
        articles = [x for x in articles if isinstance(x, PublishMixin)]
    return {'articles': [_post_instance(article, broadcasts, broadcast_order, excluded) for article in articles]}


def _post_instance(instance, broadcasts: list = None, broadcast_order: list = None, excluded: list = None) -> dict:
    _template = None
    _publish = None

    if isinstance(instance, PublishMixin):
        if instance.broadcasts and instance.broadcast_order and instance.excluded_broadcasts:
            broadcast_order = set(instance.broadcast_order) - set(instance.excluded_broadcasts)
            broadcasts = set(instance.broadcasts) - set(instance.excluded_broadcasts)
            broadcasts = broadcast_order.union(broadcasts)
        elif instance.broadcasts and instance.excluded_broadcasts and instance.broadcast_order is None:
            broadcasts = set(instance.broadcasts) - set(instance.excluded_broadcasts)
        elif instance.broadcasts and (instance.excluded_broadcasts is None and instance.broadcast_order is None):
            broadcasts = instance.broadcasts
        else:
            broadcasts = [x.name for x in instance._meta.get_fields()]
        _publish = {x: getattr(instance, x) for x in broadcasts}

    elif broadcasts or (broadcast_order or excluded):
        if broadcast_order and excluded:
            broadcast_order = set(broadcast_order) - set(excluded)
        broadcasts = set(broadcast_order).union(set(broadcasts))
        _publish = {x: getattr(instance, x) for x in broadcasts}

    if getattr(instance, 'broadcast_template'):
        _template = instance.broadcast_template
    else:
        app, label = instance._meta.label.split('.')
        _template = f'components/posts/{stringcase.snakecase(app)}/{stringcase.snakecase(label)}'.replace('.', '/')
        _template += '.html'
        _template = _template.lower()
    # return the context for the template to render with wall to be included
    return {'article': _publish, 'wall': _template}
