from django import template

register = template.Library()


@register.inclusion_tag('volunteer_applied.html', takes_context=True)
def check_volunteer(context):
    request = context['request']
    user = request.user
    instance = context['object']
    model = instance.__class__
    return {'applied': model.objects.filter(volunteer_course_application__volunteer=user, uuid=instance.uuid).exists()}
