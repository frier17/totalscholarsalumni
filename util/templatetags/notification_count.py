from django import template

register = template.Library()


@register.simple_tag
def notification_count(user):
    import util.context_data
    return util.context_data.get_notification_count(user)
