from django import template
import random

register = template.Library()


@register.simple_tag
def generate_random():
    return random.choice(random.sample(range(10000000), k=10))

