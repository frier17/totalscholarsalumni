from django import template

register = template.Library()


@register.simple_tag
def class_name(value):
    if hasattr(value, '__class__'):
        return value.__class__.__name__
