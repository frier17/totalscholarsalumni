from django import template

register = template.Library()


@register.inclusion_tag('job_applied.html', takes_context=True)
def check_applicant(context):
    request = context['request']
    user = request.user
    instance = context['object']
    model = instance.__class__
    return {'applied': model.objects.filter(job_applications__applicant=user, uuid=instance.uuid).exists()}
