import logging
import re as regex
from datetime import datetime

from cryptography.hazmat.primitives.twofactor import InvalidToken
from django import forms
from django.contrib.auth import login
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, path
from django.utils.http import urlencode
from django.utils.translation import gettext_lazy as _
from django.views.generic import FormView, View

import messages as message_templates
import tsa.settings as settings
from util import MOBILE_DIAL_CODES, GENDER, BusinessServices
from util.email import generate_otp_email
from util.logging import set_notification, flash_message
from util.models import BaseUser, UserAvatar
from util.otp_service import OtpServer
from util.signals import otp_authenticated_signal, otp_auth_failed_signal

from broadcast.sms import send_sms

logger = logging.getLogger(__name__)


class UserViewMixin(View):

    def __init__(self):
        self._account = None
        super(UserViewMixin, self).__init__()

    @property
    def user(self):
        try:
            self._account = self.request.GET.get('account') or \
                            self.request.POST.get('account') or self.kwargs.get('account')
            return BaseUser.objects.get(email=self._account)
        except ObjectDoesNotExist:
            logger.exception('User with given credentials does not exist')
        except MultipleObjectsReturned:
            logger.exception('Multiple user records exists for given credentials exist')


class OnBoardingForm(forms.Form):
    email = forms.EmailField(label=_('Email'),
                             error_messages={'required': _('Provide a valid email')})
    dial_code = forms.ChoiceField(
        label=_('Country Dial Code'),
        help_text=_('Select country dial code'),
        required=False,
        choices=MOBILE_DIAL_CODES,
        error_messages={
            'required': _('Select a valid dial code'),
            'invalid_choice': _('Selected value is invalid')
        })
    mobile = forms.CharField(
        max_length=20,
        min_length=7,
        empty_value='',
        required=False,
        label=_('Mobile'),
        widget=forms.TextInput,
        help_text=_('Enter mobile number without dial code'),
        error_messages={
            'required': _('Provide valid mobile'),
            'max_length': _('Mobile number exceeds maximum number of digits'),
            'min_length': _('Mobile number below minimum number of digits')
        })

    def send_email(self, message: str = None):
        return generate_otp_email(
            recipient=self.cleaned_data.get('email'),
            from_email=settings.DEFAULT_FROM_EMAIL,
            subject=settings.OTP_EMAIL_SUBJECT,
            message=message,
            auth_password=settings.EMAIL_HOST_PASSWORD,
            auth_user=settings.EMAIL_HOST_USER)

    def send_sms(self, message: str = None):
        mobile = str(self.cleaned_data.get('mobile'))
        dial_code = str(self.cleaned_data.get('dial_code'))
        if mobile.startswith('0'):
            mobile = dial_code + mobile[1:]
        else:
            mobile = dial_code + mobile
        return send_sms(
            to=mobile,
            message=message
        )


class AuthenticateOTPForm(forms.Form):
    otp_code = forms.CharField(max_length=10, min_length=4, label=_('Code'),
                               error_messages={
                                   'required': _('Authentication code is required'),
                                   'min_length': _(
                                       'Mobile number below minimum number of digits'),
                                   'max_length': _(
                                       'Mobile number exceeds maximum number of '
                                       'digits')
                               })


class RegistrationUpdateForm(forms.Form):
    title = forms.CharField(max_length=10, min_length=1, required=False,
                            error_messages={
                                'min_length': _(
                                    'Number of characters in middle name is '
                                    'below '
                                    'minimum '
                                    'value'),
                                'max_length': _(
                                    'Number of characters in middle exceeds '
                                    'maximum '
                                    'value')
                            })
    first_name = forms.CharField(max_length=50, min_length=2,
                                 error_messages={
                                     'required': _('Provide a username'),
                                     'min_length': _(
                                         'Number of characters in firstname/given '
                                         'name is below minimum value'),
                                     'max_length': _(
                                         'Number of characters in given name exceeds '
                                         'maximum '
                                         'value')
                                 })
    last_name = forms.CharField(max_length=50, min_length=2,
                                error_messages={
                                    'required': _('Provide your last name/surname'),
                                    'min_length': _(
                                        'Number of characters in last name is below '
                                        'minimum '
                                        'value'),
                                    'max_length': _(
                                        'Number of characters in last name exceeds '
                                        'maximum '
                                        'value')
                                })
    middle_name = forms.CharField(max_length=50, min_length=1, required=False,
                                  error_messages={
                                      'min_length': _(
                                          'Number of characters in middle name is below '
                                          'minimum '
                                          'value'),
                                      'max_length': _(
                                          'Number of characters in middle exceeds '
                                          'maximum '
                                          'value')
                                  })
    username = forms.CharField(max_length=20, min_length=2,
                               error_messages={
                                   'required': _('Provide a username'),
                                   'min_length': _(
                                       'Number of characters in username is below '
                                       'minimum '
                                       'value'),
                                   'max_length': _(
                                       'Number of characters in user exceeds maximum '
                                       'value')
                               })
    password = forms.CharField(
        widget=forms.PasswordInput,
        max_length=128,
        error_messages={
            'required': _('Password is required'),
            'min_length': _('Password is below minimum number of digits'),
            'max_length': _('Password exceeds maximum number of digits')
        },
        help_text=_('Please, update the default password. Password should be easy to '
                    'remember, and may contain special character(s) (&^%$@+...), '
                    'alphabet(s) (a, A, b, B, c, C...)'
                    'and '
                    'number(s) (0, 1, 2...)'))
    confirm_password = forms.CharField(
        widget=forms.PasswordInput,
        max_length=128,
        error_messages={
            'required': _('Password confirmation is required'),
            'min_length': _('Password confirmation is below minimum number of digits'),
            'max_length': _('Password confirmation exceeds maximum number of digits')
        },
        help_text=_('Please, confirm password'
                    'Confirmation should match original password'))
    gender = forms.ChoiceField(
        widget=forms.RadioSelect,
        choices=GENDER,
        help_text=_('Select your gender'),
        required=True,
        error_messages={
            'required': _('This field is required'),
        })
    avatar = forms.ImageField(
        label=_('Upload Passport or Avatar'),
        help_text=_(
            'Provide a recent passport photograph or image to '
            'identity you. Image file size should not exceed '
            '2.5MB. Large image files may be automatically resized'),
        required=True,
        error_messages={
            'required': _('This field is required')
        }
    )
    autobiography = forms.CharField(
        widget=forms.Textarea,
        label=_('Provide a brief description of yourself you wish to share'),
        help_text=_('Brief autobiography should not be more than 200 words'),
    )
    date_of_birth = forms.DateField(
        widget=forms.TextInput(
            attrs={'type': 'date'}
        ),
        label=_('Date of Birth'),
        help_text=_('Provide date of birth to further strengthen your profile. This '
                    'field is optional but may be used for certain analysis and '
                    'messaging services.'
                    'Date should be in format: dd/mm/yyyy'
                    ),
        required=False
    )

    pin = forms.CharField(
        widget=forms.PasswordInput,
        label=_('Pin'),
        help_text=_('Provide a login PIN. The login registered mobile number and pin will '
                    'enable alternative access to TSA website'
                    'Enter a 4 to 6 digit number as pin'
                    ),
        required=False,
        max_length=6,
        min_length=4,
        error_messages={
            'min_length': _('Pin is below minimum number of digits'),
            'max_length': _('Pin exceeds maximum number of digits')
        },
    )
    confirm_pin = forms.CharField(
        widget=forms.PasswordInput,
        label=_('Confirm Pin'),
        help_text=_('Please confirm pin'
                    'Confirmation should match original pin'),
        required=False,
        max_length=6,
        min_length=4,
        error_messages={
            'min_length': _('Pin is below minimum number of digits'),
            'max_length': _('Pin exceeds maximum number of digits')
        },
    )
    make_public = forms.BooleanField(
        required=False,
        label=_('Make profile public'),
        help_text=_('Make your profile public for other members to view and connect '
                    'with you. Public profiles are ranked higher than private profiles'
                    'See <a href="#">Terms & Agreement</a> for more')
    )

    def is_valid(self):
        self.full_clean()
        checker = str(self.cleaned_data.get('pin')).isnumeric()
        valid_password = self.cleaned_data.get('password') == self.cleaned_data.get(
            'confirm_password')
        valid_pin = \
            self.cleaned_data.get('pin') == self.cleaned_data.get('confirm_pin') and checker
        if self.cleaned_data.get('password') and (self.cleaned_data.get('pin') is not None):
            return super(RegistrationUpdateForm, self).is_valid() and valid_password and \
                   valid_pin
        else:
            return super(RegistrationUpdateForm, self).is_valid() and valid_password


class StaffRegistrationUpdateForm(forms.Form):
    work_id = forms.CharField(max_length=20, min_length=7, required=True,
                              error_messages={
                                  'required': _('IGG or Work ID is required'),
                                  'min_length': _(
                                      'Number of characters in middle name is '
                                      'below '
                                      'minimum '
                                      'value'),
                                  'max_length': _(
                                      'Number of characters in middle exceeds '
                                      'maximum '
                                      'value')
                              })
    title = forms.CharField(max_length=10, min_length=1, required=False,
                            error_messages={
                                'min_length': _(
                                    'Number of characters in middle name is '
                                    'below '
                                    'minimum '
                                    'value'),
                                'max_length': _(
                                    'Number of characters in middle exceeds '
                                    'maximum '
                                    'value')
                            })
    first_name = forms.CharField(max_length=50, min_length=2,
                                 error_messages={
                                     'required': _('Provide a username'),
                                     'min_length': _(
                                         'Number of characters in firstname/given '
                                         'name is below minimum value'),
                                     'max_length': _(
                                         'Number of characters in given name exceeds '
                                         'maximum '
                                         'value')
                                 })
    last_name = forms.CharField(max_length=50, min_length=2,
                                error_messages={
                                    'required': _('Provide your last name/surname'),
                                    'min_length': _(
                                        'Number of characters in last name is below '
                                        'minimum '
                                        'value'),
                                    'max_length': _(
                                        'Number of characters in last name exceeds '
                                        'maximum '
                                        'value')
                                })
    middle_name = forms.CharField(max_length=50, min_length=1, required=False,
                                  error_messages={
                                      'min_length': _(
                                          'Number of characters in middle name is below '
                                          'minimum '
                                          'value'),
                                      'max_length': _(
                                          'Number of characters in middle exceeds '
                                          'maximum '
                                          'value')
                                  })
    username = forms.CharField(max_length=20, min_length=2,
                               error_messages={
                                   'required': _('Provide a username'),
                                   'min_length': _(
                                       'Number of characters in username is below '
                                       'minimum '
                                       'value'),
                                   'max_length': _(
                                       'Number of characters in user exceeds maximum '
                                       'value')
                               })
    password = forms.CharField(
        widget=forms.PasswordInput,
        max_length=128,
        error_messages={
            'required': _('Password is required'),
            'min_length': _('Password is below minimum number of digits'),
            'max_length': _('Password exceeds maximum number of digits')
        },
        help_text=_('Please, update the default password. Password should be easy to '
                    'remember, and may contain special character(s) (&^%$@+...), '
                    'alphabet(s) (a, A, b, B, c, C...)'
                    'and '
                    'number(s) (0, 1, 2...)'))
    confirm_password = forms.CharField(
        widget=forms.PasswordInput,
        max_length=128,
        error_messages={
            'required': _('Password confirmation is required'),
            'min_length': _('Password confirmation is below minimum number of digits'),
            'max_length': _('Password confirmation exceeds maximum number of digits')
        },
        help_text=_('Please, confirm password'
                    'Confirmation should match original password'))
    gender = forms.ChoiceField(
        widget=forms.RadioSelect,
        choices=GENDER,
        help_text=_('Select your gender'),
        required=True,
        error_messages={
            'required': _('This field is required'),
        })
    avatar = forms.ImageField(
        label=_('Upload Passport or Avatar'),
        help_text=_(
            'Provide a recent passport photograph or image to '
            'identity you. Image file size should not exceed '
            '2.5MB. Large image files may be automatically resized'),
        required=True,
        error_messages={
            'required': _('This field is required')
        }
    )

    pin = forms.CharField(
        widget=forms.PasswordInput,
        label=_('Pin'),
        help_text=_('Provide a login PIN. The login registered mobile number and pin will '
                    'enable alternative access to TSA website'
                    'Enter a 4 to 6 digit number as pin'
                    ),
        required=False,
        max_length=6,
        min_length=4,
        error_messages={
            'min_length': _('Pin is below minimum number of digits'),
            'max_length': _('Pin exceeds maximum number of digits')
        },
    )
    confirm_pin = forms.CharField(
        widget=forms.PasswordInput,
        label=_('Confirm Pin'),
        help_text=_('Please confirm pin'
                    'Confirmation should match original pin'),
        required=False,
        max_length=6,
        min_length=4,
        error_messages={
            'min_length': _('Pin is below minimum number of digits'),
            'max_length': _('Pin exceeds maximum number of digits')
        },
    )

    def is_valid(self):
        self.full_clean()
        checker = str(self.cleaned_data.get('pin')).isnumeric()
        valid_password = self.cleaned_data.get('password') == self.cleaned_data.get(
            'confirm_password')
        valid_pin = \
            self.cleaned_data.get('pin') == self.cleaned_data.get('confirm_pin') and checker
        if self.cleaned_data.get('password') and (self.cleaned_data.get('pin') is not None):
            return super(StaffRegistrationUpdateForm, self).is_valid() and valid_password and \
                   valid_pin
        else:
            return super(StaffRegistrationUpdateForm, self).is_valid() and valid_password


class OnBoardingView(FormView):
    template_name = 'on_boarding.html'
    form_class = OnBoardingForm

    def get_success_url(self):
        account = self.request.POST.get('email')
        return '%s?%s' % (reverse_lazy('authenticate-code'),
                          urlencode({'account': account}))

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            flash_message(request, message=message_templates.auth_user_exist)
            return HttpResponseRedirect(reverse_lazy('tsa-home'))
        else:
            return super(OnBoardingView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            # @todo: add captcha to the form
            existing_user = BaseUser.objects.filter(email=form.cleaned_data.get('email')).exists()
            if not existing_user:
                new_user = BaseUser(
                    username='<DEFAULT_USERNAME>',
                    email=form.cleaned_data.get('email'),
                    mobile='(%s) %s' % (form.cleaned_data.get('dial_code'),
                                        form.cleaned_data.get('mobile'))
                )
                new_user.set_password(settings.DEFAULT_USER_PASSWORD)
                new_user.is_active = False
                new_user.save()
                OtpServer(user=new_user).generate_otp()
                return self.form_valid(form)
            else:
                flash_message(request, message='User with given email already exist')
                return self.form_invalid(form)
        else:
            return self.form_invalid(form)


class AuthenticateOTPView(UserViewMixin, FormView):
    template_name = 'authenticate_code.html'
    form_class = AuthenticateOTPForm

    def __init__(self):
        self._account = None
        super(AuthenticateOTPView, self).__init__()

    def get_success_url(self):
        if self.user:
            return reverse_lazy('update-registration',
                                kwargs={'account': self._account})
        elif self.request.user:
            return self.request.META.get('HTTP_REFERER')
        else:
            return reverse_lazy('start-on-boarding')

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            code = form.cleaned_data.get('otp_code')
            if regex.fullmatch(r'\d+', code):
                try:
                    code_hash = OtpServer(user=self.user).verify_otp(code)

                    if code_hash:
                        otp_authenticated_signal.send_robust(
                            otp=code,
                            request=self.request,
                            asctime=datetime.now(),
                            code_hash=code_hash,
                            sender=self.__class__,
                            user=self.user)
                        return self.form_valid(form)
                    else:
                        otp_auth_failed_signal.send_robust(
                            otp=code,
                            request=self.request,
                            asctime=datetime.now(),
                            code_hash=code_hash,
                            sender=self.__class__,
                            user=self.user
                        )
                        return self.form_invalid(form)
                except InvalidToken:
                    logger.exception(message_templates.otp_auth_failed.format(
                        asctime=None,
                        otp=code,
                        username=self.user.username,
                        user_id=self.user.urlsafe_uuid,
                        referer=None,
                        url=None,
                        remote_addr=None
                    ))
                    return self.form_invalid(form)
            return self.form_invalid(form)
        else:
            return self.form_invalid(form)


class RegistrationUpdateView(UserViewMixin, FormView):
    template_name = 'registration_update.html'
    form_class = RegistrationUpdateForm

    def get_success_url(self):
        return reverse_lazy('user-profile', kwargs={'slug': self.user.urlsafe_uuid})

    def post(self, request, *args, **kwargs):
        form = RegistrationUpdateForm(data=self.request.POST, files=self.request.FILES)
        if form.is_valid():
            if form.cleaned_data.get('password') == form.cleaned_data.get('confirm_password'):
                _u = self.user
                _u.username = form.cleaned_data.get('username')
                _u.first_name = form.cleaned_data.get('first_name')
                _u.last_name = form.cleaned_data.get('last_name')
                _u.gender = form.cleaned_data.get('gender')
                _u.autobiography = form.cleaned_data.get('autobiography')
                _u.date_of_birth = form.cleaned_data.get('date_of_birth')
                _u.is_public = form.cleaned_data.get('make_public')
                _u.is_shareable = form.cleaned_data.get('make_public')
                _u.middle_name = form.cleaned_data.get('middle_name')
                _u.title = form.cleaned_data.get('title')
                if form.cleaned_data.get('pin'):
                    from django.contrib.auth.hashers import make_password
                    _u.pin = make_password(form.cleaned_data.get('pin'))
                _u.set_password(form.cleaned_data.get('password'))
                _u.is_active = True
                _u.save()
                avatar = UserAvatar.objects.create(
                    user=_u,
                    media=form.cleaned_data.get('avatar')
                )
                avatar.save()
                login(self.request, user=_u)
                if self.request.user:
                    set_notification(
                        user=_u,
                        message='Successfully registered',
                        header=BusinessServices.USER_LOGIN.name,
                        notification_code=BusinessServices.USER_LOGIN.value[0],
                        notification_reason='Log'
                    )
                    flash_message(self.request,
                                  message='Successfully registered. You have been automatically logged in '
                                          'with selected credentials')

                return self.form_valid(form)
        else:
            return self.form_invalid(form)


class StaffRegistrationUpdateView(UserViewMixin, FormView):
    template_name = 'staff_registration_update.html'
    form_class = StaffRegistrationUpdateForm

    def get_success_url(self):
        return reverse_lazy('user-profile', kwargs={'slug': self.user.urlsafe_uuid})

    def post(self, request, *args, **kwargs):
        form = StaffRegistrationUpdateForm(data=self.request.POST, files=self.request.FILES)
        if form.is_valid():
            if form.cleaned_data.get('password') == form.cleaned_data.get('confirm_password'):
                _u = self.user
                _u.username = form.cleaned_data.get('username')
                _u.first_name = form.cleaned_data.get('first_name')
                _u.last_name = form.cleaned_data.get('last_name')
                _u.gender = form.cleaned_data.get('gender')
                _u.middle_name = form.cleaned_data.get('middle_name')
                _u.title = form.cleaned_data.get('title')
                if form.cleaned_data.get('pin'):
                    from django.contrib.auth.hashers import make_password
                    _u.pin = make_password(form.cleaned_data.get('pin'))
                _u.set_password(form.cleaned_data.get('password'))
                _u.is_active = True
                _u.is_staff = True
                _u.save()
                avatar = UserAvatar.objects.create(
                    user=_u,
                    media=form.cleaned_data.get('avatar')
                )
                avatar.save()
                if not login(self.request, user=_u):
                    set_notification(
                        user=_u,
                        message='Unable to log in user with selected credentials',
                        header=BusinessServices.USER_LOGIN.name,
                        notification_code=BusinessServices.USER_LOGIN.value[0],
                        notification_reason='Log'
                    )
                    flash_message(self.request, message='Unable to log in user with selected '
                                                        'credentials')

                return self.form_valid(form)
        else:
            return self.form_invalid(form)


urlpatterns = [
    path('registration/on-boarding/', OnBoardingView.as_view(),
         name='start-on-boarding'),
    path('registration/authenticate-code/',
         AuthenticateOTPView.as_view(),
         name='authenticate-code'),
    path('registration/user/update/<str:account>', RegistrationUpdateView.as_view(),
         name='update-registration'),
    path('registration/staff/update/<str:account>', StaffRegistrationUpdateView.as_view(),
         name='staff-update-registration'),
]
