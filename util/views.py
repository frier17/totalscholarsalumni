from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import DeleteView, DetailView, ListView, UpdateView, CreateView, RedirectView, TemplateView

from util import forms
from util import models


class DeleteAclMixin(DeleteView):
    user_field = None
    access_redirect = reverse_lazy('access-denied')

    def get_object(self, queryset=None):
        if self.request.user.is_authenticated:
            slug = self.kwargs.get(self.slug_url_kwarg)
            pk = self.kwargs.get(self.pk_url_kwarg)
            if pk is not None:
                queryset = queryset.filter(**{'pk': pk, f'{self.user_field}': self.request.user})
            if slug is not None and (pk is None or self.query_pk_and_slug):
                queryset = self.model.objects.filter(**{f'{self.user_field}': self.request.user, f'{self.slug_field}':
                    slug})
            try:
                return queryset.get()
            except ObjectDoesNotExist:
                return None

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object:
            return HttpResponseRedirect(self.access_redirect)
        else:
            return super(DeleteAclMixin, self).get(request, *args, **kwargs)


class UpdateAclMixin(UpdateView):
    user_field = None
    access_redirect = reverse_lazy('access-denied')

    def get_object(self, queryset=None):
        if self.request.user.is_authenticated:
            slug = self.kwargs.get(self.slug_url_kwarg)
            pk = self.kwargs.get(self.pk_url_kwarg)
            if pk is not None:
                queryset = queryset.filter(**{'pk': pk, f'{self.user_field}': self.request.user})
            if slug is not None and (pk is None or self.query_pk_and_slug):
                queryset = self.model.objects.filter(**{f'{self.user_field}': self.request.user, f'{self.slug_field}':
                    slug})
            try:
                return queryset.get()
            except ObjectDoesNotExist:
                return None

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object:
            return HttpResponseRedirect(self.access_redirect)
        else:
            return super(UpdateAclMixin, self).get(request, *args, **kwargs)


class AccessRedirectView(TemplateView):
    template_name = 'access_denied_redirect.html'


class ReadOnlyMixin(RedirectView):

    def __init__(self, *args, **kwargs):
        if not self.url:
            self.url = reverse_lazy(self.request.META.get('HTTP_REFERER'))
        super(ReadOnlyMixin, self).__init__(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        return HttpResponseRedirect(self.url)

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(self.url)


class AddressListView(ListView):
    model = models.Address
    # paginate_by = 10
    template_name = 'util/address_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(AddressListView, self).get_queryset()
            # filter queryset for logged in user
            return queryset
        else:
            return super(AddressListView, self).get_queryset().none()


class AddressCreateView(CreateView):
    model = models.Address
    form_class = forms.AddressForm
    template_name = 'util/address_form.html'
    initial = {}

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(AddressCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:address-list')


class AddressDetailView(DetailView):
    model = models.Address
    template_name = 'util/address_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(AddressDetailView, self).get_context_data(**kwargs)


class AddressUpdateView(ReadOnlyMixin):
    model = models.Address
    form_class = forms.AddressForm
    template_name = 'util/address_form.html'
    initial = {}


class AddressDeleteView(ReadOnlyMixin):
    model = models.Address
    success_url = reverse_lazy('util:address-list')
    template_name = 'util/address_confirm_delete.html'


class ApprovalListView(ListView):
    model = models.Approval
    # paginate_by = 10
    template_name = 'util/approval_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(ApprovalListView, self).get_queryset()
            # filter queryset for logged in user
            return queryset
        else:
            return super(ApprovalListView, self).get_queryset().none()


class ApprovalCreateView(CreateView):
    model = models.Approval
    form_class = forms.ApprovalForm
    template_name = 'util/approval_form.html'
    initial = {}

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(ApprovalCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:approval-list')


class ApprovalDetailView(DetailView):
    model = models.Approval
    template_name = 'util/approval_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(ApprovalDetailView, self).get_context_data(**kwargs)


class ApprovalUpdateView(ReadOnlyMixin):
    model = models.Approval
    form_class = forms.ApprovalForm
    template_name = 'util/approval_form.html'
    initial = {}


class ApprovalDeleteView(ReadOnlyMixin):
    model = models.Approval
    success_url = reverse_lazy('util:approval-list')
    template_name = 'util/approval_confirm_delete.html'


class InstitutionListView(ListView):
    model = models.Institution
    # paginate_by = 10
    template_name = 'util/institution_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(InstitutionListView, self).get_queryset()
            # filter queryset for logged in user
            return queryset
        else:
            return super(InstitutionListView, self).get_queryset().none()


class InstitutionCreateView(CreateView):
    model = models.Institution
    form_class = forms.InstitutionForm
    template_name = 'util/institution_form.html'
    initial = {}

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(InstitutionCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:institution-list')


class InstitutionDetailView(DetailView):
    model = models.Institution
    template_name = 'util/institution_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(InstitutionDetailView, self).get_context_data(**kwargs)


class InstitutionUpdateView(ReadOnlyMixin):
    model = models.Institution
    form_class = forms.InstitutionForm
    template_name = 'util/institution_form.html'
    initial = {}


class InstitutionDeleteView(ReadOnlyMixin):
    model = models.Institution
    success_url = reverse_lazy('util:institution-list')
    template_name = 'util/institution_confirm_delete.html'


class NotificationListView(ListView):
    model = models.Notification
    # paginate_by = 10
    template_name = 'util/notification_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(NotificationListView, self).get_queryset()
            # filter queryset for logged in user
            return queryset
        else:
            return super(NotificationListView, self).get_queryset().none()


class NotificationCreateView(CreateView):
    model = models.Notification
    form_class = forms.NotificationForm
    template_name = 'util/notification_form.html'
    initial = {}

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(NotificationCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:notification-list')


class NotificationDetailView(DetailView):
    model = models.Notification
    template_name = 'util/notification_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(NotificationDetailView, self).get_context_data(**kwargs)


class NotificationUpdateView(ReadOnlyMixin):
    model = models.Notification
    form_class = forms.NotificationForm
    template_name = 'util/notification_form.html'
    initial = {}


class NotificationDeleteView(ReadOnlyMixin):
    model = models.Notification
    success_url = reverse_lazy('util:notification-list')
    template_name = 'util/notification_confirm_delete.html'


class NotificationResponseListView(ListView):
    model = models.NotificationResponse
    # paginate_by = 10
    template_name = 'util/notification_response_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(NotificationResponseListView, self).get_queryset()
            # filter queryset for logged in user
            return queryset
        else:
            return super(NotificationResponseListView, self).get_queryset().none()


class NotificationResponseCreateView(CreateView):
    model = models.NotificationResponse
    form_class = forms.NotificationResponseForm
    template_name = 'util/notification_response_form.html'
    initial = {}

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(NotificationResponseCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:notification_response-list')


class NotificationResponseDetailView(DetailView):
    model = models.NotificationResponse
    template_name = 'util/notification_response_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(NotificationResponseDetailView, self).get_context_data(**kwargs)


class NotificationResponseUpdateView(UpdateAclMixin):
    model = models.NotificationResponse
    form_class = forms.NotificationResponseForm
    template_name = 'util/notification_response_form.html'
    initial = {}
    user_field = 'recipient'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(NotificationResponseUpdateView, self).get_object()

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:notification_response-list')

    def get_initial(self):
        initial = super(NotificationResponseUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class NotificationResponseDeleteView(DeleteAclMixin):
    model = models.NotificationResponse
    success_url = reverse_lazy('util:notification-response-list')
    template_name = 'util/notification_response_confirm_delete.html'
    user_field = 'recipient'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(NotificationResponseDeleteView, self).get_context_data(**kwargs)


class SignatureListView(ListView):
    model = models.Signature
    # paginate_by = 10
    template_name = 'util/signature_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(SignatureListView, self).get_queryset()
            # filter queryset for logged in user
            return queryset
        else:
            return super(SignatureListView, self).get_queryset().none()


class SignatureCreateView(CreateView):
    model = models.Signature
    form_class = forms.SignatureForm
    template_name = 'util/signature_form.html'
    initial = {}

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(SignatureCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:signature-list')


class SignatureDetailView(DetailView):
    model = models.Signature
    template_name = 'util/signature_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(SignatureDetailView, self).get_context_data(**kwargs)


class SignatureUpdateView(ReadOnlyMixin):
    model = models.Signature
    form_class = forms.SignatureForm
    template_name = 'util/signature_form.html'
    initial = {}


class SignatureDeleteView(ReadOnlyMixin):
    model = models.Signature
    success_url = reverse_lazy('util:signature-list')
    template_name = 'util/signature_confirm_delete.html'


class UserAvatarListView(ListView):
    model = models.UserAvatar
    # paginate_by = 10
    template_name = 'util/user_avatar_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(UserAvatarListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(user=user)
            return queryset
        else:
            return super(UserAvatarListView, self).get_queryset().none()


class UserAvatarCreateView(CreateView):
    model = models.UserAvatar
    form_class = forms.UserAvatarForm
    template_name = 'util/user_avatar_form.html'
    initial = {}

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(UserAvatarCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:user_avatar-list')


class UserAvatarDetailView(DetailView):
    model = models.UserAvatar
    template_name = 'util/user_avatar_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserAvatarDetailView, self).get_context_data(**kwargs)


class UserAvatarUpdateView(UpdateAclMixin):
    model = models.UserAvatar
    form_class = forms.UserAvatarForm
    template_name = 'util/user_avatar_form.html'
    initial = {}
    user_field = 'user'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(UserAvatarUpdateView, self).get_object()

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:user_avatar-list')

    def get_initial(self):
        initial = super(UserAvatarUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class UserAvatarDeleteView(DeleteAclMixin):
    model = models.UserAvatar
    success_url = reverse_lazy('util:user-avatar-list')
    template_name = 'util/user_avatar_confirm_delete.html'
    user_field = 'user'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserAvatarDeleteView, self).get_context_data(**kwargs)


class UserPreferencesListView(ListView):
    model = models.UserPreferences
    # paginate_by = 10
    template_name = 'util/user_preferences_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(UserPreferencesListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(user=user)
            return queryset
        else:
            return super(UserPreferencesListView, self).get_queryset().none()


class UserPreferencesCreateView(CreateView):
    model = models.UserPreferences
    form_class = forms.UserPreferencesForm
    template_name = 'util/user_preferences_form.html'
    initial = {}

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(UserPreferencesCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:user_preferences-list')


class UserPreferencesDetailView(DetailView):
    model = models.UserPreferences
    template_name = 'util/user_preferences_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserPreferencesDetailView, self).get_context_data(**kwargs)


class UserPreferencesUpdateView(UpdateAclMixin):
    model = models.UserPreferences
    form_class = forms.UserPreferencesForm
    template_name = 'util/user_preferences_form.html'
    initial = {}
    user_field = 'user'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(UserPreferencesUpdateView, self).get_object()

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:user_preferences-list')

    def get_initial(self):
        initial = super(UserPreferencesUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class UserPreferencesDeleteView(DeleteAclMixin):
    model = models.UserPreferences
    success_url = reverse_lazy('util:user-preferences-list')
    template_name = 'util/user_preferences_confirm_delete.html'
    user_field = 'user'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserPreferencesDeleteView, self).get_context_data(**kwargs)


class UserSignedDocumentListView(ListView):
    model = models.UserSignedDocument
    # paginate_by = 10
    template_name = 'util/user_signed_document_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(UserSignedDocumentListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(owner=user)
            return queryset
        else:
            return super(UserSignedDocumentListView, self).get_queryset().none()


class UserSignedDocumentCreateView(CreateView):
    model = models.UserSignedDocument
    form_class = forms.UserSignedDocumentForm
    template_name = 'util/user_signed_document_form.html'
    initial = {}

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(UserSignedDocumentCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:user_signed_document-list')


class UserSignedDocumentDetailView(DetailView):
    model = models.UserSignedDocument
    template_name = 'util/user_signed_document_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserSignedDocumentDetailView, self).get_context_data(**kwargs)


class UserSignedDocumentUpdateView(UpdateAclMixin):
    model = models.UserSignedDocument
    form_class = forms.UserSignedDocumentForm
    template_name = 'util/user_signed_document_form.html'
    initial = {}
    user_field = 'owner'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(UserSignedDocumentUpdateView, self).get_object()

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:user_signed_document-list')

    def get_initial(self):
        initial = super(UserSignedDocumentUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class UserSignedDocumentDeleteView(DeleteAclMixin):
    model = models.UserSignedDocument
    success_url = reverse_lazy('util:user-signed-document-list')
    template_name = 'util/user_signed_document_confirm_delete.html'
    user_field = 'owner'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserSignedDocumentDeleteView, self).get_context_data(**kwargs)


class UserUploadedDocumentListView(ListView):
    model = models.UserUploadedDocument
    # paginate_by = 10
    template_name = 'util/user_uploaded_document_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(UserUploadedDocumentListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(owner=user)
            return queryset
        else:
            return super(UserUploadedDocumentListView, self).get_queryset().none()


class UserUploadedDocumentCreateView(CreateView):
    model = models.UserUploadedDocument
    form_class = forms.UserUploadedDocumentForm
    template_name = 'util/user_uploaded_document_form.html'
    initial = {}

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(UserUploadedDocumentCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:user_uploaded_document-list')


class UserUploadedDocumentDetailView(DetailView):
    model = models.UserUploadedDocument
    template_name = 'util/user_uploaded_document_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserUploadedDocumentDetailView, self).get_context_data(**kwargs)


class UserUploadedDocumentUpdateView(UpdateAclMixin):
    model = models.UserUploadedDocument
    form_class = forms.UserUploadedDocumentForm
    template_name = 'util/user_uploaded_document_form.html'
    initial = {}
    user_field = 'owner'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(UserUploadedDocumentUpdateView, self).get_object()

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse_lazy('user-profile', kwargs={'slug': self.request.user.urlsafe_uuid})
        else:
            return reverse_lazy('util:user_uploaded_document-list')

    def get_initial(self):
        initial = super(UserUploadedDocumentUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class UserUploadedDocumentDeleteView(DeleteAclMixin):
    model = models.UserUploadedDocument
    success_url = reverse_lazy('util:user-uploaded-document-list')
    template_name = 'util/user_uploaded_document_confirm_delete.html'
    user_field = 'owner'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserUploadedDocumentDeleteView, self).get_context_data(**kwargs)
