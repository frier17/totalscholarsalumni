import re as regex
from datetime import datetime as dt, timedelta
from smtplib import SMTPException
from typing import Sequence

import requests
from django.core.mail import BadHeaderError, send_mail

import tsa.settings as mail_settings
from util import BusinessServices
from util.signals import email_successful_signal, email_error_signal


def app_send_email(api: bool = False, smtp: bool = False, **kwargs) -> bool:
    subject = kwargs.get('subject')
    message = kwargs.get('message')
    from_email = kwargs.get('from_email')
    recipients = kwargs.get('recipients')
    fail_silently = kwargs.get('fail_silently') or False
    auth_user = kwargs.get('auth_user')
    auth_password = kwargs.get('auth_password')
    html_message = kwargs.get('html')
    admin = kwargs.get('admin')
    mail_service = kwargs.get('mail_service') or BusinessServices.USER_REGISTRATION_SERVICE
    request = kwargs.get('request')

    pattern = r'([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)'
    checker = regex.compile(pattern)

    def _api_send(api_key: str, sender: str, email_subject: str, text: str,
                  receivers: Sequence = None, attachments: Sequence = None, html: str = None):
        # evaluate recipients have valid emails
        valid_recipients = [email for email in receivers if checker.fullmatch(email)]
        try:
            if all(valid_recipients):
                response = requests.post(
                        mail_settings.EMAIL_API_URL,
                        auth=('api', api_key),
                        files=attachments,
                        data={
                                'from': sender,
                                'to': valid_recipients,
                                'subject': email_subject,
                                'text': text,
                                'html': html
                        }
                )
                return response.status_code == 200 or response.status_code < 400
        except ConnectionError:
            email_error_signal.send_robust(sender=admin,
                                           asctime=dt.now(),
                                           from_email=from_email,
                                           recipient_email=receivers,
                                           message=message,
                                           mail_service=mail_service,
                                           request=request)

    def _smtp_send(sender: str, email_subject: str, text: str,
                   receivers: Sequence = None, email_user: str = None,
                   email_password: str = None, html: str = None):
        valid_recipients = [email for email in receivers if checker.fullmatch(email)]
        if all(valid_recipients):
            try:
                return send_mail(subject=email_subject, message=text, from_email=sender,
                                 recipient_list=receivers, fail_silently=fail_silently,
                                 auth_user=email_user, auth_password=email_password,
                                 html_message=html)
            except BadHeaderError:
                email_error_signal.send_robust(sender=admin,
                                               asctime=dt.now(),
                                               from_email=from_email,
                                               recipient_email=receivers,
                                               message=message,
                                               mail_service=mail_service)
                raise BadHeaderError(
                        'Invalid email parameters provided. Do ensure right details '
                        'are used')
            except SMTPException:
                email_error_signal.send_robust(sender=admin,
                                               asctime=dt.now(),
                                               from_email=from_email,
                                               recipient_email=receivers,
                                               message=message,
                                               mail_service=mail_service)
                raise
            except ConnectionError:
                email_error_signal.send_robust(sender=admin,
                                               asctime=dt.now(),
                                               from_email=from_email,
                                               recipient_email=receivers,
                                               message=message,
                                               mail_service=mail_service)

    if api:
        email_response = _api_send(
                api_key=mail_settings.EMAIL_API_KEY,
                sender=from_email,
                receivers=recipients,
                email_subject=subject,
                text=message)
        email_pass_signal = email_successful_signal.send_robust(
                sender=admin,
                asctime=dt.now(),
                from_email=from_email,
                recipient_email=recipients,
                message=message,
                mail_service=mail_service)
        email_fail_signal = email_error_signal.send_robust(
                sender=admin,
                asctime=dt.now(),
                from_email=from_email,
                recipient_email=recipients,
                message=message,
                mail_service=mail_service)
        return (email_response and email_pass_signal) or email_fail_signal

    elif smtp:
        email_response = _smtp_send(
                sender=from_email,
                email_subject=subject,
                text=message,
                receivers=recipients,
                email_user=auth_user,
                email_password=auth_password,
                html=html_message)
        email_pass_signal = email_successful_signal.send_robust(
                sender=admin,
                asctime=dt.now(),
                from_email=from_email,
                recipient_email=recipients,
                message=message,
                mail_service=mail_service)
        email_fail_signal = email_error_signal.send_robust(
                sender=admin,
                asctime=dt.now(),
                from_email=from_email,
                recipient_email=recipients,
                message=message,
                mail_service=mail_service)
        return (email_response and email_pass_signal) or email_fail_signal


def generate_otp_email(**kwargs):
    # process parameters and send email of OTP to user's email address
    html_template = """
    <h5>Sample Message from Total Scholars Alumni</h5>
    """
    subject = kwargs.get('subject')
    send_datetime = dt.now()
    end_datetime = send_datetime + timedelta(minutes=mail_settings.OTP_DURATION)
    message = kwargs.get('message')
    from_email = kwargs.get('from_email')
    recipient = kwargs.get('recipient')
    fail_silently = kwargs.get('fail_silently') or False
    auth_user = kwargs.get('auth_user')
    auth_password = kwargs.get('auth_password')
    html_message = kwargs.get('html') or html_template.format(subject)
    otp = kwargs.get('otp')

    return app_send_email(subject=subject, message=message, from_email=from_email,
                          recipient_list=[recipient], fail_silently=fail_silently,
                          auth_user=auth_user, auth_password=auth_password,
                          html_message=None)
