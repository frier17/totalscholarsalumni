import logging
from datetime import datetime as dt

from django.contrib.auth import user_logged_in, user_logged_out, user_login_failed
from django.db import models
from django.db.models.signals import post_save, post_delete, pre_save
from django.dispatch import receiver
from django.http import HttpRequest
from django.utils.safestring import mark_safe

import messages as message_templates
import tsa.settings as settings
from util import BusinessServices
from util import models as um
from util.email import app_send_email
from util.logging import _prepare_message, log_user_error, log_critical, log_user_info, \
    set_notification
from util.signals import *

logger = logging.getLogger(__name__)


@receiver(user_logged_in)
def user_logged_in(sender, **kwargs):
    user = kwargs.get('user')
    request = kwargs.get('request')
    service = BusinessServices.USER_LOGIN.value[1]
    code = BusinessServices.USER_LOGIN.value[0]
    description = message_templates.user_login.format(
        username=user.username,
        last_login=user.last_login,
        remote_addr=request.META.get('REMOTE_ADDR')
    )
    notice = um.Notification.objects.create(
        header=service,
        notification_code=code,
        message=description,
        notification_reason='Performing service request'
    )
    if notice.pk and user:
        notice.recipients.add(user)
    else:
        notice.delete()


@receiver(user_logged_out)
def user_logged_out(sender, **kwargs):
    user = kwargs.get('user')
    request = kwargs.get('request')
    service = BusinessServices.USER_LOGOUT.value[1]
    code = BusinessServices.USER_LOGOUT.value[0]
    description = mark_safe(message_templates.user_login.format(
        username=user.username,
        last_login=user.last_login,
        remote_addr=request.META.get('REMOTE_ADDR')
    ))
    notice = um.Notification(
        header=service,
        notification_code=code,
        message=description
    )
    notice.save(force_insert=True)
    if notice.pk:
        notice.recipients.add(user)


@receiver(user_login_failed)
def user_login_failed(sender, **kwargs):
    request = kwargs.get('request')
    credentials = kwargs.get('credentials')
    if isinstance(request, HttpRequest):
        message = mark_safe(message_templates.user_login_failed.format(
            asctime=dt.now(),
            credentials=credentials,
            referer=request.META.get('REFERER'),
            url=request.path,
            remote_addr=request.META.get('REMOTE_ADDR')
        ))
        log_user_info(request=request, message=message)
        if hasattr(request, 'user') and hasattr(request.user, 'uuid'):
            set_notification(message='Unable to authenticate user with given credentials',
                             user=request.user, header=BusinessServices.USER_LOGIN.name,
                             notification_code=BusinessServices.USER_LOGIN.value[0],
                             notification_reason=BusinessServices.USER_LOGIN.value[1])
    else:
        logger.error(msg='Unable to process login for user.', extra=kwargs)


@receiver(user_black_listed_signal)
def user_black_listed(sender, **kwargs):
    request = kwargs.get('request')
    user = kwargs.get('user')
    block = um.AppBlacklist(
        user=user,
        code=kwargs.get('otp'),
        created_at=kwargs.get('asctime'),
        service=BusinessServices.USER_BLACKLIST.value[1],
    )
    block.save(force_insert=True)
    user.is_active = False
    user.save()
    # log the black list and set notification
    service = BusinessServices.USER_BLACKLIST
    code = BusinessServices.USER_BLACKLIST.value[0]
    service_description = BusinessServices.USER_BLACKLIST.value[1]
    um.ServiceRequest.objects.create(
        service=service,
        request=mark_safe('User authentication blacklisted {user}. '
                          'User record: {username}; '
                          'User ID: {user_id}'.format(
            username=user.username,
            user_id=user.urlsafe_uuid,
            user=user.__class__.__name__)),
        service_description=service_description,
        code=code)
    logger.critical(_prepare_message(request, message_templates.user_black_listed, **kwargs))


@receiver(post_delete, sender='util.BaseUser')
def user_deleted(sender, **kwargs):
    user = kwargs.get('user')
    message = mark_safe(message_templates.user_deleted.format(
        asctime=dt.now(),
        username=user.username,
        user_id=user.urlsafe_uuid,
        requester=sender,
        request_id=None,
        referer=None,
        url=None,
        remote_addr=None))
    logger.critical(message, extra=kwargs)


@receiver(email_successful_signal)
def email_sent(sender, **kwargs):
    request = kwargs.get('request')
    message = _prepare_message(request, message_templates.email_successful, **kwargs)
    log_user_info(request, message=message)


@receiver(otp_generated_signal)
def otp_pre_generated(sender, **kwargs):
    # create new otpreference instance
    code = kwargs.get('otp')
    user = kwargs.get('user')
    if code:
        message = mark_safe(message_templates.otp_pre_generated.format(
            asctime=kwargs.get('asctime'),
            username=user.username,
            user_id=user.urlsafe_uuid,
            otp=code,
            url=None,
            remote_addr=None
        ))

        generated_otp = um.OtpReference.objects.create(
            reference=kwargs.get('reference'),
            user=user,
            key=kwargs.get('key'),
            code_hash=kwargs.get('code_hash'),
            created_at=kwargs.get('created_at'),
            expire_at=kwargs.get('expire_at')
        )
        if generated_otp:
            email_message = "Generated Code for Registration: %s" % code.decode('utf-8')
            response = app_send_email(
                subject=settings.OTP_EMAIL_SUBJECT,
                recipients=[generated_otp.user.email],
                message=email_message,
                html_message=None,
                from_email=settings.OTP_FROM_EMAIL,
                api=True)
            if not response:
                log_critical(message)


@receiver(otp_authenticated_signal)
def otp_authenticated(sender, **kwargs):
    request = kwargs.get('request')
    user = kwargs.get('user')
    code_hash = kwargs.get('code_hash')
    otp_record = um.OtpReference.objects.filter(
        user=user,
        code_hash=code_hash).delete()
    if otp_record:
        message = _prepare_message(request=request,
                                   message=message_templates.otp_authenticated,
                                   kwargs=kwargs)
        log_user_info(request, message=message, **kwargs)
    else:
        log_user_error(request, message='Unable to further process OTP for given user')
        logger.exception('No OTP code or reference does not exist for given user')


@receiver(otp_auth_failed_signal)
def otp_failed(sender, **kwargs):
    request = kwargs.get('request')
    user = kwargs.get('user')
    kwargs.update({'username': user.username, 'user_id': user.urlsafe_uuid})
    message = _prepare_message(request, message_templates.otp_auth_failed, **kwargs)
    log_user_error(request, message=message, **kwargs)


def notification_received(sender, **kwargs):
    return NotImplemented


def notification_sent(sender, **kwargs):
    return NotImplemented


@receiver(post_save, sender='social_network.SocialNetwork')
def user_followed(sender, **kwargs):
    instance = kwargs.get('instance')
    message = message_templates.user_followed.format(
        asctime=instance.created_at,
        follower_id=instance.follower.urlsafe_uuid,
        follower=instance.follower,
        leader=instance.leader,
        leader_id=instance.leader.urlsafe_uuid,
        referer=None,
        url=None,
        remote_addr=None)
    logger.info(message)
    """
    user: um.BaseUser, message: str = None,
                     header: str = None, notification_code: str = None,
                     notification_reason: str = None
    """
    set_notification(user=instance.follower, message=message, header=BusinessServices.SOCIAL_NETWORK.value[1],
                     notification_code=BusinessServices.SOCIAL_NETWORK.value[0],
                     notification_reason='Log of user social activity')


def user_published(sender, **kwargs):
    return NotImplemented


def sms_broadcast_sent(sender, **kwargs):
    return NotImplemented


@receiver(user_inquiry_signal)
def public_inquiry_received(sender, **kwargs):
    recipient = kwargs.get('recipient')
    remote_addr = kwargs.get('remote_addr')
    url = kwargs.get('url')
    referer = kwargs.get('referer')
    asctime = kwargs.get('asctime')
    message = kwargs.get('message')
    user = kwargs.get('user')

    service = BusinessServices.CONTACT_INQUIRY.name
    code = BusinessServices.CONTACT_INQUIRY.value[0]
    service_description = BusinessServices.CONTACT_INQUIRY.value[1]

    if user:
        notice = um.Notification.objects.create(
            header=BusinessServices.CONTACT_INQUIRY.name,
            notification_code=BusinessServices.CONTACT_INQUIRY.value[0],
            message=message,
            notification_reason='Log'
        )
        if notice.pk:
            notice.recipients.add(user)

        um.ServiceRequest.objects.create(
            service=service,
            request=message + message_templates.user_inquiry.format(
                asctime=asctime,
                username=user.username,
                user_id=user.uuid,
                remote_addr=remote_addr,
                url=url,
                referer=referer
            ),
            service_description=service_description,
            code=code)
    else:
        um.ServiceRequest.objects.create(
            service=service,
            request=message,
            service_description=service_description,
            code=code)

    if isinstance(recipient, (list, tuple)):
        response = app_send_email(api=True, recipients=recipient, subject=service,
                                  message=message_templates.contact_auto_response.format(
                                      asctime=asctime),
                                  mail_service=service, from_email=settings.DEFAULT_FROM_EMAIL)
        if not response:
            log_critical(message)
    elif isinstance(recipient, str):
        response = app_send_email(api=True, recipients=[recipient], subject=service,
                                  message=message_templates.contact_auto_response.format(
                                      asctime=asctime),
                                  mail_service=service, from_email=settings.OTP_FROM_EMAIL)
        if not response:
            log_critical(message)


def email_broadcast_sent(sender, **kwargs):
    return NotImplemented


@receiver(post_save, sender='util.UserAvatar')
def avatar_saved(sender, **kwargs):
    instance = kwargs.get('instance')
    from PIL import Image
    try:
        im = Image.open(instance.media)
        height = instance.media.height
        ratio = settings.DEFAULT_IMAGE_HEIGHT / height
        im.resize((int(ratio * instance.media.width), int(ratio * height)), Image.ANTIALIAS)
        im.save(instance.media.name)
    except IOError:
        user_id = instance.user.uuid
        log_critical(message=f'Unable to save image resized for user: {user_id}')


@receiver(multiple_user_id_signal)
def shared_user_credentials(sender, **kwargs):
    mobile = kwargs.get('mobile')
    username = kwargs.get('username')
    users = um.BaseUser.objects.filter(
        models.Q(mobile=mobile) | models.Q(username=username)).distinct()
    notice = um.Notification.objects.create(
        header=BusinessServices.USER_MANAGEMENT.name,
        notification_code=BusinessServices.USER_MANAGEMENT.value[0],
        message=message_templates.multiple_user_id.format(
            asctime=dt.now(),
            credentials='Mobile or Username',
        ),
        notification_reason='Log of shared user credentials'
    )
    if notice.pk:
        for u in users:
            notice.recipients.add(u)


@receiver(email_error_signal)
def email_failed(sender, **kwargs):
    return NotImplemented
