from django.core.signals import Signal

"""
List the various application signals for managing business domain logic. All signals with 
'message' parameter in their providing_args can be sent with string formatted messages. 
Signals with 'html' can be sent with HTML template specified as a file name that can be 
loaded from the template directory. Any keyword arguments can be used for message and html 
formatting. 
Default keys for the message and html templates are:
asctime: the current date time which supports string formatting as used in strtime()

"""

# Defines the signal sent when an email is successfullappy sent by application
email_successful_signal = Signal(
        providing_args=['asctime', 'from_email', 'recipient_email', 'message',
                        'mail_service'])

# Defines the signal sent when an attempt to sen email fails or no email was sent by the
# send_mail function
email_error_signal = Signal(
        providing_args=['asctime', 'from_email', 'recipient_email', 'message',
                        'mail_service'])

# Defines the signal sent when the OTP generated is successfully authenticated by a new user
otp_authenticated_signal = Signal(providing_args=[
        'otp', 'user', 'device', 'asctime', 'request', 'code_hash', 'created_at',
        'expire_at', 'key', 'reference'])

# Defines the signal sent when a user is blacklisted for failing to provide correct OTP
# after several trials
user_black_listed_signal = Signal(providing_args=['otp', 'user', 'ip', 'device',
                                                  'asctime', 'request'])

# Defines signal sent when user OTP authentication fails
otp_auth_failed_signal = Signal(
    providing_args=['otp', 'request', 'asctime', 'code_hash', 'sender', 'user'])

# Defines the signal sent when a user submits an inquiry through contact form
user_inquiry_signal = Signal(providing_args=['recipient', 'remote_addr', 'asctime'])

# Defines the signal when a user is registered with shared credentials with an existing user
multiple_user_id_signal = Signal(providing_args=['username', 'mobile'])

# Defines signal when OTP is generated for a new user
otp_generated_signal = Signal(providing_args=['otp', 'request', 'asctime'])

# Defines signal when creation of OTP fails for a new user
create_otp_failed_signal = Signal(providing_args=['user'])

# Defines the signal sent when an invitation to register is forwarded to recipients
invitation_to_register_signal = Signal(providing_args=['recipients', 'sender', 'message',
                                                       'html'])

# Defines the signal sent when an invitation to view TSA website is forwarded to recipients
invitation_to_view = Signal(providing_args=['recipients', 'sender', 'message', 'html'])

# Defines the signal sent when an invitation to view a given user's profile is forwarded to
# recipients
invitation_to_view_profile = Signal(providing_args=['recipients', 'sender', 'message', 'html'])

# Defines the signal sent when a recorded OtpReference is deleted after user is authenticated
otp_deleted_signal = Signal(providing_args=['otp', 'user', ])

# Defines the signal sent when an instance of a model is published for public viewing
# recipients can be a list of users or default '__all__' to make accessible to all members
object_published = Signal(providing_args=['recipients', 'publisher', 'instance'])

# Defines the signal sent when request by the user (requester) has been approved by
# admin/manager with appropriate permissions
request_approved = Signal(providing_args=['requester', 'approval', 'manager'])

# Defines the signal sent when a user follows a leader
user_followed = Signal(providing_args=['follower', 'leader', 'message', 'html'])

# Defines the signal sent when a leader blocks a follower
user_blocked = Signal(providing_args=['follower', 'leader', 'message', 'html'])

# Defines the signal sent when a user views the profile of a member
user_viewed_profile = Signal(providing_args=['member', 'user', 'asctime' 'end'])
