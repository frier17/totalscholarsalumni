from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from . import models

admin.register(models.BaseUser, UserAdmin)
admin.register(models.Approval)
admin.register(models.BusinessParty)
admin.register(models.Address)
admin.register(models.NotificationResponse)
admin.register(models.Notification)
admin.register(models.RecordedStatus)
admin.register(models.Signature)
admin.register(models.ServiceResponse)
admin.register(models.ServiceRequest)
admin.register(models.NamedCode)
admin.register(models.Institution)
admin.register(models.UserSignedDocument)
