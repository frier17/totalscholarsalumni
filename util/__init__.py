import base64
import datetime as dt
import hashlib
import hmac
import os
import random
import uuid
from enum import Enum
from enum import IntFlag
from hashlib import sha256

import magic
from django.utils import timezone

from tsa.settings import MEDIA_PATH, FILE_HASH_KEY

default_app_config = 'util.apps.UtilConfig'

char_default = {
    'max_length': 100,
}

date_default = {
    'auto_now': False,
    'auto_now_add': False,
}

currency_default = {
    'max_digits': 20,
    'decimal_places': 2,
    'default': 0.00
}

datetime_default = date_default

decimal_default = {
    'max_digits': 20,
    'decimal_places': 5,
    'default': 0.00000
}

file_default = {
    'upload_to': MEDIA_PATH,
    'max_length': 100
}

code_default = {
    'max_length': 15
}

bool_default = {
    'default': False
}

image_default = {
    'height_field': 150,
    'width_field': 150,
}

name_default = dict(max_length=256)

identifier_default = {
    'max_length': 256,
}

time_default = date_default

text_default = {
    'null': True,
}

options = {
    'null': False,
    'blank': False,
    'choices': None,
    'default': None,
    'editable': False,
    'error_messages': None,
    'help_text': '',
    'primary_key': False,
    'unique': False,
    'unique_for_date': '',
    'unique_for_month': '',
    'unique_for_year': '',
    'verbose_name': '',
    'validators': None,
}


class DocumentTypes(IntFlag):
    pass


def generate_uuid():
    # @todo: test function
    # return the unique identifier
    return uuid.uuid4()


def version_id_generator():
    # @todo: implement function
    return str(uuid.uuid4()).replace('-', '')[:8]


def generate_doc_hash():
    # @todo: test function
    doc_hash = sha256(str(random.random()).encode()).hexdigest()
    return base64.b64encode(doc_hash)


def check_file_size(file_object, allowed=1048576):
    if file_object.size > allowed:
        raise ValueError('File size for avatar image greater than approved value. File '
                         'size should not exceed %d' % allowed)
    return file_object.size


def check_file_hash(instance):
    buffer = list(instance.media.chunks())
    # fh = hashlib.sha256(repr(buffer).encode('utf-8')).hexdigest()

    return hmac.new(FILE_HASH_KEY, msg=repr(buffer).encode('utf-8'), digestmod='sha256').hexdigest()


def file_upload(instance, filename):
    approved_mimes = ['application/epub+zip', 'application/gzip', 'application/java-archive',
                      'application/json', 'application/ld+json', 'application/msword',
                      'application/octet-stream', 'application/ogg', 'application/pdf',
                      'application/rtf', 'application/vnd.amazon.ebook',
                      'application/vnd.apple.installer+xml', 'application/vnd.mozilla.xul+xml',
                      'application/vnd.ms-excel', 'application/vnd.ms-fontobject',
                      'application/vnd.ms-powerpoint',
                      'application/vnd.oasis.opendocument.presentation',
                      'application/vnd.oasis.opendocument.spreadsheet',
                      'application/vnd.oasis.opendocument.text',
                      'application/vnd.openxmlformats-officedocument.presentationml'
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                      'application/vnd.openxmlformats-officedocument.wordprocessingml',
                      'application/vnd.visio', 'application/x-7z-compressed',
                      'application/x-abiword',
                      'application/x-bzip', 'application/x-bzip2', 'application/x-csh',
                      'application/x-freearc', 'application/xhtml+xml', 'application/xml',
                      'application/x-rar-compressed', 'application/x-sh',
                      'application/x-shockwave-flash',
                      'application/x-tar', 'application/zip', 'audio/aac',
                      'audio/midi audio/x-midi',
                      'audio/mpeg', 'audio/ogg', 'audio/opus', 'audio/wav', 'audio/webm',
                      'font/otf', 'font/ttf', 'font/woff', 'font/woff2', 'image/bmp',
                      'image/gif',
                      'image/jpeg', 'image/png', 'image/svg+xml', 'image/tiff',
                      'image/vnd.microsoft.icon',
                      'image/webp', 'text/calendar', 'text/css', 'text/csv', 'text/html',
                      'text/javascript', 'text/javascript', 'text/plain', 'video/3gpp',
                      'video/3gpp2', 'video/mp2t', 'video/mpeg', 'video/ogg', 'video/webm',
                      'video/x-msvideo',
                      ]
    audio = [x for x in approved_mimes if x in (
        'audio/aac', 'audio/midi audio/x-midi', 'audio/mpeg', 'audio/ogg', 'audio/opus', 'audio/wav', 'audio/webm',)]
    document = [x for x in approved_mimes if
                x in (
                    'application/pdf',
                    'application/vnd.ms-excel', 'application/vnd.ms-powerpoint',
                    'application/vnd.oasis.opendocument.presentation',
                    'application/vnd.oasis.opendocument.spreadsheet',
                    'application/vnd.oasis.opendocument.text',
                    'application/vnd.openxmlformats-officedocument.presentationml'
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml',
                    'application/vnd.visio', 'application/x-abiword',)]
    image = [x for x in approved_mimes if x in ('image/gif', 'image/jpeg', 'image/png', 'image/svg+xml', 'image/tiff',
                                                'image/vnd.microsoft.icon', 'image/webp')]
    video = [x for x in approved_mimes if
             x in ('video/3gpp', 'video/3gpp2', 'video/mp2t', 'video/mpeg', 'video/ogg', 'video/webm',
                   'video/x-msvideo')]
    media = {
        'audio': audio,
        'document': document,
        'image': image,
        'video': video,
    }
    prefix = 'file'
    try:
        directory, basename = os.path.split(filename)
        _, ext = os.path.splitext(basename)
        _date = dt.date.today()
        file_id = str(generate_uuid())
        fragments = _date.strftime("%Y/%m/%d").split('/')
        file_path = os.path.join(*fragments, file_id)
        name = '%s%s' % (file_path, ext)
        if hasattr(instance, 'label'):
            instance.label = basename

        # Set the prefix base on the nature of file: media/document
        if instance.media:
            check_file_hash(instance)
            file_mime = magic.from_buffer(instance.media.readline(), mime=True)
            instance.media_type = file_mime
            if file_mime in media['image']:
                check_file_size(instance.media, allowed=1 * pow(2, 20))
                prefix = 'image'
            elif file_mime in media['document']:
                check_file_size(instance.media, allowed=4 * pow(2, 20))
                prefix = 'document'
            elif file_mime in media['video']:
                prefix = 'video'
            elif file_mime in media['audio']:
                prefix = 'audio'
            else:
                prefix = 'media'

        # save to the category under the upload date directory
        return "{0}/{1}/".format(prefix, name)
    except IOError:
        raise
    except Exception:
        raise


def generate_work_id():
    allowed_chars = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
                     'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3',
                     '4', '5', '6', '7', '8', '9', ]
    return ''.join(random.choices(population=allowed_chars, k=8))


def current_timezone(current: dt.datetime):
    return timezone.make_aware(dt.datetime.now(), timezone.get_current_timezone())


binary_default = dict(editable=False, max_length=500)

GENDER = (
    ('M', 'Male'),
    ('F', 'Female')
)

MOBILE_DIAL_CODES = (
    ("93", "Afghanistan: 93"),
    ("355", "Albania: 355"),
    ("213", "Algeria: 213"),
    ("1-684", "American Samoa: 1-684"),
    ("376", "Andorra: 376"),
    ("244", "Angola: 244"),
    ("1-264", "Anguilla: 1-264"),
    ("672", "Antarctica: 672"),
    ("1-268", "Antigua and Barbuda: 1-268"),
    ("54", "Argentina: 54"),
    ("374", "Armenia: 374"),
    ("297", "Aruba: 297"),
    ("61", "Australia: 61"),
    ("43", "Austria: 43"),
    ("994", "Azerbaijan: 994"),
    ("1-242", "Bahamas: 1-242"),
    ("973", "Bahrain: 973"),
    ("880", "Bangladesh: 880"),
    ("1-246", "Barbados: 1-246"),
    ("375", "Belarus: 375"),
    ("32", "Belgium: 32"),
    ("501", "Belize: 501"),
    ("229", "Benin: 229"),
    ("1-441", "Bermuda: 1-441"),
    ("975", "Bhutan: 975"),
    ("591", "Bolivia: 591"),
    ("599", "Bonaire: 599"),
    ("387", "Bosnia and Herzegovina: 387"),
    ("267", "Botswana: 267"),
    ("47", "Bouvet Island: 47"),
    ("55", "Brazil: 55"),
    ("246", "British Indian Ocean Territory: 246"),
    ("673", "Brunei Darussalam: 673"),
    ("359", "Bulgaria: 359"),
    ("226", "Burkina Faso: 226"),
    ("257", "Burundi: 257"),
    ("855", "Cambodia: 855"),
    ("237", "Cameroon: 237"),
    ("1", "Canada: 1"),
    ("238", "Cape Verde: 238"),
    ("1-345", "Cayman Islands: 1-345"),
    ("236", "Central African Republic: 236"),
    ("235", "Chad: 235"),
    ("56", "Chile: 56"),
    ("86", "China: 86"),
    ("61", "Christmas Island: 61"),
    ("61", "Cocos (Keeling) Islands: 61"),
    ("57", "Colombia: 57"),
    ("269", "Comoros: 269"),
    ("242", "Congo: 242"),
    ("243", "Democratic Republic of the Congo: 243"),
    ("682", "Cook Islands: 682"),
    ("506", "Costa Rica: 506"),
    ("385", "Croatia: 385"),
    ("53", "Cuba: 53"),
    ("599", "Curacao: 599"),
    ("357", "Cyprus: 357"),
    ("420", "Czech Republic: 420"),
    ("225", "Cote d'Ivoire: 225"),
    ("45", "Denmark: 45"),
    ("253", "Djibouti: 253"),
    ("1-767", "Dominica: 1-767"),
    ("1-809,1-829,1-849", "Dominican Republic: 1-809,1-829,1-849"),
    ("593", "Ecuador: 593"),
    ("20", "Egypt: 20"),
    ("503", "El Salvador: 503"),
    ("240", "Equatorial Guinea: 240"),
    ("291", "Eritrea: 291"),
    ("372", "Estonia: 372"),
    ("251", "Ethiopia: 251"),
    ("500", "Falkland Islands (Malvinas): 500"),
    ("298", "Faroe Islands: 298"),
    ("679", "Fiji: 679"),
    ("358", "Finland: 358"),
    ("33", "France: 33"),
    ("594", "French Guiana: 594"),
    ("689", "French Polynesia: 689"),
    ("262", "French Southern Territories: 262"),
    ("241", "Gabon: 241"),
    ("220", "Gambia: 220"),
    ("995", "Georgia: 995"),
    ("49", "Germany: 49"),
    ("233", "Ghana: 233"),
    ("350", "Gibraltar: 350"),
    ("30", "Greece: 30"),
    ("299", "Greenland: 299"),
    ("1-473", "Grenada: 1-473"),
    ("590", "Guadeloupe: 590"),
    ("1-671", "Guam: 1-671"),
    ("502", "Guatemala: 502"),
    ("44", "Guernsey: 44"),
    ("224", "Guinea: 224"),
    ("245", "Guinea-Bissau: 245"),
    ("592", "Guyana: 592"),
    ("509", "Haiti: 509"),
    ("672", "Heard Island and McDonald Islands: 672"),
    ("379", "Holy See (Vatican City State): 379"),
    ("504", "Honduras: 504"),
    ("852", "Hong Kong: 852"),
    ("36", "Hungary: 36"),
    ("354", "Iceland: 354"),
    ("91", "India: 91"),
    ("62", "Indonesia: 62"),
    ("98", "Iran, Islamic Republic of: 98"),
    ("964", "Iraq: 964"),
    ("353", "Ireland: 353"),
    ("44", "Isle of Man: 44"),
    ("972", "Israel: 972"),
    ("39", "Italy: 39"),
    ("1-876", "Jamaica: 1-876"),
    ("81", "Japan: 81"),
    ("44", "Jersey: 44"),
    ("962", "Jordan: 962"),
    ("7", "Kazakhstan: 7"),
    ("254", "Kenya: 254"),
    ("686", "Kiribati: 686"),
    ("850", "Korea, Democratic People's Republic of: 850"),
    ("82", "Korea, Republic of: 82"),
    ("965", "Kuwait: 965"),
    ("996", "Kyrgyzstan: 996"),
    ("856", "Lao People's Democratic Republic: 856"),
    ("371", "Latvia: 371"),
    ("961", "Lebanon: 961"),
    ("266", "Lesotho: 266"),
    ("231", "Liberia: 231"),
    ("218", "Libya: 218"),
    ("423", "Liechtenstein: 423"),
    ("370", "Lithuania: 370"),
    ("352", "Luxembourg: 352"),
    ("853", "Macao: 853"),
    ("389", "Macedonia, the Former Yugoslav Republic of: 389"),
    ("261", "Madagascar: 261"),
    ("265", "Malawi: 265"),
    ("60", "Malaysia: 60"),
    ("960", "Maldives: 960"),
    ("223", "Mali: 223"),
    ("356", "Malta: 356"),
    ("692", "Marshall Islands: 692"),
    ("596", "Martinique: 596"),
    ("222", "Mauritania: 222"),
    ("230", "Mauritius: 230"),
    ("262", "Mayotte: 262"),
    ("52", "Mexico: 52"),
    ("691", "Micronesia, Federated States of: 691"),
    ("373", "Moldova, Republic of: 373"),
    ("377", "Monaco: 377"),
    ("976", "Mongolia: 976"),
    ("382", "Montenegro: 382"),
    ("1-664", "Montserrat: 1-664"),
    ("212", "Morocco: 212"),
    ("258", "Mozambique: 258"),
    ("95", "Myanmar: 95"),
    ("264", "Namibia: 264"),
    ("674", "Nauru: 674"),
    ("977", "Nepal: 977"),
    ("31", "Netherlands: 31"),
    ("687", "New Caledonia: 687"),
    ("64", "New Zealand: 64"),
    ("505", "Nicaragua: 505"),
    ("227", "Niger: 227"),
    ("234", "Nigeria: 234"),
    ("683", "Niue: 683"),
    ("672", "Norfolk Island: 672"),
    ("1-670", "Northern Mariana Islands: 1-670"),
    ("47", "Norway: 47"),
    ("968", "Oman: 968"),
    ("92", "Pakistan: 92"),
    ("680", "Palau: 680"),
    ("970", "Palestine, State of: 970"),
    ("507", "Panama: 507"),
    ("675", "Papua New Guinea: 675"),
    ("595", "Paraguay: 595"),
    ("51", "Peru: 51"),
    ("63", "Philippines: 63"),
    ("870", "Pitcairn: 870"),
    ("48", "Poland: 48"),
    ("351", "Portugal: 351"),
    ("1", "Puerto Rico: 1"),
    ("974", "Qatar: 974"),
    ("40", "Romania: 40"),
    ("7", "Russian Federation: 7"),
    ("250", "Rwanda: 250"),
    ("262", "Reunion: 262"),
    ("590", "Saint Barthelemy: 590"),
    ("290", "Saint Helena: 290"),
    ("1-869", "Saint Kitts and Nevis: 1-869"),
    ("1-758", "Saint Lucia: 1-758"),
    ("590", "Saint Martin (French part): 590"),
    ("508", "Saint Pierre and Miquelon: 508"),
    ("1-784", "Saint Vincent and the Grenadines: 1-784"),
    ("685", "Samoa: 685"),
    ("378", "San Marino: 378"),
    ("239", "Sao Tome and Principe: 239"),
    ("966", "Saudi Arabia: 966"),
    ("221", "Senegal: 221"),
    ("381", "Serbia: 381"),
    ("248", "Seychelles: 248"),
    ("232", "Sierra Leone: 232"),
    ("65", "Singapore: 65"),
    ("1-721", "Sint Maarten (Dutch part): 1-721"),
    ("421", "Slovakia: 421"),
    ("386", "Slovenia: 386"),
    ("677", "Solomon Islands: 677"),
    ("252", "Somalia: 252"),
    ("27", "South Africa: 27"),
    ("500", "South Georgia and the South Sandwich Islands: 500"),
    ("211", "South Sudan: 211"),
    ("34", "Spain: 34"),
    ("94", "Sri Lanka: 94"),
    ("249", "Sudan: 249"),
    ("597", "Suriname: 597"),
    ("47", "Svalbard and Jan Mayen: 47"),
    ("268", "Swaziland: 268"),
    ("46", "Sweden: 46"),
    ("41", "Switzerland: 41"),
    ("963", "Syrian Arab Republic: 963"),
    ("886", "Taiwan: 886"),
    ("992", "Tajikistan: 992"),
    ("255", "United Republic of Tanzania: 255"),
    ("66", "Thailand: 66"),
    ("670", "Timor-Leste: 670"),
    ("228", "Togo: 228"),
    ("690", "Tokelau: 690"),
    ("676", "Tonga: 676"),
    ("1-868", "Trinidad and Tobago: 1-868"),
    ("216", "Tunisia: 216"),
    ("90", "Turkey: 90"),
    ("993", "Turkmenistan: 993"),
    ("1-649", "Turks and Caicos Islands: 1-649"),
    ("688", "Tuvalu: 688"),
    ("256", "Uganda: 256"),
    ("380", "Ukraine: 380"),
    ("971", "United Arab Emirates: 971"),
    ("44", "United Kingdom: 44"),
    ("1", "United States: 1"),
    ("1", "United States Minor Outlying Islands: 1"),
    ("598", "Uruguay: 598"),
    ("998", "Uzbekistan: 998"),
    ("678", "Vanuatu: 678"),
    ("58", "Venezuela: 58"),
    ("84", "Viet Nam: 84"),
    ("1-284", "British Virgin Islands: 1-284"),
    ("1-340", "US Virgin Islands: 1-340"),
    ("681", "Wallis and Futuna: 681"),
    ("212", "Western Sahara: 212"),
    ("967", "Yemen: 967"),
    ("260", "Zambia: 260"),
    ("263", "Zimbabwe: 263"),
)


class BusinessServices(Enum):
    USER_REGISTRATION_SERVICE = (1, 'TSA user registration service')
    VOLUNTEER_COURSE_REGISTRATION = (2, 'TSA volunteer service')
    JOB_ADVERT = (3, 'TSA job advertisement')
    OTP_AUTHENTICATION = (4, 'OTP Authentication service')
    CONTACT_INQUIRY = (5, 'Inquiry')
    USER_LOGIN = (6, 'User password login service')
    PIN_LOGIN = (7, 'User mobile pin login service')
    USER_LOGOUT = (8, 'User log out service')
    USER_BLACKLIST = (9, 'Application Authentication service')
    COMMUNICATION_SERVICE = (10, 'Application communication service via Email or SMS')
    USER_MANAGEMENT = (11, 'Multiple users with shared credentials')
    SOCIAL_NETWORK = (12, 'TSA social networking service')
