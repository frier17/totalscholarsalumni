import datetime as dt
from collections import OrderedDict
from enum import Enum
from typing import Any

import stringcase
from django.contrib.auth.models import AbstractUser, UserManager
from django.contrib.contenttypes.fields import GenericForeignKey, \
    GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import gettext_lazy as _

from tsa.settings import MEDIA_TYPES
from util import name_default, code_default, datetime_default, \
    text_default, date_default, generate_work_id, generate_uuid, version_id_generator, \
    file_upload, current_timezone, check_file_hash
from .manager import SoftDeleteManager
from .signals import multiple_user_id_signal


class ModelUrlSafeMixin(models.Model):
    uuid = models.UUIDField(
        primary_key=False,
        unique=True,
        editable=False,
        max_length=100,
        default=generate_uuid)
    slug = models.SlugField(editable=False)

    class Meta:
        abstract = True

    @property
    def urlsafe_uuid(self):
        return urlsafe_base64_encode(str(self.uuid).encode())

    def get_absolute_url(self):
        app, model = str(self._meta.label).split('.', maxsplit=1)
        app = app.lower()
        model = stringcase.spinalcase(model).lower()
        model_url = f'{app}:{model}-detail'
        return reverse_lazy(model_url, kwargs={'slug': self.slug}, current_app=app)


class PublishMixin(models.Model):
    broadcasts = None
    broadcast_order = []
    excluded_broadcasts = []
    broadcast_template = None

    is_public = models.BooleanField(default=False, help_text=_('Enable to make record '
                                                               'visible to all members'))
    is_shareable = models.BooleanField(default=False, help_text=_('Enable to make this '
                                                                  'record shareable on site'))
    objects = models.Manager()

    def publish(self, queryset: models.QuerySet = None, **kwargs) -> Any:
        # @todo: Review if to return dict or queryset
        fields = []
        if not queryset:
            queryset = self.objects.all()
        else:
            queryset = self.objects.filter(**kwargs)

        if self.broadcasts == '__all__' or not self.broadcasts:
            fields = self._meta.get_fields()
        if self.excluded_broadcasts:
            fields = list(set(self.broadcasts) - set(self.excluded_broadcasts))
        if self.broadcast_order:
            a = OrderedDict().fromkeys(self.broadcast_order, None)
            b = OrderedDict().fromkeys(fields, None)
            a.update(b)
            fields = list(a.keys())
        return queryset.values(fields)

    class Meta:
        abstract = True


class FileUploadMixin(models.Model):
    media = models.FileField(upload_to=file_upload, null=True, blank=True, help_text=_(
        'Upload relevant document other materials (e.g video works) you '
        'wish to share or submit'), )
    media_type = models.CharField(choices=MEDIA_TYPES, null=True, blank=True, **code_default)
    file_hash = models.CharField(max_length=100, editable=False, null=True)
    allow_download = models.BooleanField(default=False)
    label = models.CharField(null=True, blank=True, **name_default)

    def save(self, *args, **kwargs):
        if self.media:
            fh = check_file_hash(self)
            if not self.file_hash:
                self.file_hash = fh
        return super(FileUploadMixin, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class BaseModel(ModelUrlSafeMixin):
    objects = models.Manager()

    def save(self, *args, **kwargs):
        if self.uuid is None:
            self.uuid = generate_uuid()

        if not self.slug:
            uuid = str(self.uuid)
            self.slug = urlsafe_base64_encode(str(self.uuid).encode())
        return super(BaseModel, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.__class__.__name__} with reference {self.slug}'

    class Meta:
        abstract = True


class SoftDeleteMixin(PublishMixin, BaseModel):
    created_at = models.DateTimeField(default=timezone.now, editable=False)
    last_modified_at = models.DateTimeField(auto_now=True, editable=False)
    deleted_at = models.DateTimeField(auto_now_add=False, null=True, editable=False)
    force_delete = models.BooleanField(default=False, editable=False)
    objects = SoftDeleteManager()

    def save(self, *args, **kwargs):
        if self.created_at is None:
            self.created_at = current_timezone(dt.datetime.now())
        return super(SoftDeleteMixin, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        # @todo: test implementation
        # apply the soft delete concept on delete
        # if force_delete = True, then delete from db irrespective of settings
        if self.force_delete:
            return super(SoftDeleteMixin, self).delete(*args, **kwargs)
        else:
            self.deleted_at = current_timezone(dt.datetime.now())
            return self.pk

    class Meta:
        abstract = True
        ordering = ['created_at']
        get_latest_by = 'last_modified_at'


class BaseUser(PublishMixin, ModelUrlSafeMixin, AbstractUser):
    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']
    GENDER = [('M', 'Male'), ('F', 'Female')]
    email = models.EmailField(_('email address'), unique=True)
    work_id = models.CharField(**name_default)
    mobile = models.CharField(**name_default)
    password = models.CharField(_('password'), max_length=128)
    gender = models.CharField(_('gender'), choices=GENDER, null=True, **code_default)
    title = models.CharField(_('title'), max_length=10, null=True, blank=True)
    middle_name = models.CharField(_('middle name'), max_length=50, null=True, blank=True)
    autobiography = models.TextField(help_text='Provide a short description of yourself. You '
                                               'may also share your goals, mission, '
                                               'or life vision here', **text_default)
    date_of_birth = models.DateField(null=True, blank=True, **date_default)
    pin = models.CharField(null=True, **code_default)
    social_likes = GenericRelation('social_network.UserLikes',
                                   related_query_name='member')  # references members liked by this user
    is_staff = models.BooleanField(default=False)
    addresses = GenericRelation('Address', related_query_name='addressee')

    objects = UserManager()

    def save(self, *args, **kwargs):
        if self.username == '<DEFAULT_USERNAME>' or not self.username:
            self.username = generate_work_id()
            if BaseUser.objects.filter(
                    models.Q(mobile=self.mobile) | models.Q(username=self.username)):
                multiple_user_id_signal.send(sender=self.__class__, username=self.username,
                                             mobile=self.mobile)
        if not self.work_id:
            self.work_id = generate_work_id()
        if self.slug is None:
            self.slug = urlsafe_base64_encode(str(self.uuid).encode())
        return super(BaseUser, self).save(*args, **kwargs)

    def publish(self, **kwargs):
        self.broadcasts = ['created_at', 'last_login', 'username', 'first_name',
                           'middle_name', 'last_name', 'gender', 'date_joined']
        self.broadcast_order = ['title', 'first_name', 'middle_name', 'last_name',
                                'date_joined', 'last_login']
        post = super(BaseUser, self).publish(**kwargs)
        return post

    @property
    def public_identity(self):
        return f'{str(self.title).title()} {self.first_name} {str(self.last_name).upper()}'


class ApprovedStaff(SoftDeleteMixin):
    staff = models.OneToOneField('BaseUser', on_delete=models.DO_NOTHING)
    approved = models.BooleanField(default=False)


class UserUploadedDocument(SoftDeleteMixin):
    owner = models.ForeignKey('BaseUser', on_delete=models.DO_NOTHING, related_name='uploaded_document')


class UserSignedDocument(SoftDeleteMixin):
    owner = models.ForeignKey('BaseUser', on_delete=models.DO_NOTHING)
    description = models.TextField()
    issued_at = models.DateTimeField(null=True)
    revised_at = models.DateTimeField(null=True)
    # @todo: generate version from the file content hash
    version_id = models.CharField(default=version_id_generator,
                                  editable=False, **name_default)
    label = models.CharField(**name_default)
    # generate hash for document using authentication and user secret key/info
    # Example Google Authenticator or 2FA may be used to generate hash
    signatures = models.ManyToManyField('util.Signature')
    uploaded_document = models.OneToOneField(
        'UserUploadedDocument',
        on_delete=models.DO_NOTHING,
        related_name='signed_document', null=True)

    def save(self, *args, **kwargs):
        super(UserSignedDocument, self).save(*args, **kwargs)


class ServiceRequest(SoftDeleteMixin):
    service = models.CharField(**name_default)
    request = models.TextField(**text_default)
    service_description = models.TextField(null=True)
    code = models.CharField(null=True, blank=True, **code_default)
    requester = models.ForeignKey('BaseUser', null=True, on_delete=models.CASCADE)


class ServiceResponse(SoftDeleteMixin):
    response_datetime = models.DateField(auto_now_add=True)
    is_accepted_request = models.BooleanField(default=False)
    response = models.TextField()
    request = models.ForeignKey('ServiceRequest', on_delete=models.DO_NOTHING)


class Address(BaseModel):
    web = models.URLField(null=True)
    street_address = models.TextField(**name_default)
    house_number = models.CharField(null=True, **name_default)
    house_name = models.CharField(null=True, **name_default)
    city = models.CharField(null=True, **name_default)
    state = models.CharField(null=True, **name_default)
    country = models.CharField(null=True, **name_default)
    post_code = models.CharField(null=True, **code_default)
    forwarding_address = models.TextField(null=True)
    description = models.TextField(null=True, help_text=_('Any added information to '
                                                          'describing this address'))
    is_residential_address = models.BooleanField(default=False, help_text=_(
        'Tick if this is a residential address'))
    is_work_address = models.BooleanField(default=False, help_text=_(
        'Tick if this is a work address'))
    is_proxy_address = models.BooleanField(default=False, help_text=_(
        'Tick if this is a proxy or third party address'))
    alt_emails = models.EmailField()
    is_primary_address = models.BooleanField(default=False, help_text=_(
        'Tick if this is your primary address'))

    content_type = models.ForeignKey(ContentType, on_delete=models.DO_NOTHING)
    object_id = models.CharField(**name_default)
    content_object = GenericForeignKey()


class BusinessParty(BaseModel):
    owners = models.ForeignKey('BaseUser', on_delete=models.DO_NOTHING,
                               null=True)
    cac_reference = models.CharField(
        help_text='CAC RC or unique reference for registered business',
        null=True, **name_default)
    addresses = GenericRelation(Address, related_query_name='business_party')


class NamedCode(BaseModel):
    # The name or string repr which can be parsed by target enum type.
    # The enum will have a default OTHER for cases not yet identified
    # Enum flag OTHER will enable collection and processing
    name = models.CharField(default='UNASSIGNED', **name_default)
    code_value = models.CharField(default='UNASSIGNED', **code_default)
    meaning = models.TextField()

    def parse_to_enum(self, enum_type=None):
        # @todo: test function
        # return a given enum member for the given name
        if isinstance(enum_type, Enum):
            is_member = any([(n, m) for n, m in enum_type.__members__.items()
                             if self.name == n])
            if is_member:
                return enum_type[self.name]

    def parse_from_enum(self, enum_type=None, enum_value=None,
                        enum_name=None, meaning=None):
        # @todo: test function
        # for the given enum type, and value create instance record
        if not isinstance(enum_type, Enum):
            raise RuntimeError('Expected Enum or subclass. {0} '
                               'provided'.format(type(enum_type)))
        if enum_value and not enum_name:
            enum_name = enum_type(enum_value).name
            instance = self.objects.create(name=enum_name,
                                           enum_value=enum_value,
                                           meaning=meaning)
            instance.save()
        elif enum_name:
            is_member = any([(n, m) for n, m in enum_type.__members__.items()
                             if n == enum_name])
            if is_member:
                instance = \
                    self.objects.create(
                        name=enum_name,
                        enum_value=enum_type[enum_name],
                        meaning=meaning)
                instance.save()


class Signature(SoftDeleteMixin):
    # Serves as a digital hash or the hash of a human signature depicting the person's approval
    signature_hash = models.CharField(**name_default)
    # image may be image file or QR
    signature_image = models.BinaryField()
    signature_timestamp = models.DateTimeField()
    signature_2fa = models.TextField()


class RecordedStatus(NamedCode, SoftDeleteMixin):
    recorded_datetime = models.DateTimeField()
    approval = models.ForeignKey('Approval', on_delete=models.CASCADE, related_name='status')

    def save(self, *args, **kwargs):
        # ensure only codes from the status enum are saved
        pass

    class Meta:
        ordering = ('-pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('util:recorded_status:detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('util:recorded_status:update', args=(self.pk,))


class BaseEvent(BaseModel):
    event_comment = models.TextField(**text_default)
    event_start = models.DateTimeField(**datetime_default)
    event_end = models.DateTimeField(null=True)
    event_type = models.CharField(null=True, **code_default)

    class Meta:
        abstract = True


class Approval(SoftDeleteMixin):
    approved_by = models.ForeignKey(BaseUser, on_delete=models.DO_NOTHING,
                                    related_name="%(app_label)s_%("
                                                 "class)s_approvals_by")
    approved_on = models.DateTimeField(auto_now_add=True)
    approved_till = models.DateTimeField(null=True)
    # @todo: investigate. A foreign key will serve. Drop GenericRelation
    approved_through = models.ForeignKey(
        BaseUser,
        on_delete=models.DO_NOTHING,
        related_name="%(app_label)s_%(class)s_approvals_through")
    content_type = models.ForeignKey(ContentType, on_delete=models.DO_NOTHING)
    object_id = models.CharField(**name_default)
    content_object = GenericForeignKey('content_type', 'object_id')


class Notification(SoftDeleteMixin):
    recipients = models.ManyToManyField(BaseUser)
    header = models.CharField(**name_default)
    notification_code = models.CharField(**code_default)
    message = models.TextField(**text_default)
    notification_reason = models.TextField(**text_default)

    def publish(self, **kwargs):
        self.broadcasts = ['header', 'recipients', 'message', 'notification_reason']
        return super(Notification, self).publish(**kwargs)


class NotificationResponse(PublishMixin, BaseModel):
    acknowledgement = models.TextField()
    recipient = models.ForeignKey('BaseUser', on_delete=models.DO_NOTHING)
    notification = models.ForeignKey('Notification', on_delete=models.DO_NOTHING)
    response_datetime = models.DateTimeField(auto_now=True)

    def publish(self, **kwargs):
        self.broadcasts = ['notification', 'response_date', 'acknowledgement']
        return super(NotificationResponse, self).publish(**kwargs)


class Institution(BaseModel):
    institution_name = models.CharField(**name_default)
    addresses = models.ManyToManyField('Address', related_name='owner')
    students = models.ManyToManyField('BaseUser', related_name='institution')
    contact = models.TextField()
    is_academic = models.BooleanField(default=False)


class UserAvatar(BaseModel):
    media = models.ImageField(upload_to=file_upload)
    user = models.OneToOneField(BaseUser, on_delete=models.CASCADE, related_name='avatar')
    media_type = models.CharField(**name_default)
    file_hash = models.CharField(max_length=100, editable=False, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


class UserPreferences(BaseModel):
    meta = models.CharField(**name_default)
    value = models.TextField(**text_default)
    user = models.ForeignKey('BaseUser', on_delete=models.CASCADE)


class AppBlacklist(BaseModel):
    user = models.ForeignKey('BaseUser', on_delete=models.CASCADE)
    code = models.CharField(max_length=10)
    created_at = models.DateTimeField(editable=False)
    service = models.CharField(null=True, blank=True, max_length=100)

    objects = models.Manager()

    @property
    def black_list(self):
        return AppBlacklist.objects.all()


class OtpReference(models.Model):
    user = models.ForeignKey('BaseUser', on_delete=models.CASCADE, related_name="otp_codes")
    key = models.BinaryField()
    reference = models.BinaryField()
    code_hash = models.CharField(max_length=100)
    created_at = models.DateTimeField(editable=False)
    expire_at = models.DateTimeField(editable=False)
    throttle = models.PositiveSmallIntegerField(editable=False, default=0)
    wait = models.PositiveIntegerField(editable=False, default=0)
    objects = models.Manager()


class Complaints(SoftDeleteMixin):
    REPORT_CHOICES = (
        ('FRD', 'Fraud'),
        ('UNC', 'Unclear'),
        ('UNV', 'Unverified'),
        ('FAK', 'Fake'),
        ('ABU', 'Abusive terms used'),
        ('IRR', 'Irrelevant')
    )
    is_blacklisted = models.BooleanField(default=False, editable=False)
    reporter = models.CharField(null=True, **name_default)
    comments = models.CharField(max_length=50, null=True)
    rating = models.CharField(choices=REPORT_CHOICES, **code_default)
    referer = models.CharField(editable=False, **name_default)
    remote_addr = models.CharField(editable=False, **name_default)
