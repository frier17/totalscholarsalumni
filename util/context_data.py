import re as regex
from collections import namedtuple
from datetime import datetime
from enum import IntFlag
from typing import Any

import pytz
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Sum
from django.utils import timezone

from achievement.models import AcademicGraduation, Achievements, EducationHistory, Qualification, WorkExperience
from career.models import JobApplication, JobPosting
from mentorship.models import MentoringProgramme, MentoringSession
from profile_management.models import Community, ProfessionalMembership, ScholarshipProfile, Testimonial, UserCV, \
    UserSkillSet
from social_network.models import SocialNetwork, UserLikes, UserComments
from tsa import settings
from util.models import UserAvatar, Notification, ServiceRequest, \
    ServiceResponse, NotificationResponse, BaseUser
from volunteer.models import VolunteerCourse, VolunteerCourseApplication


class DateAttribute(IntFlag):
    DATE = 0
    YEAR = 2
    MONTH = 3
    DAY = 5
    WEEK = 7
    WEEK_DAY = 11
    HOUR = 13
    MINUTE = 17
    SECONDS = 19


def _check_date(value: str, flag: DateAttribute) -> Any:
    months = (
        'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november',
        'december', 'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'
    )
    date_format = '%Y-%m-%d'
    date_format_tz = '%Y-%m-%d %Z'
    month_format = '%Y-%m'
    _date = None
    if value:
        if regex.fullmatch(r'\d{2,4}', value) and flag == DateAttribute.YEAR:
            _date = datetime(year=int(value), month=1, day=1, tzinfo=pytz.utc)
            if timezone.is_naive(_date):
                _date = timezone.make_aware(_date, timezone=pytz.utc)
        elif regex.fullmatch(r'\d{4}[-/]\d{1,2}[-/]\d{1,2}', value) and flag in \
                (DateAttribute.YEAR, DateAttribute.DATE):
            value = value.replace('/', '-')
            _date = datetime.strptime(value, date_format)
            if timezone.is_naive(_date):
                _date = timezone.make_aware(_date, timezone=pytz.utc)
        elif regex.match(r'\d{4}[-/]\d{1,2}[-/]\d{1,2}\s{0,}(\w+)', value) and flag in \
                (DateAttribute.YEAR, DateAttribute.DATE):
            value = value.replace('/', '-')
            _date = datetime.strptime(value, date_format_tz)
            if timezone.is_naive(_date):
                _date = timezone.make_aware(_date, timezone=pytz.utc)
        if regex.fullmatch(r'\d{1,2}', value) and flag == DateAttribute.MONTH:
            _date = datetime(year=datetime.now().year, month=int(value), day=1)
            if timezone.is_naive(_date):
                _date = timezone.make_aware(_date, timezone=pytz.utc)
        elif regex.fullmatch(r'\d{4}-\d{2}', value) and DateAttribute.MONTH:
            _date = datetime.strptime(value, month_format)
            if timezone.is_naive(_date):
                _date = timezone.make_aware(_date, timezone=pytz.utc)
        elif regex.fullmatch(r'\w+', value, regex.IGNORECASE) and flag == DateAttribute.MONTH and value in months:
            _date = datetime(year=datetime.now().year, month=months.index(value.lower()), day=1)
            if timezone.is_naive(_date):
                _date = timezone.make_aware(_date, timezone=pytz.utc)
        elif regex.match(r'\w+', value) and DateAttribute.DATE:
            _date = datetime.strptime(value, date_format)
            if timezone.is_naive(_date):
                _date = timezone.make_aware(_date, timezone=pytz.utc)
    return _date


def _prepare_time(**kwargs):
    time_param = namedtuple('TimeParameter',
                            'year, month, start_date, end_date, created_at, last_modified_at, period, before_period, '
                            'after_period, is_duration, '
                            'use_start_date, use_end_date, use_created_at, use_last_modified')
    year = _check_date(kwargs.get('year'), DateAttribute.YEAR)
    month = _check_date(kwargs.get('month'), DateAttribute.MONTH)
    start_date = _check_date(kwargs.get('start'), DateAttribute.DATE) if kwargs.get('start') else None
    end_date = _check_date(kwargs.get('end'), DateAttribute.DATE) if kwargs.get('end') else None
    created_at = _check_date(kwargs.get('created'), DateAttribute.DATE) if kwargs.get('end') else None
    last_modified_at = _check_date(kwargs.get('last_modified'), DateAttribute.DATE) if kwargs.get('end') else None
    period = _check_date(kwargs.get('year'), DateAttribute.YEAR)

    before_period = True if kwargs.get('reference') == '-1' else False
    after_period = True if kwargs.get('reference') == '1' else False
    is_duration = True if kwargs.get('reference') == '0' else False

    use_start_date = True if kwargs.get('attr_search') == '2' else False
    use_end_date = True if kwargs.get('attr_search') == '3' else False
    use_created_at = True if kwargs.get('attr_search') == '5' else False
    use_last_modified = True if kwargs.get('attr_search') == '7' else False

    return time_param(year=year, month=month, start_date=start_date, end_date=end_date, period=period,
                      before_period=before_period, after_period=after_period, is_duration=is_duration,
                      use_start_date=use_start_date, use_end_date=use_end_date, use_created_at=use_created_at,
                      use_last_modified=use_last_modified, created_at=created_at, last_modified_at=last_modified_at)


def _process_date_info(queryset: Any, attribute: str, context: str, year: Any = None, month: Any = None,
                       start_date: Any = None, end_date: Any = None, period: Any = None, is_duration: bool = None,
                       before_period: bool = None, after_period: bool = None) -> dict:
    result = {
        'year': year, 'month': month, 'start_date': start_date, 'end_date': end_date, 'period': period,
        'is_duration': is_duration, 'before_period': before_period, 'after_period': after_period
    }
    if not (year or month or start_date or end_date or period):
        result[context] = queryset.filter(models.Q(is_public=True) | models.Q(is_shareable=True))

    if isinstance(year, datetime) and month is None:
        result[context] = queryset.filter(**{f'{attribute}__year': year.year})
    elif isinstance(month, datetime):
        result[context] = queryset.filter(**{f'{attribute}__year': month.year, f'{attribute}__month': month.month})

    if before_period and isinstance(period, datetime):
        result[context] = queryset.filter(**{f'{attribute}__lte': period})
    elif after_period and isinstance(period, datetime):
        result[context] = queryset.filter(**{f'{attribute}__gte': period})

    if isinstance(start_date, datetime) and isinstance(end_date, datetime) and is_duration:
        result[context] = queryset.filter(**{f'{attribute}__range': (start_date, end_date)})
    elif isinstance(start_date, datetime) and after_period:
        result[context] = queryset.filter(**{f'{attribute}__gte': start_date})
    elif isinstance(start_date, datetime) and before_period:
        result[context] = queryset.filter(**{f'{attribute}__lte': start_date})
    elif isinstance(end_date, datetime) and after_period:
        result[context] = queryset.filter(**{f'{attribute}__gte': end_date})
    elif isinstance(end_date, datetime) and before_period:
        result[context] = queryset.filter(**{f'{attribute}__lte': end_date})
    return result


def _make_time_context(data: str, attribute: dict, use_start_date: bool = False, use_end_date: bool = False,
                       use_created_at: bool = False, use_last_modified: bool = False, **kwargs):
    queryset = kwargs.get('queryset')
    year = kwargs.get('year')
    month = kwargs.get('month')
    start_date = kwargs.get('start_date')
    end_date = kwargs.get('end_date')
    period = kwargs.get('period')
    before_period = kwargs.get('before_period')
    after_period = kwargs.get('after_period')
    is_duration = kwargs.get('is_duration')

    if use_start_date:
        start_context = _process_date_info(
            queryset=queryset, attribute=attribute.get('start'), context=data, year=year, month=month,
            start_date=start_date, end_date=end_date, period=period, before_period=before_period,
            after_period=after_period, is_duration=is_duration)[data]
        context = {'objects': start_context}

    elif use_end_date:
        end_context = _process_date_info(
            queryset=queryset, attribute=attribute.get('end'), context=data, year=year, month=month,
            start_date=start_date, end_date=end_date, period=period, before_period=before_period,
            after_period=after_period, is_duration=is_duration)[data]
        context = {'objects': end_context}

    elif use_created_at:
        end_context = _process_date_info(
            queryset=queryset, attribute=attribute.get('created'), context=data, year=year, month=month,
            start_date=start_date, end_date=end_date, period=period, is_duration=is_duration,
            before_period=before_period, after_period=after_period)[data]
        context = {'objects': end_context}
    elif use_last_modified:
        end_context = _process_date_info(
            queryset=queryset, attribute=attribute.get('last'), context=data, year=year, month=month,
            start_date=start_date, end_date=end_date, period=period, is_duration=is_duration,
            before_period=before_period, after_period=after_period)[data]
        context = {'objects': end_context}
    elif start_date and end_date:
        start_context = _process_date_info(
            queryset=queryset, attribute=attribute.get('start'), context=data, year=year, month=month,
            start_date=start_date, end_date=end_date, period=period, before_period=before_period,
            after_period=after_period, is_duration=is_duration)[data]
        end_context = _process_date_info(
            queryset=queryset, attribute=attribute.get('end'), context=data, year=year, month=month,
            start_date=start_date, end_date=end_date, period=period, before_period=before_period,
            after_period=after_period, is_duration=is_duration)[data]
        context = {'objects': list(start_context) + list(end_context)}
    else:
        '''
        start_context = _process_date_info(
            queryset=queryset, attribute=attribute.get('start'), context=data, year=year, month=month,
            start_date=start_date, end_date=end_date, period=period, before_period=before_period,
            after_period=after_period, is_duration=is_duration)[data]
        '''
        context = {'objects': queryset}
    return context


def _get_user_data(entity: Any, queryset: Any = None, **kwargs) -> Any:
    select_related = kwargs.get('select_related')
    fields = kwargs.get('fields')
    filters = kwargs.get('filters')

    if not queryset:
        queryset = entity.objects.all()
    if filters and isinstance(filters, dict):
        queryset = queryset.filter(**filters)
    if fields and isinstance(fields, (list, tuple)):
        queryset = queryset.only(*fields)

    if select_related and isinstance(select_related, (list, tuple)):
        queryset = queryset.select_related(*select_related)

    return queryset


def get_academic_graduation(user: Any, **kwargs):
    queryset = kwargs.get('queryset')
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    context = dict()

    queryset = _get_user_data(entity=AcademicGraduation, select_related=('graduand',), fields=fields, filters=filters,
                              queryset=queryset)

    if member:
        queryset = queryset.filter((models.Q(is_public=True) | models.Q(is_shareable=True)), graduand=member)
    else:
        queryset = queryset.filter(graduand=user)
    context.update({'objects': queryset})
    return context


def get_achievements(user: Any, **kwargs):
    queryset = kwargs.get('queryset')
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    context = dict()

    queryset = _get_user_data(entity=Achievements, select_related=('achiever',), fields=fields, filters=filters,
                              queryset=queryset)
    if member:
        queryset = queryset.filter(achiever=member)
    else:
        queryset = queryset.filter(achiever=user)
    context.update({'objects': queryset})
    return context


def get_education_history(user: Any, **kwargs):
    queryset = kwargs.get('queryset')
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    context = dict()

    queryset = _get_user_data(entity=EducationHistory, select_related=('awardee'), fields=fields,
                              filters=filters,
                              queryset=queryset)

    if member:
        queryset = queryset.filter(awardee=member)
    else:
        queryset = queryset.filter(awardee=user)
    context.update({'objects': queryset})
    return context


def get_qualification(user: Any, **kwargs):
    queryset = kwargs.get('queryset')
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    context = dict()

    queryset = _get_user_data(entity=Qualification, select_related=('awarding_institution', 'awardee'), fields=fields,
                              filters=filters,
                              queryset=queryset)

    if member:
        queryset = queryset.filter(awardee=member)
    else:
        queryset = queryset.filter(awardee=user)
    context.update({'objects': queryset})
    return context


def get_work_experience(user: Any, **kwargs):
    queryset = kwargs.get('queryset')
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    context = dict()

    queryset = _get_user_data(entity=WorkExperience, select_related=('employee',), fields=fields, filters=filters,
                              queryset=queryset)

    if member:
        queryset = queryset.filter(employee=member)
    else:
        queryset = queryset.filter(employee=user)
    context.update({'objects': queryset})
    return context


def get_user_avatar(user: Any, **kwargs) -> dict:
    """
    Retrieve a user's photo or avatar
    :param user: currently logged in user
    :param kwargs: extra parameter to specify attributes of avatar in context
    :return: dict: dictionary of context data to be rendered by view
    """
    context = dict()
    if user.is_authenticated:
        user_avatar = UserAvatar.objects.filter(user=user)
        if user_avatar.exists():
            avatar = user_avatar.get(user=user)
            context['avatar'] = avatar
            height = avatar.media.height
            width = avatar.media.width
            new_width = 0
            new_height = 0
            if kwargs.get('height'):
                new_height = kwargs.get('height')
            elif kwargs.get('width'):
                new_width = kwargs.get('width')
            else:
                new_height = settings.DEFAULT_IMAGE_HEIGHT
                new_width = settings.DEFAULT_IMAGE_WIDTH
            ratio = (new_height / height) or (new_width / width)
            avatar_height = int(ratio * avatar.media.height)
            avatar_width = int(ratio * avatar.media.width)
            context['avatar_height'] = avatar_height
            context['avatar_width'] = avatar_width
        else:
            context['avatar'] = None

    return context


def get_followers(user: Any = None, **kwargs) -> dict:
    """
    Retrieve the list of members following the currently logged in user or a specified user
    identified by slug
    :param user: The currently logged in user
    :param kwargs: dictionary of member slug and other parameter for filtering
    :return: dict: dictionary as context data for the view
    """
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    queryset = kwargs.get('queryset')
    context = dict()

    if not queryset:
        queryset = SocialNetwork.objects.all().select_related('leader')

    if member:
        queryset = queryset.filter(
            (models.Q(leader__is_shareable=True) | models.Q(leader__is_public=True)),
            (models.Q(follower=user) | models.Q(leader=user) &
             (models.Q(leader=member)))
        ).select_related('leader', 'follower')
        if filters:
            queryset = queryset.filter(**filters)
        if fields:
            queryset = queryset.only(*fields)
    else:
        queryset = queryset.filter(leader=user)
        queryset.order_by('-created_at')
        if filters:
            queryset = queryset.filter(**filters)
        if fields:
            queryset = queryset.only(*fields)
    context.update({'objects': queryset})
    return context


def get_leaders(user: Any = None, **kwargs) -> dict:
    """
    Retrieve a list of members being followed by the currently logged in user or specified user
    :param user: Currently logged in user
    :param kwargs: Dictionary of parameters that can be used for filtering
    :return: dict: context data as dictionary for the view
    """
    context = dict()
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    queryset = kwargs.get('queryset')

    if not queryset:
        queryset = SocialNetwork.objects.all().select_related('follower', 'leader')

    if member:
        queryset = queryset.filter(
            (
                    models.Q(follower__is_shareable=True) |
                    models.Q(follower__is_public=True)),
            (
                    models.Q(follower=member) &
                    (models.Q(follower=user) | models.Q(leader=user))
            )
        ).select_related('leader', 'follower')
        if filters:
            queryset = queryset.filter(**filters)
        if fields:
            queryset = queryset.only(*fields)
    else:
        queryset = queryset.filter(follower=user)
        queryset.order_by('-created_at')
        if filters:
            queryset = queryset.filter(**filters)
        if fields:
            queryset = queryset.only(*fields)
    context.update({'objects': queryset})
    return context


def get_notifications(user: Any, **kwargs):
    context = dict()
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    exclude = kwargs.get('exclude')
    queryset = None
    if user and fields and filters:
        queryset = Notification.objects.filter(recipients=user, **filters).only(*fields)
    elif user and filters:
        queryset = Notification.objects.filter(recipients=user, **filters)
        if exclude:
            queryset = queryset.exclude(**exclude)
    elif user and fields:
        queryset = Notification.objects.filter(recipients=user).only(*fields)
    elif user and exclude:
        queryset = Notification.objects.exclude(**exclude)
        queryset = queryset.filter(recipients=user)
    elif user and not (fields or filters):
        queryset = Notification.objects.filter(recipients=user)
    if queryset:
        queryset.order_by('-created_at')
        start_date = queryset.first().created_at
        end_date = queryset.last().created_at
        count = queryset.count()
        context.update({
            'notifications': queryset,
            'notice_start_date': start_date,
            'notice_end_date': end_date,
            'notification_count': count})
    return context


# @todo: deprecated. User get_notification_response
def get_notification_count(user: Any, **kwargs):
    if user:
        return Notification.objects.filter(recipients=user).count()


# @todo: deprecated. User get_service_responses
def get_service_requests(user, **kwargs):
    context = dict()
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    if user.is_authenticated and filters and fields:
        queryset = ServiceRequest.objects.filter(requester=user, **filters).only(*fields)
        context.update({'service_requests': queryset})
    elif user.is_authenticated and not (filters and fields):
        queryset = ServiceRequest.objects.filter(requester=user, **filters).only(*fields)
        context.update({'service_requests': queryset})
    return context


def get_service_responses(user: Any = None, **kwargs) -> dict:
    """
    Retrieve a list of responses for service requests sent to currently logged in user
    :param user: The currently logged in user
    :param kwargs: Collection of parameters for filtering queryset
    :return: dictionary of context data for rendering view
    """
    context = dict()
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    if user.is_authenticated:
        queryset = ServiceResponse.objects.filter(request__requester=user).select_related('request')
        if filters:
            responses = queryset.filter(**filters).select_related('request')
            context.update({'service_responses': responses})
        if fields:
            responses = queryset.only(*fields)
            context.update({'service_responses': responses})
    return context


def get_notification_responses(user: Any = None, **kwargs) -> dict:
    """
    Retrieve a list of responses for notices sent to currently logged in user
    :param user: The currently logged in user
    :param kwargs: Collection of parameters for filtering queryset
    :return: dictionary of context data for rendering view
    """
    context = dict()
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    if user:
        queryset = NotificationResponse.objects.filter(recipient=user).select_related('notification')
        if filters:
            notices = queryset.filter(**filters).order_by('created_at')
            context.update({'notification_responses': notices})
        if fields:
            notices = queryset.only(*fields).order_by('created_at')
            context.update({'notification_responses': notices})
    return context


def get_trainings(user: Any = None, **kwargs) -> dict:
    """
    Retrieve trainings for the logged in user or member identified by slug
    :param context: context data of the view that will be updated
    :param user: currently logged in user
    :param kwargs: collection of filtering parameters and other data for preparing list
    :return: dictionary of updated context data for rendering view
    """
    queryset = kwargs.get('queryset')
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    context = dict()

    queryset = _get_user_data(entity=MentoringSession, select_related=('pupil', 'mentor'), fields=fields,
                              filters=filters,
                              queryset=queryset)
    if member:
        queryset = queryset.filter(models.Q(pupil=member) | models.Q(mentor=member))
    else:
        queryset = queryset.filter(models.Q(pupil=user) | models.Q(mentor=user))
    context.update({'objects': queryset})
    return context


def get_job_postings(queryset: Any = None, **kwargs) -> dict:
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')

    year, month, start_date, end_date, created_at, last_modified_at, period, before_period, after_period, \
    is_duration, use_start_date, use_end_date, use_created_at, use_last_modified = _prepare_time(**kwargs)

    if not queryset:
        queryset = JobPosting.objects.filter(
            models.Q(is_public=True) | models.Q(is_shareable=True) | models.Q()).select_related('source')
    if filters:
        queryset = queryset.filter(**filters)
    if fields:
        queryset = queryset.only(*fields)
    context = _make_time_context(
        queryset=queryset, data='applications', use_start_date=use_start_date, use_created_at=use_created_at,
        use_end_date=use_end_date, use_last_modified=use_last_modified,
        attribute={'created': 'created_at', 'last': 'last_modified_at'}, year=year, month=month, start_date=start_date,
        end_date=end_date, period=period, before_period=before_period, after_period=after_period,
        is_duration=is_duration)

    return context


def get_job_applications(user: Any, queryset: Any = None, **kwargs) -> dict:
    """
    Retrieve a list of job applications by the current member or logged in user
    :param user: the currently logged in member
    :param kwargs: a dictionary of context data and parameters for filtering job application
    :return dict: a dictionary of context data for the view
    """
    context = dict()
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')

    year, month, start_date, end_date, created_at, last_modified_at, period, before_period, after_period, \
    is_duration, use_start_date, use_end_date, use_created_at, use_last_modified = _prepare_time(**kwargs)

    if not queryset:
        queryset = JobApplication.objects.filter(application_cv__user=user).select_related('application_cv')
    if filters:
        queryset = queryset.filter(**filters)
    if fields:
        queryset = queryset.only(*fields)
    context = _make_time_context(
        queryset=queryset, data='applications', use_start_date=use_start_date, use_created_at=use_created_at,
        use_end_date=use_end_date, use_last_modified=use_last_modified,
        attribute={'created': 'created_at', 'last': 'last_modified_at'}, year=year, month=month, start_date=start_date,
        end_date=end_date, period=period, before_period=before_period, after_period=after_period,
        is_duration=is_duration)
    return context


def get_community(user: Any, **kwargs) -> dict:
    queryset = kwargs.get('queryset')
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    context = dict()

    queryset = _get_user_data(entity=Community, fields=fields, filters=filters, queryset=queryset)

    if member:
        queryset = queryset.filter(indigenes=member)
    else:
        queryset = queryset.filter(indigenes=user)
    context.update({'objects': queryset})
    return context


def get_associations(user: Any, **kwargs) -> dict:
    queryset = kwargs.get('queryset')
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    context = dict()

    queryset = _get_user_data(entity=ProfessionalMembership, select_related=('member',), fields=fields, filters=filters,
                              queryset=queryset)

    if member:
        queryset = queryset.filter(member=member)
    else:
        queryset = queryset.filter(member=user)
    context.update({'objects': queryset})
    return context


def get_scholarships(user: Any, **kwargs) -> dict:
    queryset = kwargs.get('queryset')
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    context = dict()

    queryset = _get_user_data(entity=ScholarshipProfile, select_related=('owner',), fields=fields, filters=filters,
                              queryset=queryset)

    if member:
        queryset = queryset.filter(owner=member)
    else:
        queryset = queryset.filter(owner=user)
    context.update({'objects': queryset})
    return context


def get_testimonials(user: Any, **kwargs) -> dict:
    queryset = kwargs.get('queryset')
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    context = dict()

    queryset = _get_user_data(entity=Testimonial, select_related=('testifier',), fields=fields, filters=filters,
                              queryset=queryset)

    if member:
        queryset = queryset.filter(testifier=member)
    else:
        queryset = queryset.filter(testifier=user)
    context.update({'objects': queryset})
    return context


def get_user_cvs(user: Any, **kwargs) -> dict:
    queryset = kwargs.get('queryset')
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    context = dict()

    queryset = _get_user_data(entity=UserCV, select_related=('user',), fields=fields, filters=filters,
                              queryset=queryset)

    if member:
        queryset = queryset.filter(user=member)
    else:
        queryset = queryset.filter(user=user)
    context.update({'objects': queryset})
    return context


def get_user_skillsets(user: Any, **kwargs) -> dict:
    queryset = kwargs.get('queryset')
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    context = dict()

    queryset = _get_user_data(entity=UserSkillSet, select_related=('user', 'skill'), fields=fields, filters=filters,
                              queryset=queryset)

    if member:
        queryset = queryset.filter(user=member)
    else:
        queryset = queryset.filter(user=user)
    context.update({'objects': queryset})
    return context


def get_volunteer_courses(user: Any = None, queryset: Any = None, **kwargs):
    """
    :param user:
    :param kwargs:
    :return:
    """
    context = dict()
    fields = kwargs.get('fields')
    filters = kwargs.get('filters')
    member = kwargs.get('member')

    year, month, start_date, end_date, created_at, last_modified_at, period, before_period, after_period, \
    is_duration, use_start_date, use_end_date, use_created_at, use_last_modified = _prepare_time(**kwargs)

    if not queryset:
        queryset = VolunteerCourse.objects.all()
    if filters:
        queryset = queryset.filter(**filters)
    if fields:
        queryset = queryset.only(*fields)

    if user or member:
        queryset = _get_user_data(entity=VolunteerCourse, select_related=('course_owners', 'course_approvals'),
                                  fields=fields, filters=filters, queryset=queryset)
        if member:
            queryset = queryset.filter(course_owners=member)
        else:
            queryset = queryset.filter(course_owners=user)
        context.update({'objects': queryset})
    else:
        context = _make_time_context(data='courses',
                                     attribute={'start': 'course_start', 'end': 'course_end', 'created': 'created_at',
                                                'last': 'last_modified_at'}, queryset=queryset,
                                     use_start_date=use_start_date,
                                     use_end_date=use_end_date, use_created_at=use_created_at, year=year, month=month,
                                     start_date=start_date, end_date=end_date, period=period,
                                     before_period=before_period,
                                     after_period=after_period, is_duration=is_duration)
    return context


def get_user_volunteer_courses(user: Any, **kwargs):
    """
    :param user:
    :param kwargs:
    :return:
    """

    queryset = kwargs.get('queryset')
    member = kwargs.get('member')
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    context = dict()

    queryset = _get_user_data(entity=VolunteerCourseApplication,
                              select_related=('volunteer', 'volunteer_course'),
                              fields=fields, filters=filters, queryset=queryset)
    if member:
        queryset = queryset.filter(
            models.Q(volunteer=member) | models.Q(volunteer_course__course_owners=member)).distinct()
    else:
        queryset = queryset.filter(models.Q(volunteer=user) | models.Q(volunteer_course__course_owners=user)).distinct()

    context.update({'objects': queryset})
    return context


def get_comments(instance: Any, **kwargs):
    entity = ContentType.objects.get_for_model(instance.__class__)
    result = UserComments.objects.filter(
        models.Q(comments__isnull=False),
        content_type__pk=entity.id, object_id=instance.pk).values_list('comments', 'user__username', 'created_at')
    return result


def get_likes(instance: Any = None, user: Any = None, entity: Any = None, queryset: Any = None, **kwargs):
    # Get all the likes of a given instance
    user_rating = None
    result = dict()
    liked_entity = None

    if instance:
        liked_entity = ContentType.objects.get_for_model(instance.__class__)

    base_queryset = UserLikes.objects.filter(content_type__pk=liked_entity.id, object_id=instance.pk)
    likes = base_queryset.filter(like=True).count()
    count_ratings = base_queryset.filter(rating__gt=0).count()
    calc_ratings = base_queryset.filter(rating__gt=0).aggregate(Sum('rating'))
    sum_ratings = calc_ratings['rating__sum']
    count_comments = UserComments.objects.filter(content_type__pk=liked_entity.id, object_id=instance.pk,
                                                 comments__isnull=False).count()
    comments = get_comments(instance=instance)

    if isinstance(user, BaseUser) and instance is not None:
        user_rating = base_queryset.filter(rating__gt=0, user=user).last()
    elif entity:
        liked_entity = ContentType.objects.get_for_model(entity)
        likes = UserLikes.objects.filter(content_type__pk=liked_entity.id, like=True).count()
        count_ratings = UserLikes.objects.filter(content_type__pk=liked_entity.id, rating__gt=0).count()
        calc_ratings = UserLikes.objects.filter(content_type__pk=liked_entity.id, rating__gt=0).aggregate(
            sum_ratings=Sum('rating'))
        sum_ratings = calc_ratings['rating__sum']
        count_comments = UserComments.objects.filter(content_type__pk=liked_entity.id, comments__isnull=False).count()

    result.update(
        {'count_likes': likes, 'count_ratings': count_ratings, 'count_comments': count_comments,
         'sum_ratings': sum_ratings, 'user_rating': user_rating, 'comments': comments})
    return result


def get_members(queryset: Any = None, **kwargs):
    year = _check_date(kwargs.get('year'), DateAttribute.YEAR)
    month = _check_date(kwargs.get('month'), DateAttribute.MONTH)
    start_date = _check_date(kwargs.get('start'), DateAttribute.DATE) if kwargs.get('start') else None
    end_date = _check_date(kwargs.get('end'), DateAttribute.DATE) if kwargs.get('end') else None
    period = _check_date(kwargs.get('year'), DateAttribute.YEAR)

    before_period = True if kwargs.get('reference') == '-1' else False
    after_period = True if kwargs.get('reference') == '1' else False
    is_duration = True if kwargs.get('reference') == '0' else False
    filters = kwargs.get('filters')

    if not queryset:
        members = BaseUser.objects.all()
    else:
        members = queryset
    if filters and isinstance(filters, dict):
        members = members.filter(**filters)
    context = _process_date_info(queryset=members, attribute='date_joined', context='members', year=year, month=month,
                                 start_date=start_date, end_date=end_date, period=period,
                                 before_period=before_period, after_period=after_period, is_duration=is_duration)

    return context


def get_programme(queryset: Any = None, **kwargs):
    context = dict()
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')

    year, month, start_date, end_date, created_at, last_modified_at, period, before_period, after_period, \
    is_duration, use_start_date, use_end_date, use_created_at, use_last_modified = _prepare_time(**kwargs)

    if not queryset:
        queryset = MentoringProgramme.objects.filter(models.Q(is_public=True) | models.Q(is_shareable=True))
    if filters:
        queryset = queryset.filter(**filters)
    if fields:
        queryset = queryset.only(*fields)

    if use_start_date:
        start_context = _process_date_info(
            queryset=queryset, attribute='programme_start', context='programmes', year=year, month=month,
            start_date=start_date, end_date=end_date, period=period, before_period=before_period,
            after_period=after_period, is_duration=is_duration)['programmes']
        context = {'objects': start_context}
    elif use_end_date:
        end_context = _process_date_info(
            queryset=queryset, attribute='programme_end', context='programmes', year=year, month=month,
            start_date=start_date, end_date=end_date, period=period, before_period=before_period,
            after_period=after_period, is_duration=is_duration)['programmes']
        context = {'objects': end_context}
    elif use_created_at:
        end_context = _process_date_info(
            queryset=queryset, attribute='created_at', context='programmes', year=year, month=month,
            start_date=start_date, end_date=end_date, period=period, is_duration=is_duration,
            before_period=before_period, after_period=after_period)['programmes']
        context = {'objects': end_context}
    elif start_date and end_date:
        start_context = _process_date_info(
            queryset=queryset, attribute='programme_start', context='programmes', year=year, month=month,
            start_date=start_date, end_date=end_date, period=period, before_period=before_period,
            after_period=after_period, is_duration=is_duration)['programmes']
        end_context = _process_date_info(
            queryset=queryset, attribute='programme_end', context='programmes', year=year, month=month,
            start_date=start_date, end_date=end_date, period=period, before_period=before_period,
            after_period=after_period, is_duration=is_duration)['programmes']
        context = {'objects': list(start_context) + list(end_context)}
    else:
        start_context = _process_date_info(
            queryset=queryset, attribute='programme_start', context='programmes', year=year, month=month,
            start_date=start_date, end_date=end_date, period=period, before_period=before_period,
            after_period=after_period, is_duration=is_duration)['programmes']
        context = {'objects': start_context}

    return context


def get_careers(user: Any = None, queryset: Any = None, **kwargs):
    filters = kwargs.get('filters')
    fields = kwargs.get('fields')
    year = kwargs.get('year')
    month = kwargs.get('month')
    start_date = kwargs.get('start_date')
    end_date = kwargs.get('end_date')
    period = kwargs.get('period')
    before_period = kwargs.get('before_period')
    after_period = kwargs.get('after_period')
    context = dict()

    if not queryset:
        if user.is_authenticated:
            queryset = JobApplication.objects.filter(
                models.Q(is_public=True) | models.Q(is_shareable=True),
                applicant=user).select_related('job_posting', 'application_cv')

        else:
            queryset = JobPosting.objects.filter(models.Q(is_public=True) | models.Q(is_shareable=True))
    if filters:
        queryset = queryset.filter(**filters)
    if fields:
        queryset = queryset.only(*fields)

    context = _process_date_info(queryset=queryset, attribute='created_at', context='careers', year=year, month=month,
                                 start_date=start_date, end_date=end_date, period=period, before_period=before_period,
                                 after_period=after_period)

    return context
