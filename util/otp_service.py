import base64
import hashlib
import os
import random
import time
import uuid
from collections import namedtuple
from datetime import datetime, timedelta
from enum import Enum
from typing import Any

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.hashes import SHA256
from cryptography.hazmat.primitives.twofactor import InvalidToken
from cryptography.hazmat.primitives.twofactor.hotp import HOTP
from cryptography.hazmat.primitives.twofactor.totp import TOTP

from util import current_timezone
from util.models import AppBlacklist, BaseUser, OtpReference
from util.signals import otp_generated_signal, user_black_listed_signal, \
    otp_auth_failed_signal

MAX_COUNT = 2
MAX_WAIT_DURATION = 3600000  # 1hr in milliseconds
COUNT_FACTOR = 1
otp_entry = namedtuple('OtpRecord', ('key', 'reference', 'code_hash'))
AUTH_KEY_HASH = None  # Set to the hashed key of the user. Possibly from config
KEY_SIZE = 40
TIME_STEP = 30
DEFAULT_CODE_LENGTH = 6
ISSUER = 'TSA Digital Services'


def generate_token() -> str:
    return base64.urlsafe_b64encode(str(uuid.uuid4()).encode())


def _generate_totp(length: int = 6, times_step: int = 30):
    if length < 6 or length > 8:
        length = DEFAULT_CODE_LENGTH
    if times_step < TIME_STEP:
        times_step = TIME_STEP
    try:
        key = os.urandom(KEY_SIZE)
        totp = TOTP(key=key, length=length, algorithm=SHA256(), time_step=times_step,
                    backend=default_backend())
        time_value = time.time()
        totp_value = totp.generate(time_value)
        return totp_value, key, time_value
    except ValueError:
        raise ValueError('Invalid OTP parameter(s) provided')
    except TypeError:
        raise TypeError('Unsupported operation used for generating OTP')
    except Exception:
        raise Exception('Invalid operation')


def _generate_hotp(counter: int = 0):
    try:
        if isinstance(counter, int) and counter < 0:
            counter = 0
        key = os.urandom(KEY_SIZE)
        hotp = HOTP(key=key, length=6, algorithm=SHA256(), backend=default_backend())
        hotp_value = hotp.generate(counter)
        return hotp_value, key
    except ValueError:
        raise ValueError('Invalid user key provided for OTP')
    except TypeError:
        raise TypeError('Unsupported operation used for generating OTP')
    except Exception:
        raise Exception('Invalid operation')


def _verify_totp(code: bytes, key: str, timer: Any = None):
    try:
        timer = TIME_STEP if timer is None else timer
        totp = TOTP(key=key, length=len(code), algorithm=SHA256(), backend=default_backend(

        ), time_step=timer)
        return True if totp.verify(code, timer) is None else False
    except InvalidToken:
        raise


def _verify_hotp(code: bytes, key: str, counter: int = 0):
    try:
        hotp = HOTP(key=key, length=len(code), algorithm=SHA256(), backend=default_backend())
        return True if hotp.verify(code, counter) is None else False
    except InvalidToken:
        raise


def otp_unblock(auth, username):
    # Attempt using perm as auth or AUTH_KEY_HASH
    return NotImplemented


class OTP(Enum):
    HOTP = 139
    TOTP = 177


class OtpServer:
    __slots__ = '_counter', '_throttle', '_codes', '_service', '_account', '_session', \
                '_cache', '_wait', '_timer', '_user_hash', 'USER_IDENTIFIER', 'user'

    def __init__(self, user: BaseUser):
        self._counter = 0
        self._cache = dict()
        # validate email
        self._session = None
        self._throttle = 0
        self._codes = []
        self._wait = 0
        self._timer = None
        self.USER_IDENTIFIER = 'email'
        abbreviation = generate_token()[:4]
        self._user_hash = abbreviation.upper()

        if isinstance(user, BaseUser):
            self.user = user
        else:
            raise RuntimeError('Invalid user object provided')
        self._account = user.email

    @property
    def counter(self):
        return self._counter

    @property
    def throttle(self):
        return self._throttle

    @property
    def codes(self):
        return self._codes

    @property
    def service(self):
        return self._service

    @property
    def account(self):
        return self._account

    @property
    def session(self):
        return self._session

    @session.setter
    def session(self, value):
        self._session = value if isinstance(value, str) else None

    @property
    def wait(self):
        return self._wait

    def _make_code_hash(self, reference: Any) -> str:
        return hashlib.md5(str(reference).encode()).hexdigest()

    def _check_code_hash(self, reference: Any):
        return hashlib.md5(reference).hexdigest()

    def generate_otp(self, otp_type: Any = 'HOTP'):
        # @todo: Add user's reference to the otp record before save
        created = current_timezone(datetime.now())
        expiration = created + timedelta(hours=24)
        if otp_type == 'HOTP' or otp_type == OTP.HOTP:
            reference = random.randint(1000, 9999)
            hotp, key = _generate_hotp(counter=reference)
            if key and len(hotp) >= 6:
                # send otp_signal
                reference = str(reference).encode()
                otp_generated_signal.send(
                        sender=self.__class__,
                        reference=reference,
                        user=self.user,
                        otp=hotp,
                        key=key,
                        code_hash=self._make_code_hash(reference),
                        created_at=created,
                        expire_at=expiration)
                return hotp
        elif otp_type == 'TOTP' or otp_type == OTP.TOTP:
            totp, key, timer = _generate_totp(times_step=TIME_STEP)
            if key and len(totp) >= 6:
                reference = str(timer).encode()
                otp_generated_signal.send(
                        sender=self.__class__,
                        user=self.user,
                        otp=totp,
                        key=key,
                        reference=reference,
                        code_hash=self._make_code_hash(reference),
                        created_at=created,
                        expire_at=expiration)
                return totp

    def verify_otp(self, code: int, otp_type: Any = 'HOTP'):
        now = current_timezone(datetime.now())

        def _pre_check_lockout(server, entity: BaseUser, service: str = None):
            if server:
                if (server.throttle > MAX_COUNT) or (server.wait > MAX_WAIT_DURATION):
                    # send the fail otp authentication signal
                    otp_auth_failed_signal.send_robust(
                            sender=self.__class__,
                            user=entity,
                            code=code,
                            created_at=datetime.now(),
                            service=service)
                    user_black_listed_signal.send_robust(
                            sender=self.__class__,
                            user=entity,
                            code=code,
                            created_at=datetime.now(),
                            service=service)
                server.throttle += COUNT_FACTOR
                server.wait = server.throttle * 60000
                server.save()

        if not BaseUser.objects.filter(email=self.account).exists():
            raise RuntimeError("Selected user does not exist. Ensure user is registered or "
                               "valid user email is provided")
        if AppBlacklist.objects.filter(user=self.user).exists():
            raise RuntimeError('Unable to process request for blocked account')

        user_codes = list(OtpReference.objects.filter(user=self.user))
        searched = [
                x for x in user_codes
                if x.code_hash == self._make_code_hash(x.reference) and x.expire_at > now
        ]
        record = None
        if searched:
            record = searched.pop()

        try:
            if otp_type == 'HOTP' or otp_type == OTP.HOTP:
                if not record:
                    return False
                if _verify_hotp(str(code).encode(), record.key, int(record.reference)):
                    return record.code_hash
                else:
                    return False

            elif otp_type == 'TOTP' or otp_type == OTP.TOTP:
                if _verify_totp(str(code).encode(), record.key, float(record.reference)):
                    return record.code_hash
                else:
                    return False

        except InvalidToken:
            raise
        finally:
            _pre_check_lockout(record, entity=self.user, service='OTP validation')

    def otp_provisioning_url(self, otp_type: Any, otp: OtpReference,
                             issuer: str = None) -> Any:
        if not BaseUser.objects.filter(email=self.account):
            raise RuntimeError('Invalid user account provided. Ensure user is registered or '
                               'has valid email')
        if not issuer:
            issuer = ISSUER
        key = otp.key
        if otp_type == 'HOTP' or otp_type == OTP.HOTP:
            hotp = HOTP(key=key, length=DEFAULT_CODE_LENGTH, algorithm=SHA256(),
                        backend=default_backend())
            return hotp.get_provisioning_uri(account_name=self._user_hash,
                                             counter=otp.reference,
                                             issuer=issuer)
        else:
            totp = TOTP(key=key, length=DEFAULT_CODE_LENGTH, algorithm=SHA256(),
                        backend=default_backend(), time_step=TIME_STEP)
            return totp.get_provisioning_uri(account_name=self._user_hash, issuer=issuer)
