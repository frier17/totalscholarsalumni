
from django import forms
from util import models


class AddressForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(AddressForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.Address
        fields = '__all__'
        exclude = ['content_type', 'object_id']


class ApprovalForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(ApprovalForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.Approval
        fields = '__all__'      


class InstitutionForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(InstitutionForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.Institution
        fields = '__all__'      


class NotificationForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(NotificationForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.Notification
        fields = '__all__'      


class NotificationResponseForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(NotificationResponseForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.NotificationResponse
        fields = '__all__'      


class SignatureForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(SignatureForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.Signature
        fields = '__all__'      


class UserAvatarForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(UserAvatarForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.UserAvatar
        fields = '__all__'      


class UserPreferencesForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(UserPreferencesForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.UserPreferences
        fields = '__all__'      


class UserSignedDocumentForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(UserSignedDocumentForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.UserSignedDocument
        fields = '__all__'      


class UserUploadedDocumentForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(UserUploadedDocumentForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.UserUploadedDocument
        fields = '__all__'      

