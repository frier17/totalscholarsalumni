# Manage the various reporting queries of the application
# Define queries
from decimal import *
import datetime as dt
from typing import Any, Mapping

from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Q, F, Sum, Avg, Min, Max, Count

from achievement.models import AcademicGraduation, Achievements, EducationHistory, Qualification, WorkExperience
from career.models import JobApplication, JobPosting
from mentorship.models import MentoringProgramme, MentoringSession
from profile_management.models import Community, ProfessionalMembership, ScholarshipProfile, SkillSet, Testimonial, \
    UserCV, UserSkillSet
from social_network.models import SocialNetwork, UserLikes, UserComments
from util.models import BaseUser, Address, AppBlacklist, Institution, Notification, NotificationResponse, Complaints, \
    UserUploadedDocument


class MemberReportMixin(models.QuerySet):
    user_field = None

    def entity_for_member(self, member: BaseUser, user_field: str = None):
        if not user_field:
            user_field = self.user_field
        if hasattr(self.model, user_field):
            return self.filter(**{f'{user_field}': member})


class DateReportMixin(models.QuerySet):
    datetime_field = None

    def entry_on_date(self, selected: dt.datetime, datetime_field: str = None, for_year: bool = False,
                      for_month: bool = False, for_day: bool = False):
        if not datetime_field:
            datetime_field = self.datetime_field
        if isinstance(selected, dt.datetime):
            if not (for_year or for_month or for_day):
                return self.filter(**{f'{datetime_field}': selected})
            if for_year:
                return self.filter(**{f'{datetime_field}__year': selected.year})
            elif for_month:
                return self.filter(**{f'{datetime_field}__month': selected.month})
            elif for_day:
                return self.filter(**{f'{datetime_field}__day': selected.day})

    def entry_between_dates(self, start: Any, end: Any, datetime_field: str = None, for_year: bool = False,
                            for_month: bool = False, for_day: bool = False, for_range: bool = False):
        if not datetime_field:
            datetime_field = self.datetime_field
        if isinstance(start, (dt.date, dt.datetime)) and isinstance(end, (dt.date, dt.datetime)):
            if start and end and not (for_year or for_month or for_day):
                if for_range:
                    return self.filter(**{f'{datetime_field}__range': (start, end)})
                return self.filter(models.Q(**{f'{datetime_field}': start}) | models.Q(**{f'{datetime_field}': end}))

            elif start and end and for_year:
                return self.filter(models.Q(**{f'{datetime_field}__year': start.year}) |
                                   models.Q(**{f'{datetime_field}__year': end.year}))
            elif start and end and for_month:
                return self.filter(models.Q(**{f'{datetime_field}__month': start.month}) |
                                   models.Q(**{f'{datetime_field}__month': end.month}))
            elif start and end and for_day:
                return self.filter(models.Q(**{f'{datetime_field}__day': start.day}) |
                                   models.Q(**{f'{datetime_field}__day': end.day}))

    def entry_before_date(self, selected: Any, datetime_field: str = None, for_year: bool = False,
                          for_month: bool = False, for_day: bool = False):
        if not datetime_field:
            datetime_field = self.datetime_field
        if isinstance(selected, (dt.date, dt.datetime)):
            if selected and not (for_year or for_month or for_day):
                return self.filter(models.Q(**{f'{datetime_field}__lt': selected}))
            elif selected and for_year:
                return self.filter(**{f'{datetime_field}__year__lt': selected.year})
            elif selected and for_month:
                return self.filter(**{f'{datetime_field}__month__lt': selected.month})
            elif selected and for_day:
                return self.filter(**{f'{datetime_field}__day__lt': selected.day})

    def entry_on_or_before_date(self, selected: Any, datetime_field: str = None, for_year: bool = False,
                                for_month: bool = False, for_day: bool = False):
        if not datetime_field:
            datetime_field = self.datetime_field
        if isinstance(selected, (dt.date, dt.datetime)):
            if selected and not (for_year or for_month or for_day):
                return self.filter(models.Q(**{f'{datetime_field}__lte': selected}))
            elif selected and for_year:
                return self.filter(**{f'{datetime_field}__year__lte': selected.year})
            elif selected and for_month:
                return self.filter(**{f'{datetime_field}__month__lte': selected.month})
            elif selected and for_day:
                return self.filter(**{f'{datetime_field}__day__lte': selected.day})

    def entry_after_date(self, selected: Any, datetime_field: str = None, for_year: bool = False,
                         for_month: bool = False, for_day: bool = False):
        if not datetime_field:
            datetime_field = self.datetime_field
        if isinstance(selected, (dt.date, dt.datetime)):
            if selected and not (for_year or for_month or for_day):
                return self.filter(models.Q(**{f'{datetime_field}__gt': selected}))
            elif selected and for_year:
                return self.filter(**{f'{datetime_field}__year__gt': selected.year})
            elif selected and for_month:
                return self.filter(**{f'{datetime_field}__month__gt': selected.month})
            elif selected and for_day:
                return self.filter(**{f'{datetime_field}__day__gt': selected.day})

    def entry_on_or_after_date(self, selected: Any, datetime_field: str = None, for_year: bool = False,
                               for_month: bool = False, for_day: bool = False):
        if not datetime_field:
            datetime_field = self.datetime_field
        if isinstance(selected, (dt.date, dt.datetime)):
            if selected and not (for_year or for_month or for_day):
                return self.filter(models.Q(**{f'{datetime_field}__gte': selected}))
            elif selected and for_year:
                return self.filter(**{f'{datetime_field}__year__gte': selected.year})
            elif selected and for_month:
                return self.filter(**{f'{datetime_field}__month__gte': selected.month})
            elif selected and for_day:
                return self.filter(**{f'{datetime_field}__day__gte': selected.day})


class DateRangeMixin(models.QuerySet):
    range_start_field = None
    range_end_field = None

    def entry_between_dates(self, start: Any, end: Any, range_start_field: str, range_end_field: str,
                            for_year: bool = False, for_month: bool = False, for_day: bool = False, for_range: bool =
                            False):
        if not range_end_field:
            range_end_field = self.range_end_field
        if not range_start_field:
            range_start_field = self.range_start_field

        if isinstance(start, (dt.date, dt.datetime)) and isinstance(end, (dt.date, dt.datetime)):
            if start and end and not (for_year or for_month or for_day):
                if for_range:
                    return self.filter(
                        models.Q(**{f'{range_start_field}__range': (start, end)}) |
                        models.Q(**{f'{range_end_field}__range': end}))
                return self.filter(
                    models.Q(**{f'{range_start_field}': start}) & models.Q(**{f'{range_end_field}': end}))
            elif start and end and for_year:
                return self.filter(models.Q(**{f'{range_start_field}__year': start.year}) &
                                   models.Q(**{f'{range_end_field}__year': end.year}))
            elif start and end and for_month:
                return self.filter(models.Q(**{f'{range_start_field}__month': start.month}) &
                                   models.Q(**{f'{range_end_field}__month': end.month}))
            elif start and end and for_day:
                return self.filter(models.Q(**{f'{range_start_field}__day': start.day}) &
                                   models.Q(**{f'{range_end_field}__day': end.day}))


class CreatedAtReportMixin(DateReportMixin):
    created_at_field = 'created_at'

    def created_on_date(self, selected: dt.datetime, for_year: bool = False, for_month: bool = False,
                        for_day: bool = False):
        datetime_field = self.created_at_field
        return super().entry_on_date(selected, datetime_field, for_year, for_month, for_day)

    def created_between_dates(self, start: Any, end: Any, for_year: bool = False, for_month: bool = False,
                              for_day: bool = False):
        datetime_field = self.created_at_field
        return super().entry_between_dates(start, end, datetime_field, for_year, for_month, for_day)

    def created_before_date(self, selected: Any, for_year: bool = False, for_month: bool = False,
                            for_day: bool = False):
        datetime_field = self.created_at_field
        return super().entry_before_date(selected, datetime_field, for_year, for_month, for_day)

    def created_on_or_before_date(self, selected: Any, for_year: bool = False, for_month: bool = False,
                                  for_day: bool = False):
        datetime_field = self.created_at_field
        return super().entry_on_or_before_date(selected, datetime_field, for_year, for_month, for_day)

    def created_after_date(self, selected: Any, for_year: bool = False, for_month: bool = False, for_day: bool = False):
        datetime_field = self.created_at_field
        return super().entry_after_date(selected, datetime_field, for_year, for_month, for_day)

    def created_on_or_after_date(self, selected: Any, for_year: bool = False, for_month: bool = False,
                                 for_day: bool = False):
        datetime_field = self.created_at_field
        return super().entry_on_or_after_date(selected, datetime_field, for_year, for_month, for_day)


class ModifiedReportMixin(DateReportMixin):
    last_modified_field = 'last_modified_at'

    def modified_on_date(self, selected: dt.datetime, for_year: bool = False, for_month: bool = False,
                         for_day: bool = False):
        datetime_field = self.last_modified_field
        return super().entry_on_date(selected, datetime_field, for_year, for_month, for_day)

    def modified_between_dates(self, start: Any, end: Any, for_year: bool = False,
                               for_month: bool = False, for_day: bool = False):
        datetime_field = self.last_modified_field
        return super().entry_between_dates(start, end, datetime_field, for_year, for_month, for_day)

    def modified_before_date(self, selected: Any, for_year: bool = False,
                             for_month: bool = False, for_day: bool = False):
        datetime_field = self.last_modified_field
        return super().entry_before_date(selected, datetime_field, for_year, for_month, for_day)

    def modified_on_or_before_date(self, selected: Any, for_year: bool = False,
                                   for_month: bool = False, for_day: bool = False):
        datetime_field = self.last_modified_field
        return super().entry_on_or_before_date(selected, datetime_field, for_year, for_month, for_day)

    def modified_after_date(self, selected: Any, for_year: bool = False,
                            for_month: bool = False, for_day: bool = False):
        datetime_field = self.last_modified_field
        return super().entry_after_date(selected, datetime_field, for_year, for_month, for_day)

    def modified_on_or_after_date(self, selected: Any, for_year: bool = False,
                                  for_month: bool = False, for_day: bool = False):
        datetime_field = self.last_modified_field
        return super().entry_on_or_after_date(selected, datetime_field, for_year, for_month, for_day)


class DeletedReportMixin(DateReportMixin):
    deleted_at_field = 'deleted_at'

    def deleted_on_date(self, selected: dt.datetime, for_year: bool = False,
                        for_month: bool = False, for_day: bool = False):
        datetime_field = self.deleted_at_field
        return super().entry_on_date(selected, datetime_field, for_year, for_month, for_day)

    def deleted_between_dates(self, start: Any, end: Any, for_year: bool = False,
                              for_month: bool = False, for_day: bool = False):
        datetime_field = self.deleted_at_field
        return super().entry_between_dates(start, end, datetime_field, for_year, for_month, for_day)

    def deleted_before_date(self, selected: Any, for_year: bool = False,
                            for_month: bool = False, for_day: bool = False):
        datetime_field = self.deleted_at_field
        return super().entry_before_date(selected, datetime_field, for_year, for_month, for_day)

    def deleted_on_or_before_date(self, selected: Any, for_year: bool = False,
                                  for_month: bool = False, for_day: bool = False):
        datetime_field = self.deleted_at_field
        return super().entry_on_or_before_date(selected, datetime_field, for_year, for_month, for_day)

    def deleted_after_date(self, selected: Any, for_year: bool = False,
                           for_month: bool = False, for_day: bool = False):
        datetime_field = self.deleted_at_field
        return super().entry_after_date(selected, datetime_field, for_year, for_month, for_day)

    def deleted_on_or_after_date(self, selected: Any, for_year: bool = False,
                                 for_month: bool = False, for_day: bool = False):
        datetime_field = self.deleted_at_field
        return super().entry_on_or_after_date(selected, datetime_field, for_year, for_month, for_day)


class FilterReportMixin(models.QuerySet):
    filter_attributes = []

    def get_filtered_report(self, report_filter: Mapping = None, **kwargs):
        if self.filter_attributes:
            approved_filter = {str(x): y for x, y in report_filter.items() if x in self.filter_attributes}
            return self.filter(**approved_filter)


class AcademicGraduationQuerySet(models.QuerySet):
    def __init__(self, model=AcademicGraduation, query=None, using=None, hints=None):
        super(AcademicGraduationQuerySet, self).__init__(model, query, using, hints)

    def graduation_for_member(self, member: BaseUser):
        if isinstance(member, BaseUser):
            return self.filter(graduand=member)

    def graduation_within_dates(self, start: Any = None, end: Any = None):
        rsc = None
        if start and end:
            if isinstance(start, (dt.date, dt.datetime)) and isinstance(end, (dt.date, dt.datetime)):
                rsc = self.filter(models.Q(graduating_year=start.year),
                                  models.Q(graduating_year=end.year))

        elif start and not end:
            if isinstance(start, (dt.date, dt.datetime)):
                rsc = self.filter(graduating_year=start.year)
        elif end and not start:
            if isinstance(end, (dt.date, dt.datetime)):
                rsc = self.filter(graduating_year=end.year)
        return rsc

    def graduation_within_academic_years(self, start: Any = None, end: Any = None):
        rsc = None
        if start and end:
            if isinstance(start, (dt.date, dt.datetime)) and isinstance(end, (dt.date, dt.datetime)):
                rsc = self.filter(models.Q(graduating_academic_year__icontains=start.year),
                                  models.Q(graduating_academic_year__icontains=end.year))

        elif start and not end:
            if isinstance(start, (dt.date, dt.datetime)):
                rsc = self.filter(graduating_academic_year__icontains=start.year)
        elif end and not start:
            if isinstance(end, (dt.date, dt.datetime)):
                rsc = self.filter(graduating_academic_year__icontains=end.year)
        return rsc


class AchievementsQuerySet(MemberReportMixin):
    def __init__(self, model=Achievements, query=None, using=None, hints=None):
        super(AchievementsQuerySet, self).__init__(model, query, using, hints)

    def achievement_with_title(self, title: str):
        return self.filter(title__icontains=title)

    def entity_for_member(self, member: BaseUser, user_field: str = 'achiever'):
        super(AchievementsQuerySet, self).entity_for_member(member, user_field)


class EducationHistoryQuerySet(DateReportMixin, DateRangeMixin, CreatedAtReportMixin, DeletedReportMixin,
                               ModifiedReportMixin, MemberReportMixin):
    range_end_field = 'programme_end'
    range_start_field = 'programme_start'
    datetime_field = 'programme_start'

    def __init__(self, model=EducationHistory, query=None, using=None, hints=None):
        super(EducationHistoryQuerySet, self).__init__(model, query, using, hints)


class QualificationQuerySet(MemberReportMixin, FilterReportMixin):
    user_field = 'awardee'
    filter_attributes = ['qualification_name', 'qualification_code', 'awarding_institution']

    def __init__(self, model=Qualification, query=None, using=None, hints=None):
        super(QualificationQuerySet, self).__init__(model, query, using, hints)

    def get_qualification(self, qualification: str = None, code: str = None):
        return self.get_filtered_report(
            report_filter={'qualification_name__icontains': qualification, 'qualification_code__icontains': code})

    def qualification_from_institution(self, institution: str = None):
        return self.get_filtered_report(report_filter={'awarding_institution__icontains': institution})

    def qualification_by_institutions(self):
        return self.values('awarding_institution').annotate(total_awards=Count('qualification_name'))


class WorkExperienceQuerySet(DateRangeMixin, CreatedAtReportMixin, ModifiedReportMixin, DeletedReportMixin,
                             MemberReportMixin, FilterReportMixin):
    user_field = 'employee'
    datetime_field = 'employment_start'
    range_start_field = 'employment_start'
    range_end_field = 'employment_end'
    filter_attributes = ['job_title', 'employer', 'is_volunteer_role']

    def __init__(self, model=WorkExperience, query=None, using=None, hints=None):
        super(WorkExperienceQuerySet, self).__init__(model, query, using, hints)

    def active_employment_by_employer(self, employer: str):
        return self.values('employer').annotate(models.Q(total_employments=Count('employee')) &
                                                models.Q(employment_end__isnull=True) & models.Q(
            employer__icontains=str(employer)))

    def all_employment_by_employer(self, employer: str):
        return self.values('employer').annotate(models.Q(total_employments=Count('employee')) &
                                                models.Q(employer__icontains=str(employer)))

    def avg_salary(self, filter_field: str = None):
        if filter_field:
            rsc = self.annotate(average_salary=Avg(f'{filter_field}'))
            return rsc['average_salary']
        else:
            rsc = self.annotate(average_salary=Avg('base_salary_amount'))
            return rsc['average_salary']

    def max_salary(self, filter_field: str = None):
        if filter_field:
            rsc = self.annotate(max_salary=Max(f'{filter_field}'))
            return rsc['max_salary']
        else:
            rsc = self.annotate(max_salary=Max('base_salary_amount'))
            return rsc['max_salary']

    def min_salary(self, filter_field: str = None):
        if filter_field:
            self.annotate(min_salary=Min('base_salary_amount'))
        else:
            self.annotate(min_salary=Min('base_salary_amount'))

    def employment_range(self, start: Any, end: Any, filter_field: str = None):
        if filter_field:
            return self.filter(**{f'{filter_field}__range': (start, end)})
        else:
            return self.filter(base_salary_amount__range=(start, end))


class JobApplicationQuerySet(MemberReportMixin, CreatedAtReportMixin, ModifiedReportMixin,
                             DeletedReportMixin):
    user_field = 'applicant'
    datetime_field = 'application_datetime'

    def __init__(self, model=JobApplication, query=None, using=None, hints=None):
        super(JobApplicationQuerySet, self).__init__(model, query, using, hints)


class JobPostingQuerySet(FilterReportMixin, MemberReportMixin, CreatedAtReportMixin, ModifiedReportMixin,
                         DeletedReportMixin):
    filter_attributes = ['title']
    user_field = 'source'

    def __init__(self, model=JobPosting, query=None, using=None, hints=None):
        super(JobPostingQuerySet, self).__init__(model, query, using, hints)


class MentoringProgrammeQuerySet(DateRangeMixin, CreatedAtReportMixin, ModifiedReportMixin, DeletedReportMixin,
                                 MemberReportMixin, FilterReportMixin):
    user_field = 'source'
    datetime_field = 'programme_start'
    range_start_field = 'programme_start'
    range_end_field = 'programme_end'
    filter_attributes = ['programme_name']

    def __init__(self, model=MentoringProgramme, query=None, using=None, hints=None):
        super(MentoringProgrammeQuerySet, self).__init__(model, query, using, hints)


class MentoringSessionQuerySet(FilterReportMixin):
    filter_attributes = ['programme', 'mentor', 'pupil']

    def __init__(self, model=MentoringSession, query=None, using=None, hints=None):
        super(MentoringSessionQuerySet, self).__init__(model, query, using, hints)


class CommunityQuerySet(FilterReportMixin):
    filter_attributes = ['community_name', 'tribal_group', 'local_govt_area', 'state_of_origin', 'location_type',
                         'asset_type', 'community_code', 'community_mou']

    def __init__(self, model=Community, query=None, using=None, hints=None):
        super(CommunityQuerySet, self).__init__(model, query, using, hints)


class ProfessionalMembershipQuerySet(MemberReportMixin, FilterReportMixin, DateRangeMixin):
    user_field = 'member'
    filter_attributes = ['association', 'association_contact', 'member_role', 'member_position']
    range_start_field = 'membership_start'
    range_end_field = 'membership_end'

    def __init__(self, model=ProfessionalMembership, query=None, using=None, hints=None):
        super(ProfessionalMembershipQuerySet, self).__init__(model, query, using, hints)


class ScholarshipProfileQuerySet(MemberReportMixin, FilterReportMixin, DateRangeMixin, CreatedAtReportMixin,
                                 ModifiedReportMixin, DeletedReportMixin):
    filter_attributes = ['scholarship_academic_year', 'entry_level_on_award', 'community', 'scholarship_code',
                         'benefit_amount', 'scholarship_award_year', 'course_of_study']
    range_start_field = 'scholarship_start',
    range_end_field = 'scholarship_end',
    user_field = 'owner'

    def __init__(self, model=ScholarshipProfile, query=None, using=None, hints=None):
        super(ScholarshipProfileQuerySet, self).__init__(model, query, using, hints)

    def scholarship_by_gender(self):
        return self.values('owner__gender').annotate(gender_count=Count('pk'))

    def scholarship_by_community(self):
        return self.values('community').annotate(total_scholarships=Count('pk'))

    def scholarship_by_university(self):
        return self.values('owner__institution__institution_name').annotate(total_scholarships=Count('pk'))

    def scholarship_by_course(self):
        return self.values('course_of_study').annotate(total_scholarships=Count('pk'))


class TestimonialQuerySet(MemberReportMixin):
    user_field = 'testifier'

    def __init__(self, model=Testimonial, query=None, using=None, hints=None):
        super(TestimonialQuerySet, self).__init__(model, query, using, hints)


class UserCVQuerySet(MemberReportMixin, FilterReportMixin):
    user_field = 'user'
    filter_attributes = ['cv_name', 'career_objective', 'description', 'work_experiences', 'hobbies', 'skills']

    def __init__(self, model=UserCV, query=None, using=None, hints=None):
        super(UserCVQuerySet, self).__init__(model, query, using, hints)


class UserSkillSetQuerySet(MemberReportMixin, FilterReportMixin):
    user_field = 'user'
    filter_attributes = ['skill', 'proficiency_level']

    def __init__(self, model=UserSkillSet, query=None, using=None, hints=None):
        super(UserSkillSetQuerySet, self).__init__(model, query, using, hints)


class SocialNetworkQuerySet(FilterReportMixin, DateReportMixin):
    filter_attributes = ['follower', 'leader']
    datetime_field = 'social_start'

    def __init__(self, model=SocialNetwork, query=None, using=None, hints=None):
        super(SocialNetworkQuerySet, self).__init__(model, query, using, hints)


class UserCommentsQuerySet(MemberReportMixin, FilterReportMixin):
    user_field = 'user'
    filter_attributes = ['comments']

    def __init__(self, model=UserComments, query=None, using=None, hints=None):
        super(UserCommentsQuerySet, self).__init__(model, query, using, hints)


class UserLikesQuerySet(MemberReportMixin, FilterReportMixin):
    user_field = 'user'
    filter_attributes = ['rating', 'like']

    def __init__(self, model=UserLikes, query=None, using=None, hints=None):
        super(UserLikesQuerySet, self).__init__(model, query, using, hints)

    def max_rating(self, content: Any, group_by: str, aggregate_field: str):
        return self.filter(
            content_type__pk=content.id).values(f'{group_by}').annotate(max_rating=Max(f'{aggregate_field}'))

    def min_rating(self, content, group_by: str, aggregate_field: str):
        return self.filter(
            content_type__pk=content.id).values(f'{group_by}').annotate(min_rating=Min(f'{aggregate_field}'))

    def avg_rating(self, content, group_by: str, aggregate_field: str):
        return self.filter(
            content_type__pk=content.id).values(f'{group_by}').annotate(avg_rating=Avg(f'{aggregate_field}'))

    def max_likes(self, content: Any, group_by: str, aggregate_field: str):
        return self.filter(
            content_type__pk=content.id).values(f'{group_by}').annotate(max_rating=Max(f'{aggregate_field}'))

    def min_likes(self, content, group_by: str, aggregate_field: str):
        return self.filter(
            content_type__pk=content.id).values(f'{group_by}').annotate(min_rating=Min(f'{aggregate_field}'))

    def avg_likes(self, content, group_by: str, aggregate_field: str):
        return self.filter(
            content_type__pk=content.id).values(f'{group_by}').annotate(avg_rating=Avg(f'{aggregate_field}'))


class AddressQuerySet(FilterReportMixin):
    filter_attributes = []

    def __init__(self, model=Address, query=None, using=None, hints=None):
        super(AddressQuerySet, self).__init__(model, query, using, hints)

    def members_in_country(self, country: str = None):
        content = ContentType.objects.get_for_model(BaseUser)
        if country:
            return self.filter(content_type__pk=content.id, country=country).values('country').annotate(
                total_members=Count('pk'))
        else:
            return self.filter(content_type__pk=content.id).values('country').annotate(total_members=Count('pk'))

    def members_in_states(self, state: str = None):
        content = ContentType.objects.get_for_model(BaseUser)
        if state:
            return self.filter(content_type__pk=content.id, state=state).values('state').annotate(
                total_members=Count('pk'))
        else:
            return self.filter(content_type__pk=content.id).values('state').annotate(total_members=Count('pk'))

    def members_in_region(self, country: str, state: str):
        content = ContentType.objects.get_for_model(BaseUser)
        return self.filter(
            content_type__pk=content.id, country__icontains=country, state__icontains=state).values(
            'country', 'state').annotate(total_members=Count('pk'))

    def members_in_city(self, country: str, state: str, city: str = None):
        content = ContentType.objects.get_for_model(BaseUser)
        return self.filter(
            content_type__pk=content.id, country__icontains=country, state__icontains=state,
            city__icontains=city).values('country', 'state', 'city').annotate(total_members=Count('pk'))

    def members_in_lga(self, country: str, state: str, lga: str = None):
        content = ContentType.objects.get_for_model(BaseUser)
        return self.filter(
            content_type__pk=content.id, country__icontains=country, state__icontains=state,
            city__icontains=lga).values('country', 'state', 'lga').annotate(total_members=Count('pk'))


class AppBlacklistQuerySet(FilterReportMixin):
    filter_attributes = ['code', 'service']

    def __init__(self, model=AppBlacklist, query=None, using=None, hints=None):
        super(AppBlacklistQuerySet, self).__init__(model, query, using, hints)


class BaseUserQuerySet(FilterReportMixin, DateReportMixin):
    filter_attributes = ['gender', 'title', 'date_of_birth' 'is_staff']
    datetime_field = 'date_of_birth'

    def __init__(self, model=BaseUser, query=None, using=None, hints=None):
        super(BaseUserQuerySet, self).__init__(model, query, using, hints)


class InstitutionQuerySet(FilterReportMixin):
    filter_attributes = ['institution_name', 'contact', 'is_academic']

    def __init__(self, model=Institution, query=None, using=None, hints=None):
        super(InstitutionQuerySet, self).__init__(model, query, using, hints)


class NotificationQuerySet(FilterReportMixin, CreatedAtReportMixin, ModifiedReportMixin, DeletedReportMixin):
    filter_attributes = ['header', 'notification_code', 'notification_reason']

    def __init__(self, model=Notification, query=None, using=None, hints=None):
        super(NotificationQuerySet, self).__init__(model, query, using, hints)


class NotificationResponseQuerySet(MemberReportMixin, DateReportMixin):
    user_field = 'recipient'
    datetime_field = 'response_date'

    def __init__(self, model=NotificationResponse, query=None, using=None, hints=None):
        super(NotificationResponseQuerySet, self).__init__(model, query, using, hints)


class ComplaintsQuerySet(FilterReportMixin, CreatedAtReportMixin, ModifiedReportMixin, DeletedReportMixin):
    filter_attributes = ['is_blacklisted', 'reporter']

    def __init__(self, model=Complaints, query=None, using=None, hints=None):
        super(ComplaintsQuerySet, self).__init__(model, query, using, hints)


class UserUploadedDocumentQuerySet(MemberReportMixin, FilterReportMixin, CreatedAtReportMixin, ModifiedReportMixin,
                                   DeletedReportMixin):
    filter_attributes = ['file_hash', 'allow_download', 'label']
    user_field = 'owner'

    def __init__(self, model=UserUploadedDocument, query=None, using=None, hints=None):
        super(UserUploadedDocumentQuerySet, self).__init__(model, query, using, hints)
