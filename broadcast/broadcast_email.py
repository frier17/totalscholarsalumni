import os
import re as regex
import smtplib
import ssl

from datetime import datetime as dt
from email import encoders
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from typing import Sequence, List, Union, Any

import requests

import settings as mail_settings

email_regex = regex.compile(r'([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)')


def _smtp_send(subject: str, msg: str, from_email: str, recipient_list: Union[str, List[str]], auth_user: str,
               auth_password: str, html_message: str, bcc: List[str] = None, cc: List[str] = None,
               attachment: Union[str, List[str]] = None) -> dict:
    message = MIMEMultipart()
    part = MIMEBase('application', 'octet-stream')
    if not auth_user:
        # assume the SMTP username is same as sender's email
        auth_user = from_email
    message["From"] = from_email
    if isinstance(recipient_list, str):
        message["To"] = recipient_list
    elif isinstance(recipient_list, List):
        recipient_list = [x for x in recipient_list if email_regex.fullmatch(x)]
        receivers = ', '.join(recipient_list)
        message["To"] = receivers
    message["Subject"] = subject
    if isinstance(bcc, str):
        message["Bcc"] = bcc  # Recommended for mass emails
    elif isinstance(bcc, List):
        bcc = [x for x in bcc if email_regex.fullmatch(x)]
        receivers = ', '.join(bcc)
        message['Bcc'] = receivers
    if isinstance(cc, str):
        message['Cc'] = cc  # Recommended for mass emails
    elif isinstance(cc, List):
        cc = [x for x in cc if email_regex.fullmatch(x)]
        receivers = ', '.join(cc)
        message['Cc'] = receivers
    if message:
        # Add body to email
        message.attach(MIMEText(msg, "plain"))
    if html_message:
        message.attach(MIMEText(html_message, "html"))

    if isinstance(attachment, str):
        file_path = os.path.isfile(attachment)
        if file_path:
            with open(attachment, 'rb') as file_attachment:
                part.set_payload(file_attachment.read())
                encoders.encode_base64(part)
                part.add_header(
                    'Content-Disposition',
                    f'attachment; filename= {os.path.basename(attachment)}',
                )
                # Add attachment to message
                message.attach(part)

    elif isinstance(attachment, List):
        files = [x for x in attachment if os.path.isfile(x)]
        file_path = all(files)
        if file_path:
            for attach_file in files:
                with open(attach_file, 'rb') as file_attachment:
                    part = MIMEBase('application', 'octet-stream')
                    part.set_payload(file_attachment.read())
                    encoders.encode_base64(part)
                    part.add_header(
                        'Content-Disposition',
                        f'attachment; filename= {os.path.basename(attach_file)}',
                    )
                    # Add attachment to message
                    message.attach(part)

    text = message.as_string()

    # Log in to server using secure context and send email
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(mail_settings.EMAIL_SMTP_SERVER, mail_settings.EMAIL_SMTP_SENDING_PORT, context=context) as \
            server:
        server.login(auth_user, auth_password)
        return server.sendmail(from_email, recipient_list, text)


def simple_send(subject: str, message: str, recipients: Union[str, List[str]]):
    return _api_send(
        email_subject=subject, api_key=mail_settings.EMAIL_API_KEY, sender=mail_settings.API_FROM_EMAIL,
        text=message, receivers=recipients, fail_silently=True
    )


def _api_send(api_key: str, sender: str, email_subject: str, text: str,
              receivers: Union[str, List] = None, attachments: Sequence = None, html: str = None, fail_silently: bool =
              False):
    # evaluate recipients have valid emails
    valid_recipients = []
    if isinstance(receivers, List):
        valid_recipients = [email for email in receivers if email_regex.fullmatch(email)]
    elif isinstance(receivers, str):
        receivers = [receivers, ]
        valid_recipients = [email for email in receivers]
    try:
        if all(valid_recipients):
            print(mail_settings.EMAIL_API_URL)
            print(mail_settings.EMAIL_API_KEY)

            response = requests.post(
                mail_settings.EMAIL_API_URL,
                auth=('api', api_key),
                files=attachments,
                data={
                    'from': sender,
                    'to': valid_recipients,
                    'subject': email_subject,
                    'text': text,
                    'html': html
                }
            )
            if response.status_code == 200 or response.status_code < 400:
                return response.json()
            else:
                if fail_silently:
                    return response.json()
                raise ConnectionError(response.json())
    except ConnectionError as err:
        if fail_silently:
            return err.__traceback__
        raise


def app_send_email(subject: str, message: str, from_email: str, recipients: Union[str, List[str]],
                   auth_user: str, auth_password: str, html_message: str, admin, mail_service, api: bool = False,
                   smtp: bool = False,
                   fail_silently: bool = False, bcc: Union[str, List] = None, cc: Union[str, List] = None,
                   attachments: Union[str, List] = None) -> Any:
    if api and not smtp:
        email_response = _api_send(
            api_key=mail_settings.EMAIL_API_KEY,
            sender=from_email,
            receivers=recipients,
            email_subject=subject,
            text=message, fail_silently=fail_silently)
        return dict(sender=admin, asctime=dt.now(), from_email=from_email, recipient_email=recipients,
                    message=message, mail_service=mail_service) if email_response else False

    if smtp and not api:
        email_response = _smtp_send(
            subject=subject, msg=message, from_email=from_email, recipient_list=recipients, auth_user=auth_user,
            auth_password=auth_password, html_message=html_message, bcc=bcc, cc=cc, attachment=attachments)
        return dict(sender=admin, asctime=dt.now(), from_email=from_email, recipient_email=recipients,
                    message=message, mail_service=mail_service) if email_response else False
