import os
from configparser import ConfigParser
# Load settings from config or .env file
config = ConfigParser()
config_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config.ini')
config.read(config_path)
sections = config.sections()
cf = {str(e).upper(): config.get(s, e) for s in config.sections() for e in config.options(s)}

for name, value in cf.items():
    value = str(value).replace("'", '')
    os.environ[name] = value

# API email settings
EMAIL_API_URL = os.getenv('EMAIL_API_URL')
EMAIL_API_KEY = os.getenv('EMAIL_API_KEY')
EMAIL_SMTP_SERVER = os.getenv('EMAIL_SMTP_SERVER')
EMAIL_SMTP_SENDING_PORT = os.getenv('EMAIL_SMTP_SENDING_PORT')
API_FROM_EMAIL = os.getenv('API_FROM_EMAIL')

# Default settings
EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = False
EMAIL_USE_SSL = False

EMAIL_TEST_MESSAGE = 'Sample application testing from the Total Scholars Alumni community'

TWILIO_SID = os.getenv('TWILIO_SID')
TWILIO_AUTH_TOKEN = os.getenv('TWILIO_AUTH_TOKEN')
TWILIO_SENDER_MOBILE = os.getenv('TWILIO_SENDER_MOBILE')
TWILIO_MESSAGE_SERVICE = os.getenv('TWILIO_MESSAGE_SERVICE')
