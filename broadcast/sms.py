import re
from typing import List, Union

from twilio.base.exceptions import TwilioRestException
from twilio.rest import Client

import settings

account_sid = settings.TWILIO_SID
auth_token = settings.TWILIO_AUTH_TOKEN


def _check_mobile(mobile: str) -> bool:
    pattern = r'[+]{0,1}(\d{3})\D*(\d{3})\D*(\d{4})\D*(\d*)$'
    return re.compile(pattern).fullmatch(mobile) is not None


def _queue_messsage(from_: str, recipients: List, msg: Client, message: str) -> any:

    return [msg.messages.create(from_=from_, body=message, to=r) for r in recipients]


def send_sms(to: Union[List, str], message: str, message_service: str = None, use_service: bool = False) -> List:
    recipients = []
    if isinstance(to, str):
        if _check_mobile(to):
            recipients.append(to)
    elif isinstance(to, List):
        recipients = [t for t in to if _check_mobile(t)]
    else:
        raise ValueError
    client = Client(account_sid, auth_token)

    try:
        if message_service:
            if not re.match(r'\w{34}', message_service):
                raise ValueError
            msg_queue = _queue_messsage(from_=message_service, recipients=recipients, msg=client, message=message)

        else:
            if use_service:
                msg_queue = _queue_messsage(from_=settings.TWILIO_MESSAGE_SERVICE, recipients=recipients, msg=client,
                                            message=message)
            else:
                msg_queue = _queue_messsage(from_=settings.TWILIO_SENDER_MOBILE, recipients=recipients, msg=client,
                                            message=message)

        _properties = ['direction', 'from_', 'to', 'date_updated', 'price', 'error_message', 'uri', 'num_media',
                       'status', 'date_sent', 'date_created', 'error_code', 'price_unit', 'api_version', ]
        return [{x: getattr(message, x) for x in _properties} for message in msg_queue]

    except TwilioRestException:
        raise
