from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.utils.translation import gettext_lazy as _

from util import code_default, name_default, text_default, datetime_default, currency_default
from util.models import BaseModel, SoftDeleteMixin, PublishMixin, FileUploadMixin


class Profile(SoftDeleteMixin):
    owner = models.ForeignKey('util.BaseUser', on_delete=models.CASCADE)
    profile_start = models.DateTimeField(null=True, **datetime_default)
    profile_end = models.DateTimeField(null=True, **datetime_default)
    description = models.TextField(**text_default)

    class Meta:
        abstract = True


class ScholarshipProfile(Profile):
    scholarship_start = models.DateField()
    scholarship_end = models.DateField()
    scholarship_academic_year = models.CharField(**code_default)
    entry_level_on_award = models.CharField(**code_default)
    community = models.ForeignKey('Community', on_delete=models.DO_NOTHING, related_name='scholarship_profile')
    scholarship_code = models.CharField(**code_default)
    benefit_amount = models.DecimalField(**currency_default)
    benefit_currency = models.CharField(null=True, **code_default)
    scholarship_award_year = models.DateField()
    course_of_study = models.CharField(help_text=_('The course of Study or educational '
                                                   'programme for which this scholarship was '
                                                   'awarded'), null=True, blank=True,
                                       **name_default)
    year_of_graduation = models.CharField(null=True, **code_default)
    graduating_grade = models.CharField(help_text=_('The graduating grade (e.g CGPA) for the '
                                                    'named '
                                                    'programme for which the scholarship was '
                                                    'awarded'), null=True, **name_default)
    approvals = GenericRelation('util.Approval', related_name='scholarships')

    def save(self, *args, **kwargs):
        user_community = Community.objects.filter(indigenes=self.owner).first()
        if self.community == user_community:
            return super(ScholarshipProfile, self).save(*args, **kwargs)
        else:
            raise RuntimeError('Community of scholarship and awardee should be identical')


class Community(PublishMixin, BaseModel):
    community_name = models.CharField(**name_default)
    tribal_group = models.CharField(**name_default)
    local_govt_area = models.CharField(**name_default)
    state_of_origin = models.CharField(**name_default)
    location_type = models.CharField(**code_default)
    asset_type = models.CharField(editable=False, null=True, blank=True, **code_default)
    community_code = models.CharField(editable=False, null=True, blank=True, **code_default)
    community_mou = models.CharField(editable=False, null=True, blank=True, **name_default)
    indigenes = models.ManyToManyField('util.BaseUser', related_name='community')
    approvals = GenericRelation('util.Approval', related_name='communities')


class Testimonial(PublishMixin, BaseModel):
    testimony = models.TextField(**text_default)
    testifier = models.ForeignKey('util.BaseUser', on_delete=models.DO_NOTHING,
                                  related_name='testimonies')
    testified_on = models.DateTimeField(**datetime_default)
    approvals = GenericRelation('util.Approval', related_name='testimonials')

    def publish(self, **kwargs):
        self.excluded_broadcasts = ['approvals']
        return super(Testimonial, self).publish(**kwargs)


class SkillSet(BaseModel):
    name = models.CharField(**name_default)
    description = models.TextField(**text_default)


class UserSkillSet(PublishMixin, BaseModel):
    user = models.ForeignKey('util.BaseUser', on_delete=models.CASCADE,
                             related_name='skills')
    skill = models.ForeignKey('SkillSet', on_delete=models.DO_NOTHING,
                              related_name='users')
    proficiency_level = models.PositiveSmallIntegerField(
        null=True,
        help_text=_('Numeric value to rank a user. Default range is set from 0 - 9'),
        default=0)

    def publish(self, **kwargs):
        self.broadcast_order = ['skill', 'user']
        return super(UserSkillSet, self).publish(**kwargs)


class ProfessionalMembership(PublishMixin, BaseModel):
    membership_start = models.DateTimeField(null=True, blank=True, **datetime_default)
    membership_end = models.DateTimeField(null=True, blank=True, **datetime_default)
    association = models.CharField(**name_default)
    association_contact = models.TextField(blank=True, **text_default)
    member_role = models.CharField(null=True, blank=True, **name_default)
    member_position = models.CharField(**name_default)
    member = models.ForeignKey('util.BaseUser', on_delete=models.CASCADE)

    def publish(self, **kwargs):
        self.broadcasts = '__all__'
        self.broadcast_order = ['association', 'membership_start', 'membership_end',
                                'member_position', 'member_role']
        return super(ProfessionalMembership, self).publish(**kwargs)


class UserCV(FileUploadMixin, SoftDeleteMixin):
    cv_name = models.CharField(**name_default, null=True)
    career_objective = models.TextField(**text_default)
    description = models.TextField(**text_default)
    user = models.ForeignKey('util.BaseUser', on_delete=models.CASCADE,
                             related_name='user_cv', null=True)
    work_experiences = models.ManyToManyField('achievement.WorkExperience',
                                              related_name="user_cv")
    education_histories = models.ManyToManyField('achievement.EducationHistory',
                                                 related_name="user_cv")
    qualifications = models.ManyToManyField('achievement.Qualification',
                                            related_name="user_cv")
    publication = models.TextField(help_text=_('List publications (online or other media) '
                                               'you wish to share with prospective '
                                               'employers'), blank=True, **text_default)
    hobbies = models.TextField(help_text=_('List or select your hobbies you wish to '
                                           'share with prospective employers'), blank=True, **text_default)
    skills = models.ManyToManyField('UserSkillSet', related_name='user_cv')

    def publish(self, **kwargs):
        self.broadcasts = '__all__'
        self.excluded_broadcasts = ['work_experiences', 'education_histories',
                                    'qualifications', 'publications', 'hobbies']
        return super(UserCV, self).publish(**kwargs)
