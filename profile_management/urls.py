from django.urls import path

from profile_management import views

app_name = 'profile_management'

urlpatterns = []

urlpatterns += (
    # urls for Community
    path('community/', views.CommunityListView.as_view(), name='community-list'),
    path('community/create/', views.CommunityCreateView.as_view(), name='community-create'),
    path('community/detail/<str:slug>/', views.CommunityDetailView.as_view(), name='community-detail'),
    path('community/update/<str:slug>/', views.CommunityUpdateView.as_view(), name='community-update'),
    path('community/delete/<str:slug>/', views.CommunityDeleteView.as_view(), name='community-delete')
)

urlpatterns += (
    # urls for ProfessionalMembership
    path('professional-membership/', views.ProfessionalMembershipListView.as_view(),
         name='professional-membership-list'),
    path('professional-membership/create/', views.ProfessionalMembershipCreateView.as_view(),
         name='professional-membership-create'),
    path('professional-membership/detail/<str:slug>/', views.ProfessionalMembershipDetailView.as_view(),
         name='professional-membership-detail'),
    path('professional-membership/update/<str:slug>/', views.ProfessionalMembershipUpdateView.as_view(),
         name='professional-membership-update'),
    path('professional-membership/delete/<str:slug>/', views.ProfessionalMembershipDeleteView.as_view(),
         name='professional-membership-delete')
)

urlpatterns += (
    # urls for ScholarshipProfile
    path('scholarship-profile/', views.ScholarshipProfileListView.as_view(), name='scholarship-profile-list'),
    path('scholarship-profile/create/', views.ScholarshipProfileCreateView.as_view(),
         name='scholarship-profile-create'),
    path('scholarship-profile/detail/<str:slug>/', views.ScholarshipProfileDetailView.as_view(),
         name='scholarship-profile-detail'),
    path('scholarship-profile/update/<str:slug>/', views.ScholarshipProfileUpdateView.as_view(),
         name='scholarship-profile-update'),
    path('scholarship-profile/delete/<str:slug>/', views.ScholarshipProfileDeleteView.as_view(),
         name='scholarship-profile-delete')
)

urlpatterns += (
    # urls for SkillSet
    path('skill-set/', views.SkillSetListView.as_view(), name='skill-set-list'),
    path('skill-set/create/', views.SkillSetCreateView.as_view(), name='skill-set-create'),
    path('skill-set/detail/<str:slug>/', views.SkillSetDetailView.as_view(), name='skill-set-detail'),
    path('skill-set/update/<str:slug>/', views.SkillSetUpdateView.as_view(), name='skill-set-update'),
    path('skill-set/delete/<str:slug>/', views.SkillSetDeleteView.as_view(), name='skill-set-delete')
)

urlpatterns += (
    # urls for Testimonial
    path('testimonial/', views.TestimonialListView.as_view(), name='testimonial-list'),
    path('testimonial/create/', views.TestimonialCreateView.as_view(), name='testimonial-create'),
    path('testimonial/detail/<str:slug>/', views.TestimonialDetailView.as_view(), name='testimonial-detail'),
    path('testimonial/update/<str:slug>/', views.TestimonialUpdateView.as_view(), name='testimonial-update'),
    path('testimonial/delete/<str:slug>/', views.TestimonialDeleteView.as_view(), name='testimonial-delete')
)

urlpatterns += (
    # urls for UserCV
    path('user-c-v/', views.UserCVListView.as_view(), name='user-c-v-list'),
    path('user-c-v/create/', views.UserCVCreateView.as_view(), name='user-c-v-create'),
    path('user-c-v/detail/<str:slug>/', views.UserCVDetailView.as_view(), name='user-c-v-detail'),
    path('user-c-v/update/<str:slug>/', views.UserCVUpdateView.as_view(), name='user-c-v-update'),
    path('user-c-v/delete/<str:slug>/', views.UserCVDeleteView.as_view(), name='user-c-v-delete')
)

urlpatterns += (
    # urls for UserSkillSet
    path('user-skill-set/', views.UserSkillSetListView.as_view(), name='user-skill-set-list'),
    path('user-skill-set/create/', views.UserSkillSetCreateView.as_view(), name='user-skill-set-create'),
    path('user-skill-set/detail/<str:slug>/', views.UserSkillSetDetailView.as_view(), name='user-skill-set-detail'),
    path('user-skill-set/update/<str:slug>/', views.UserSkillSetUpdateView.as_view(), name='user-skill-set-update'),
    path('user-skill-set/delete/<str:slug>/', views.UserSkillSetDeleteView.as_view(), name='user-skill-set-delete')
)
