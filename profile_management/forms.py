
from django import forms
from profile_management import models


class CommunityForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(CommunityForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.Community
        fields = '__all__'
        exclude = ['indigenes']


class ProfessionalMembershipForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(ProfessionalMembershipForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.ProfessionalMembership
        fields = '__all__'
        exclude = ['member']


class ScholarshipProfileForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(ScholarshipProfileForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.ScholarshipProfile
        fields = '__all__'
        exclude = ['owner']


class SkillSetForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(SkillSetForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.SkillSet
        fields = '__all__'      


class TestimonialForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(TestimonialForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.Testimonial
        fields = '__all__'
        exclude = ['testifier', 'testified_on']


class UserCVForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(UserCVForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.UserCV
        fields = '__all__'
        exclude = ['user']


class UserSkillSetForm(forms.ModelForm):

    def get_initial_for_field(self, field, field_name):
        # override the initial value for the specified form field
        return super(UserSkillSetForm, self).get_initial_for_field(field, field_name)

    class Meta:
        model = models.UserSkillSet
        fields = '__all__'
        exclude = ['user']
