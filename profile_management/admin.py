from django.contrib import admin

from . import models

admin.register(models.ScholarshipProfile)
admin.register(models.Profile)
admin.register(models.Community)
