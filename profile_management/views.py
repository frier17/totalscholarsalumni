import datetime as dt

from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms import ModelMultipleChoiceField
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import DetailView, ListView, CreateView

from profile_management import forms
from profile_management import models
import achievement.models as achievements
from util import current_timezone
from util.logging import flash_message
from util.views import DeleteAclMixin, UpdateAclMixin, ReadOnlyMixin


class CommunityListView(ListView):
    model = models.Community
    # paginate_by = 10
    template_name = 'profile_management/community_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(CommunityListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(indigenes=user)
            return queryset
        else:
            return super(CommunityListView, self).get_queryset().none()


class CommunityCreateView(LoginRequiredMixin, CreateView):
    model = models.Community
    form_class = forms.CommunityForm
    template_name = 'profile_management/community_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')

    def get(self, request, *args, **kwargs):
        if self.request.user.is_staff:
            return super(CommunityCreateView, self).get(request, *args, **kwargs)
        flash_message(request, 'You do not have access of permission to perform selected task')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    def post(self, request, *args, **kwargs):
        if self.request.user.is_staff:
            form = self.get_form()
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.form_invalid(form)
        flash_message(request, 'You do not have access of permission to perform selected task')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    def get_initial(self):
        initial = super(CommunityCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        return reverse_lazy('profile_management:community-list')


class CommunityDetailView(DetailView):
    model = models.Community
    template_name = 'profile_management/community_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(CommunityDetailView, self).get_context_data(**kwargs)


class CommunityUpdateView(UpdateAclMixin):
    model = models.Community
    form_class = forms.CommunityForm
    template_name = 'profile_management/community_form.html'
    initial = {}
    user_field = 'approvals__approved_by'

    def get(self, request, *args, **kwargs):
        if self.request.user.is_staff:
            return super(CommunityUpdateView, self).get(request, *args, **kwargs)
        flash_message(request, 'You do not have access of permission to perform selected task')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(CommunityUpdateView, self).get_object()

    def get_success_url(self):
        return reverse_lazy('profile_management:community-list')

    def get_initial(self):
        initial = super(CommunityUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class CommunityDeleteView(DeleteAclMixin):
    model = models.Community
    success_url = reverse_lazy('profile_management:community-list')
    template_name = 'profile_management/community_confirm_delete.html'
    user_field = 'approvals__approved_by'

    def get(self, request, *args, **kwargs):
        if self.request.user.is_staff:
            return super(CommunityDeleteView, self).get(request, *args, **kwargs)
        flash_message(request, 'You do not have access of permission to perform selected task')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(CommunityDeleteView, self).get_context_data(**kwargs)


class ProfessionalMembershipListView(ListView):
    model = models.ProfessionalMembership
    # paginate_by = 10
    template_name = 'profile_management/professional_membership_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(ProfessionalMembershipListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(member=user)
            return queryset
        else:
            return super(ProfessionalMembershipListView, self).get_queryset().none()


class ProfessionalMembershipCreateView(LoginRequiredMixin, CreateView):
    model = models.ProfessionalMembership
    form_class = forms.ProfessionalMembershipForm
    template_name = 'profile_management/professional_membership_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')
    
    def form_valid(self, form):
        form.instance.member = self.request.user
        return super(ProfessionalMembershipCreateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(ProfessionalMembershipCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        return reverse_lazy('profile_management:professional_membership-list')


class ProfessionalMembershipDetailView(DetailView):
    model = models.ProfessionalMembership
    template_name = 'profile_management/professional_membership_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(ProfessionalMembershipDetailView, self).get_context_data(**kwargs)


class ProfessionalMembershipUpdateView(UpdateAclMixin):
    model = models.ProfessionalMembership
    form_class = forms.ProfessionalMembershipForm
    template_name = 'profile_management/professional_membership_form.html'
    initial = {}
    user_field = 'member'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(ProfessionalMembershipUpdateView, self).get_object()

    def get_success_url(self):
        return reverse_lazy('profile_management:professional-membership-list')

    def get_initial(self):
        initial = super(ProfessionalMembershipUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class ProfessionalMembershipDeleteView(DeleteAclMixin):
    model = models.ProfessionalMembership
    success_url = reverse_lazy('profile_management:professional-membership-list')
    template_name = 'profile_management/professional_membership_confirm_delete.html'
    user_field = 'member'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(ProfessionalMembershipDeleteView, self).get_context_data(**kwargs)


class ScholarshipProfileListView(LoginRequiredMixin, ListView):
    model = models.ScholarshipProfile
    # paginate_by = 10
    template_name = 'profile_management/scholarship_profile_list.html'
    login_url = reverse_lazy('user-login')

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(ScholarshipProfileListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(owner=user)
            return queryset
        else:
            return super(ScholarshipProfileListView, self).get_queryset().none()


class ScholarshipProfileCreateView(LoginRequiredMixin, CreateView):
    model = models.ScholarshipProfile
    form_class = forms.ScholarshipProfileForm
    template_name = 'profile_management/scholarship_profile_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')
    
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(ScholarshipProfileCreateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(ScholarshipProfileCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        return reverse_lazy('profile_management:scholarship-profile-list')


class ScholarshipProfileDetailView(LoginRequiredMixin, DetailView):
    model = models.ScholarshipProfile
    template_name = 'profile_management/scholarship_profile_detail.html'
    login_url = reverse_lazy('user-login')

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(ScholarshipProfileDetailView, self).get_context_data(**kwargs)


class ScholarshipProfileUpdateView(UpdateAclMixin):
    model = models.ScholarshipProfile
    form_class = forms.ScholarshipProfileForm
    template_name = 'profile_management/scholarship_profile_form.html'
    initial = {}
    user_field = 'owner'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(ScholarshipProfileUpdateView, self).get_object()

    def get_success_url(self):
        return reverse_lazy('profile_management:scholarship-profile-list')

    def get_initial(self):
        initial = super(ScholarshipProfileUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class ScholarshipProfileDeleteView(DeleteAclMixin):
    model = models.ScholarshipProfile
    success_url = reverse_lazy('profile_management:scholarship-profile-list')
    template_name = 'profile_management/scholarship_profile_confirm_delete.html'
    user_field = 'owner'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(ScholarshipProfileDeleteView, self).get_context_data(**kwargs)


class SkillSetListView(LoginRequiredMixin, ListView):
    model = models.SkillSet
    # paginate_by = 10
    template_name = 'profile_management/skill_set_list.html'
    login_url = reverse_lazy('user-login')

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(SkillSetListView, self).get_queryset()
            # filter queryset for logged in user
            return queryset
        else:
            return super(SkillSetListView, self).get_queryset().none()


class SkillSetCreateView(LoginRequiredMixin, CreateView):
    model = models.SkillSet
    form_class = forms.SkillSetForm
    template_name = 'profile_management/skill_set_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(SkillSetCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        return reverse_lazy('profile_management:skill-set-list')


class SkillSetDetailView(LoginRequiredMixin, DetailView):
    model = models.SkillSet
    template_name = 'profile_management/skill_set_detail.html'
    login_url = reverse_lazy('user-login')

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(SkillSetDetailView, self).get_context_data(**kwargs)


class SkillSetUpdateView(ReadOnlyMixin):
    model = models.SkillSet
    form_class = forms.SkillSetForm
    template_name = 'profile_management/skill_set_form.html'
    initial = {}


class SkillSetDeleteView(ReadOnlyMixin):
    model = models.SkillSet
    success_url = reverse_lazy('profile_management:skill-set-list')
    template_name = 'profile_management/skill_set_confirm_delete.html'


class TestimonialListView(ListView):
    model = models.Testimonial
    # paginate_by = 10
    template_name = 'profile_management/testimonial_list.html'

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(TestimonialListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(testifier=user)
            return queryset
        else:
            return super(TestimonialListView, self).get_queryset().none()


class TestimonialCreateView(LoginRequiredMixin, CreateView):
    model = models.Testimonial
    form_class = forms.TestimonialForm
    template_name = 'profile_management/testimonial_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')

    def form_valid(self, form):
        form.instance.testifier = self.request.user
        form.instance.testified_on = current_timezone(dt.datetime.now())
        return super(TestimonialCreateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(TestimonialCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        return reverse_lazy('profile_management:testimonial-list')


class TestimonialDetailView(DetailView):
    model = models.Testimonial
    template_name = 'profile_management/testimonial_detail.html'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(TestimonialDetailView, self).get_context_data(**kwargs)


class TestimonialUpdateView(UpdateAclMixin):
    model = models.Testimonial
    form_class = forms.TestimonialForm
    template_name = 'profile_management/testimonial_form.html'
    initial = {}
    user_field = 'testifier'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(TestimonialUpdateView, self).get_object()

    def get_success_url(self):
        return reverse_lazy('profile_management:testimonial-list')

    def get_initial(self):
        initial = super(TestimonialUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class TestimonialDeleteView(DeleteAclMixin):
    model = models.Testimonial
    success_url = reverse_lazy('profile_management:testimonial-list')
    template_name = 'profile_management/testimonial_confirm_delete.html'
    user_field = 'testifier'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(TestimonialDeleteView, self).get_context_data(**kwargs)


class UserCVListView(LoginRequiredMixin, ListView):
    model = models.UserCV
    # paginate_by = 10
    template_name = 'profile_management/user_c_v_list.html'
    login_url = reverse_lazy('user-login')

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(UserCVListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(user=user)
            return queryset
        else:
            return super(UserCVListView, self).get_queryset().none()


class UserCVCreateView(LoginRequiredMixin, CreateView):
    model = models.UserCV
    form_class = forms.UserCVForm
    template_name = 'profile_management/user_c_v_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')
    
    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(UserCVCreateView, self).form_valid(form)

    def get(self, request, *args, **kwargs):

        work_experiences = achievements.WorkExperience.objects.filter(employee=self.request.user)
        education = achievements.EducationHistory.objects.filter(awardee=self.request.user)
        qualifications = achievements.Qualification.objects.filter(awardee=self.request.user)
        form = self.form_class(initial={'work_experiences': work_experiences, 'education_histories': education,
                                        'qualifications': qualifications})
        form.fields['work_experiences'] = ModelMultipleChoiceField(work_experiences)
        form.fields['education_histories'] = ModelMultipleChoiceField(education)
        form.fields['qualifications'] = ModelMultipleChoiceField(qualifications)
        return render(request, template_name=self.template_name, context={'form': form})

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(UserCVCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        return reverse_lazy('profile_management:user-c-v-list')


class UserCVDetailView(LoginRequiredMixin, DetailView):
    model = models.UserCV
    template_name = 'profile_management/user_c_v_detail.html'
    login_url = reverse_lazy('user-login')

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserCVDetailView, self).get_context_data(**kwargs)


class UserCVUpdateView(UpdateAclMixin):
    model = models.UserCV
    form_class = forms.UserCVForm
    template_name = 'profile_management/user_c_v_form.html'
    initial = {}
    user_field = 'user'
    login_url = reverse_lazy('user-login')

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(UserCVUpdateView, self).get_object()

    def get_success_url(self):
        return reverse_lazy('profile_management:user-c-v-list')

    def get_initial(self):
        initial = super(UserCVUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class UserCVDeleteView(DeleteAclMixin):
    model = models.UserCV
    success_url = reverse_lazy('profile_management:user-c-v-list')
    template_name = 'profile_management/user_c_v_confirm_delete.html'
    user_field = 'user'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserCVDeleteView, self).get_context_data(**kwargs)


class UserSkillSetListView(LoginRequiredMixin, ListView):
    model = models.UserSkillSet
    # paginate_by = 10
    template_name = 'profile_management/user_skill_set_list.html'
    login_url = reverse_lazy('user-login')

    def get_queryset(self):
        # Retrieve list object for the current user or approved user  
        user = self.request.user
        if user.is_authenticated:
            queryset = super(UserSkillSetListView, self).get_queryset()
            # filter queryset for logged in user
            queryset = queryset.filter(user=user)
            return queryset
        else:
            return super(UserSkillSetListView, self).get_queryset().none()


class UserSkillSetCreateView(LoginRequiredMixin, CreateView):
    model = models.UserSkillSet
    form_class = forms.UserSkillSetForm
    template_name = 'profile_management/user_skill_set_form.html'
    initial = {}
    login_url = reverse_lazy('user-login')
    
    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(UserSkillSetCreateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        initial = super(UserSkillSetCreateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial

    def get_success_url(self):
        return reverse_lazy('profile_management:user-skill-set-list')


class UserSkillSetDetailView(LoginRequiredMixin, DetailView):
    model = models.UserSkillSet
    template_name = 'profile_management/user_skill_set_detail.html'
    login_url = reverse_lazy('user-login')

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserSkillSetDetailView, self).get_context_data(**kwargs)


class UserSkillSetUpdateView(UpdateAclMixin):
    model = models.UserSkillSet
    form_class = forms.UserSkillSetForm
    template_name = 'profile_management/user_skill_set_form.html'
    initial = {}
    user_field = 'user'

    def get_object(self, queryset=None):
        # Retrieve object record if current user has permission to object
        return super(UserSkillSetUpdateView, self).get_object()

    def get_success_url(self):
        return reverse_lazy('profile_management:user-skill-set-list')

    def get_initial(self):
        initial = super(UserSkillSetUpdateView, self).get_initial()
        # Update initial data for instantiating form using currently logged user for queryset
        # for Many to Many and One to Many related fields
        return initial


class UserSkillSetDeleteView(DeleteAclMixin):
    model = models.UserSkillSet
    success_url = reverse_lazy('profile_management:user-skill-set-list')
    template_name = 'profile_management/user_skill_set_confirm_delete.html'
    user_field = 'user'

    def get_context_data(self, **kwargs):
        # Retrieve object record if current user has permission to object
        return super(UserSkillSetDeleteView, self).get_context_data(**kwargs)
